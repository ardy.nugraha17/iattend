GLOBAL.self = GLOBAL;
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { StackNavigator } from 'react-navigation';


// ini untuk screen //

import BarcodeScreen from './src/containers/BarcodeScreen';
import BarcodeScanScreen from './src/containers/BarcodeScreen/scan';
import CameraScreen from './src/containers/CameraScreen';
import DetailEvent from './src/containers/DetailEvent';
import EventDetail from './src/containers/EventDetail';
import EventList from './src/component/EventList';
import HomeScreen from './src/containers/Dashboard';
import Login from './src/containers/Login/';
import SplashScreen from './src/containers/SplashScreen';
import SignatureScreen from './src/containers/SignatureScreen';
import Register from './src/containers/Register/';
import Attendeelist from './src/containers/Attendeelist/';
import Agenda from './src/containers/Agenda';
import ScheduleByDay from './src/containers/Agenda/scheduleByDay';
import ScheduleByDayDetail from './src/containers/Agenda/detail';
import SpeakersList from './src/containers/Speakers';
import SpeakersDetail from './src/containers/Speakers/detail';
import SponsorsList from './src/containers/Sponsors';
import SponsorsDetail from './src/containers/Sponsors/detail';
import AboutUsScreen from './src/containers/AboutUs';
import QuestionScreen from './src/containers/Question';
import PollingScreen from './src/containers/Polling';
import PollingQuestionScreen from './src/containers/Polling/question';
import FileScreen from './src/containers/File';
import DetailFileScreen from './src/containers/File/detail';
import EvaluationScreen from './src/containers/EvaluationForm';

import store from './src/redux/configureStore';
   
console.disableYellowBox = true;

const AppNavigator = StackNavigator(
  {
    Camera: { screen: CameraScreen },
    DetailEvent: { screen: DetailEvent },
    EventDetail: { screen: EventDetail },
    EventList: { screen: EventList },
    Home: { screen: HomeScreen },
    Login: { screen: Login },
    Splash: { screen: SplashScreen },
    Signature: { screen: SignatureScreen },
    Register: { screen: Register },
    Attend: { screen: BarcodeScreen },
    ScanScreen:{screen: BarcodeScanScreen},
    Attendeelist: { screen: Attendeelist },
    Agenda: { screen: Agenda },
    ScheduleByDay: { screen: ScheduleByDay },
    ScheduleByDayDetail: { screen: ScheduleByDayDetail },
    Speakers: { screen: SpeakersList},
    SpeakersDetail:{screen:SpeakersDetail},
    Sponsors: { screen: SponsorsList},
    SponsorsDetail: { screen: SponsorsDetail},
    AboutUs: { screen: AboutUsScreen },
    Question:{screen: QuestionScreen},
    Polling: { screen: PollingScreen},
    PollingQuestion:{screen: PollingQuestionScreen},
    File: { screen: FileScreen},
    DetailFile: { screen: DetailFileScreen},
    Evaluation: { screen: EvaluationScreen},
  },
  {
    initialRouteName: 'Splash',
    headerMode: 'none',
  },
);

export default class App extends Component {

  render() {
     return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
}