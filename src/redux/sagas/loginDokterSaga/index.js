import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const userLogin = payload => ApiCaller('mobileIattend_login_dokter', 'post', null, payload).then(response => response);

export const watchUserLoginDokter = function* watchUserLoginDokter() {
  yield takeLatest('USER_LOGIN_DOKTER', function* (action) {
    try {
      const data = yield call(userLogin.bind(this, action.payload));
      yield put({ type: 'USER_LOGIN_DOKTER_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'USER_LOGIN_DOKTER_FAILED', payload: error });
    }
  });
};
