import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';
import { getEventListData } from '../../../apiParser/eventDataParser';

const fetchAgendaList = payload => ApiCaller('mobileIattend_polling', 'post', null, payload).then(response => response);

export const watchPollingData = function* watchPollingData() {
  yield takeLatest('FETCH_POLLING', function* (action) {
    try {
      const data = yield call(fetchAgendaList.bind(this, action.payload));
      // const eventData = getEventListData(data);
      yield put({ type: 'FETCH_POLLING_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'FETCH_POLLING_FAILED', payload: error });
    }
  });
};
