import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const userRegistration = payload => ApiCaller('mobileIattend_register', 'post', null, payload).then(response => response);

export const watchUserRegistration = function* watchUserRegistration() {
  yield takeLatest('USER_REGISTRATION', function* (action) {
    try {
      const data = yield call(userRegistration.bind(this, action.payload));
      yield put({ type: 'USER_REGISTER_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'USER_REGISTER_FAILED', payload: error });
    }
  });
};
