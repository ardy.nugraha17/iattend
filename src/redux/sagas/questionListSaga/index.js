import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';
import { getQuestionListData } from '../../../apiParser/questionDataParser';

const userLogin = payload => ApiCaller('getdetailquestion', 'post', null, payload).then(response => response);

export const watchQuestionListData = function* watchQuestionListData() {
  yield takeLatest('FETCH_QUESTION_LIST', function* (action) {
    try {
      const data = yield call(userLogin.bind(this, action.payload));
      const questionData = getQuestionListData(data);
      yield put({ type: 'FETCH_QUESTION_LIST_SUCCESS', payload: questionData });
    } catch (error) {
      yield put({ type: 'FETCH_QUESTION_LIST_FAILED', payload: error });
    }
  });
};