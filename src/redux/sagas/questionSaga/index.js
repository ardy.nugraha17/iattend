import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';
import { getEventListData } from '../../../apiParser/eventDataParser';

const fetchAgendaList = payload => ApiCaller('mobileIattend_question', 'post', null, payload).then(response => response);

export const watchQuestionData = function* watchQuestionData() {
  yield takeLatest('FETCH_QUESTION', function* (action) {
    try {
      const data = yield call(fetchAgendaList.bind(this, action.payload));
      // const eventData = getEventListData(data);
      yield put({ type: 'FETCH_QUESTION_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'FETCH_QUESTION_FAILED', payload: error });
    }
  });
};
