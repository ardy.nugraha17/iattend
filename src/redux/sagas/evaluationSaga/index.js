import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const fetchAgendaList = payload => ApiCaller('evaluationaudienceakar', 'post', null, payload).then(response => response);

export const watchQuestionData = function* watchQuestionData() {
  yield takeLatest('FETCH_EVALUATION', function* (action) {
    try {
      const data = yield call(fetchAgendaList.bind(this, action.payload));
      yield put({ type: 'FETCH_EVALUATION_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'FETCH_EVALUATION_FAILED', payload: error });
    }
  });
};
