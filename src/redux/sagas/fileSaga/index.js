import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const fetchFileList = payload => ApiCaller('fileaudienceakar', 'post', null, payload).then(response => response);

export const watchFileData = function* watchFileData() {
  yield takeLatest('FETCH_FILE', function* (action) {
    try {
      const data = yield call(fetchFileList.bind(this, action.payload));
      // const eventData = getEventListData(data);
      yield put({ type: 'FETCH_FILE_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'FETCH_FILE_FAILED', payload: error });
    }
  });
};
