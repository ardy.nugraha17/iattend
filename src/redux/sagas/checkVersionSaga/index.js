import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const userLogin = payload => ApiCaller('mobileIattend_checkversion', 'post', null, payload).then(response => response);

export const watchUserCheckVersion = function* watchUserCheckVersion() {
  yield takeLatest('CHECK_VERSION', function* (action) {
    try {
      const data = yield call(userLogin.bind(this, action.payload));
      yield put({ type: 'CHECK_VERSION_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'CHECK_VERSION_FAILED', payload: error });
    }
  });
};
