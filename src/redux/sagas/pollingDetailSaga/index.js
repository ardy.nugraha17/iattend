import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const fetchPollingDetailList = payload => ApiCaller('mobileIattend_polling_detail', 'post', null, payload).then(response => response);

export const watchPollingDetailData = function* watchPollingDetailData() {
  yield takeLatest('FETCH_POLLING_QUESTION', function* (action) {
    try {
      const data = yield call(fetchPollingDetailList.bind(this, action.payload));
      // const eventData = getEventListData(data);
      yield put({ type: 'FETCH_POLLING_QUESTION_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'FETCH_POLLING_QUESTION_FAILED', payload: error });
    }
  });
};
