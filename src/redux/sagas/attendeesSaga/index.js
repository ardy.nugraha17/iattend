import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const userLogin = payload => ApiCaller('apiinsertattendees', 'post', null, payload).then(response => response);

export const watchUserAttendees = function* watchUserAttendees() {
  yield takeLatest('ADD_PERSON', function* (action) {
    try {
      const data = yield call(userLogin.bind(this, action.payload));
      yield put({ type: 'ADD_PERSON_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'ADD_PERSON_FAILED', payload: error });
    }
  });
};
