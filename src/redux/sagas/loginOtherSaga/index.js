import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const userLoginOther = payload => ApiCaller('mobileIattend_login_other', 'post', null, payload).then(response => response);

export const watchUserLoginOther = function* watchUserLoginOther() {
  yield takeLatest('USER_LOGIN_OTHER', function* (action) {
    try {
      const data = yield call(userLoginOther.bind(this, action.payload));
      yield put({ type: 'USER_LOGIN_OTHER_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'USER_LOGIN_OTHER_FAILED', payload: error });
    }
  });
};
