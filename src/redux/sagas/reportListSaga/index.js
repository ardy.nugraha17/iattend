import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const userReport = payload => ApiCaller('mobileIattend_report_attend_again', 'post', null, payload).then(response => response);

export const watchUserReportList = function* watchUserReportList() {
  yield takeLatest('FETCH_REPORT_LIST', function* (action) {
    try {
      const data = yield call(userReport.bind(this, action.payload));
      yield put({ type: 'FETCH_REPORT_LIST_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'FETCH_REPORT_LIST_FAILED', payload: error });
    }
  });
};