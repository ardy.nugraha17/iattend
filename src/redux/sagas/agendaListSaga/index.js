import { put, takeLatest, call } from 'redux-saga/effects';
import ApiCaller from '../../../common/apiCaller';

const fetchAgendaList = payload => ApiCaller('getagendaaudienceakar', 'post', payload.accessToken, payload.eventId).then(response => response);

export const watchAgendaListData = function* watchAgendaListData() {
  yield takeLatest('FETCH_AGENDA_LIST', function* (action) {
    try {
      const data = yield call(fetchAgendaList.bind(this, action.payload));
      yield put({ type: 'FETCH_AGENDA_LIST_SUCCESS', payload: data });
    } catch (error) {
      yield put({ type: 'FETCH_AGENDA_LIST_FAILED', payload: error });
    }
  });
};
