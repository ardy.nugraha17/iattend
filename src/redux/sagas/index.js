import { all } from 'redux-saga/effects';
import { watchEventListData } from './eventListSaga/';
import { watchUserLogin } from './loginSaga/';
import { watchUserLoginOther } from './loginOtherSaga/';
import { watchUserLoginDokter } from './loginDokterSaga/';
import { watchUserRegistration } from './registrationSaga';
import { watchUserAttendees } from './attendeesSaga';
import { watchUserCheckVersion } from './checkVersionSaga/';
import { watchUserReportList } from './reportListSaga/';
import { watchAgendaListData } from './agendaListSaga/';
import { watchSpeakerListData } from './speakerListSaga/';
import { watchSponserListData } from './sponserListSaga/';
import { watchQuestionData } from './questionSaga/';
import { watchPollingData } from './pollingSaga/';
import { watchPollingDetailData } from './pollingDetailSaga/';
import { watchFileData } from './fileSaga/';

const rootSaga = function* rootSaga() {
  yield all([ 
    watchEventListData(),
    watchUserLogin(),
    watchUserLoginOther(),
    watchUserLoginDokter(),
    watchUserRegistration(),
    watchUserAttendees(),
    watchUserCheckVersion(),
    watchUserReportList(),
    watchAgendaListData(),
    watchSpeakerListData(),
    watchSponserListData(),
    watchQuestionData(),
    watchPollingData(),
    watchFileData(),
    watchPollingDetailData()
    
  ]);
};


export default rootSaga;
