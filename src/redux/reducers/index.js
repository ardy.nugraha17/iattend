import { combineReducers } from 'redux';

import eventData from './eventDataReducer';
import reportData from './reportDataReducer';
import loginData from './loginReducer';
import loginOtherData from './loginOtherReducer';
import loginDokterData from './loginDokterReducer';
import checkversionData from './checkversionReducer';
import registerData from './registerReducer';
import attendees from './attendeesReducer';
import agendaData from './agendaReducer';
import speakerData from './speakersReducer';
import aponserData from './sponserReducer';
import questionData from './questionReducer';
import pollingData from './pollingReducer';
import pollingdetailData from './pollingDetailReducer';
import fileData from './fileReducer';
import evaluationData from './evaluationReducer';

export default combineReducers({
  eventData,
  loginData,
  loginOtherData,
  loginDokterData,
  registerData,
  attendees,
  checkversionData,
  reportData,
  agendaData,
  speakerData,
  aponserData,
  questionData,
  pollingData,
  fileData,
  evaluationData,
  pollingdetailData
});
