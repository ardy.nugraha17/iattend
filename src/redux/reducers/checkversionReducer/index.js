export default function reducer(
  state = {
    checkversion: [],
  },
  action,
) {
  switch (action.type) {
    case 'CHECK_VERSION_SUCCESS': {
      return { ...state, checkversion: action.payload };
    }
    case 'CHECK_VERSION_FAILED': {
      return { ...state, checkversion: action.payload };
    }
    default: {
      return state;
    }
  }
}
