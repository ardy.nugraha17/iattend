export default function reducer(
  state = {
    loginDokter: [],
  },
  action,
) {
  switch (action.type) {
    case 'USER_LOGIN_DOKTER_SUCCESS': {
      return { ...state, loginDokter: action.payload };
    }
    case 'USER_LOGIN_DOKTER_FAILED': {
      return { ...state, loginDokter: action.payload };
    }
    default: {
      return state;
    }
  }
}
