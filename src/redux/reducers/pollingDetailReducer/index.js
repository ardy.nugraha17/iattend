export default function reducer(
  state = {
    pollingQuestion: [],
  },
  action,
) {
  switch (action.type) {
    case 'FETCH_POLLING_QUESTION_SUCCESS': {
      return { ...state, pollingQuestion: action.payload };
    }
    case 'FETCH_POLLING_QUESTION_FAILED': {
      return { ...state, pollingQuestion: action.payload };
    }
    default: {
      return state;
    }
  }
}
