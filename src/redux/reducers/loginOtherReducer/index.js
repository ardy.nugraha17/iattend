export default function reducer(
  state = {
    loginOtherData: [],
  },
  action,
) {
  switch (action.type) {
    case 'USER_LOGIN_OTHER_SUCCESS': {
      return { ...state, loginOtherData: action.payload };
    }
    case 'USER_LOGIN_OTHER_FAILED': {
      return { ...state, loginOtherData: action.payload };
    }
    default: {
      return state;
    }
  }
}
