export default function reducer(
  state = {
    polling: [],
  },
  action,
) {
  switch (action.type) {
    case 'FETCH_POLLING_SUCCESS': {
      return { ...state, polling: action.payload };
    }
    case 'FETCH_POLLING_FAILED': {
      return { ...state, polling: action.payload };
    }
    default: {
      return state;
    }
  }
}
