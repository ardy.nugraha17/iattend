export default function reducer(
  state = {
    evaluation: [],
  },
  action,
) {
  switch (action.type) {
    case 'FETCH_EVALUATION_SUCCESS': {
      return { ...state, evaluation: action.payload };
    }
    case 'FETCH_EVALUATION_FAILED': {
      return { ...state, evaluation: action.payload };
    }
    default: {
      return state;
    }
  }
}
