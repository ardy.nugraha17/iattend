export default function reducer(
  state = {
    speakers: [],
  },
  action,
) {
  switch (action.type) {
    case 'FETCH_SPEAKERS_LIST_SUCCESS': {
      return { ...state, speakers: action.payload };
    }
    case 'FETCH_SPEAKERS_LIST_FAILED': {
      return { ...state, speakers: action.payload };
    }
    default: {
      return state;
    }
  }
}
