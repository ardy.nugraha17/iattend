export default function reducer(
  state = {
    sponsers: [],
  },
  action,
) {
  switch (action.type) {
    case 'FETCH_SPONSERS_LIST_SUCCESS': {
      return { ...state, sponsers: action.payload };
    }
    case 'FETCH_SPONSERS_LIST_FAILED': {
      return { ...state, sponsers: action.payload };
    }
    default: {
      return state;
    }
  }
}
