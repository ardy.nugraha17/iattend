export default function reducer(
  state = {
    listfileData: [],
  },
  action,
) {
  switch (action.type) {
    case 'FETCH_FILE_SUCCESS': {
      return { ...state, listfileData: action.payload };
    }
    case 'FETCH_FILE_FAILED': {
      return { ...state, listfileData: action.payload };
    }
    default: {
      return state;
    }
  }
}
