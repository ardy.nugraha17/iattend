export default function reducer(
  state = {
    report: [],
  },
  action,
) {
  switch (action.type) {
    case 'FETCH_REPORT_LIST_SUCCESS': {
      return { ...state, report: action.payload };
    }
    case 'FETCH_REPORT_LIST_FAILED': {
      return { ...state, report: action.payload };
    }
    default: {
      return state;
    }
  }
}
