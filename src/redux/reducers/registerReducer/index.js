export default function reducer(
  state = {
    register: [],
  },
  action,
) {
  switch (action.type) {
    case 'USER_REGISTER_SUCCESS': {
     console.log(action.payload);
      return { ...state, register: action.payload };
    }
    case 'USER_REGISTER_FAILED': {
      return { ...state, register: action.payload };
    }
    default: {
      return state;
    }
  }
}
