export default function reducer(
  state = {
    question: [],
  },
  action,
) {
  switch (action.type) {
    case 'FETCH_QUESTION_SUCCESS': {
      return { ...state, question: action.payload };
    }
    case 'FETCH_QUESTION_FAILED': {
      return { ...state, question: action.payload };
    }
    default: {
      return state;
    }
  }
}
