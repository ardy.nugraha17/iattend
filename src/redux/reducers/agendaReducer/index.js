export default function reducer(
  state = {
    agenda: [],
  },
  action,
) {
  switch (action.type) {
    case 'FETCH_AGENDA_LIST_SUCCESS': {
      return { ...state, agenda: action.payload };
    }
    case 'FETCH_AGENDA_LIST_FAILED': {
      return { ...state, agenda: action.payload };
    }
    default: {
      return state;
    }
  }
}
