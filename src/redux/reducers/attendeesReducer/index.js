export default function reducer(
  state = {
    person: [],
  },
  action,
) {
  switch (action.type) {
    case 'ADD_PERSON_SUCCESS': {
      return { ...state, person: action.payload };
    }
    case 'ADD_PERSON_FAILED': {
      return { ...state, person: action.payload };
    }
    default: {
      return state;
    }
  }
}
