import React, { Component } from 'react'
import {
  View,
  Image,
  StatusBar,
  // Text,
  AsyncStorage, 
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  CameraRoll, 
  ToastAndroid,
  FlatList,
  Dimensions,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  BackHandler

} from 'react-native'
import { Tabs, Tab, Container, Content, Item, Input, Label,Button, List, ListItem, Left, Body, Right, Thumbnail, Text
  //  Icon,
  // Button
} from 'native-base';
import { connect } from 'react-redux';
// import Icon from "react-native-vector-icons/FontAwesome";
import styles from './styles';
import DrawerView from '../../component/DrawerView';
import EventList from '../../component/EventList';
import { eventList, eventListJune , mainText} from '../../common/constant';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'

import Spinner from 'react-native-loading-spinner-overlay';
// import Button from '../../component/Button';
// import { Button } from 'react-native-elements';

import Icon from 'react-native-vector-icons/MaterialIcons';

import validate from '../../common/validate.js'


import Camera from 'react-native-camera';

// untuk capture camera 
import { RNCamera } from 'react-native-camera';
import ImageResizer from 'react-native-image-resizer';
import SignatureCapture from 'react-native-signature-capture';

import { Col, Row, Grid } from "react-native-easy-grid";
import ApiCaller from '../../common/apiCaller';
import axios from 'axios';
import { Dialog  } from 'react-native-simple-dialogs';
import moment from 'moment';

const timer = require('react-native-timer');



class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data : [],
      gender : "",
      isFetching: false,
      eventName:'',
      eventDate:'',
      eventVenue:'',
      ActivityIndicator: false,
      searchAttendeText:''
    }

    this.ArraylistAttende = '';

  }
  
  componentDidMount(){
    console.log("attendeelist componentDidMount",this.props.navigation.state.params.navigation.state.params, this.state.reportData);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillReceiveProps (nextProps) {
    console.log("attendeelist",nextProps.reportData.report);
    // console.log("attendeelist dua",nextProps.reportData.report[0].event_name);




    if(nextProps.reportData.report.length > 0){
      this.setState({
        data:nextProps.reportData.report,
        isFetching: false,
        ActivityIndicator:true
      })
  
      this.ArraylistAttende = nextProps.reportData.report;
    }






  }

  // just before the update
  componentWillUpdate (nextProps, nextState) {

  }

  // immediately after the update
  componentDidUpdate (prevProps, prevState) {
    console.log("componentDidUpdate")
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  goBack(){
    this.props.navigation.goBack(null)
  }

  componentWillMount()
  {

      console.log("componentWillMount attendlist", this.props.navigation.state.params.navigation.state.params);
      this.setState({
        eventName: this.props.navigation.state.params.navigation.state.params.eventTitle,
        eventDate: this.props.navigation.state.params.navigation.state.params.eventDate,
        eventVenue: this.props.navigation.state.params.navigation.state.params.eventAddress
      })
      this.searchRandomUser();
  }

  onRefresh() {
      this.setState({ isFetching: true }, function() { this.searchRandomUser() });
   }

  searchRandomUser = async () =>
  {


    AsyncStorage.getItem("userData").then((data) => {
   
      console.log("barcodeReceived audienceId attendee list",JSON.parse(data));
      // console.log("submitButton", this.state.eventId + "data userdata" + data + ":" + this.state.textClass + ":" + this.state.textTrack);


      ApiCaller('mobileIattend_report_attend', 'post', JSON.parse(data).token, {
        audienceid : JSON.parse(data).audienceid,
        email : JSON.parse(data).mail
    
      }).then(response =>{
  
        console.log("barcodeReceived response dua", response);

        const value = {
          id: this.props.navigation.state.params.navigation.state.params.eventId
        }
        this.props.dispatch({
          type: 'FETCH_REPORT_LIST',
          payload: value
        }); 
  
      })
  

    }).done();


  }


  searchAttende(text){

    this.setState({
      searchAttendeText:text
    })
    
    if(this.state.searchAttendeText.length > 0){
      const newData = this.ArraylistAttende.filter(function(item){

        console.log("searchEvaluation dua",item, text);
        const itemData = item.audience_name.toUpperCase()
        const textData = text.toUpperCase()
        return itemData.indexOf(textData) > -1
      })
      this.setState({
          data: newData,
      })
    }else if(this.state.data.length == 0){
      this.searchRandomUser();
    }



  }


  converDate(){

    if(this.state.eventDate !=''){
      var a = moment(this.state.eventDate).format('DD MMMM YYYY') + " Time " + moment(this.state.eventDate).format('h:mm A');
      return a;
    }else{
      var a = '-';
      return a;
    }

  }


  render() {

    const {navigation} = this.props;

    return (

      <Container style={styles.container}>


            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Attendee List"
            />

              <View style = {styles.containerEmpat}>

                  <Text style = {{fontSize:16, fontWeight:'bold', fontFamily:'Roboto'}}>Date Time Event :</Text>
                  <Text style = {{fontSize:16, fontFamily:'Roboto', borderBottomWidth:0.5, borderBottomColor:'#d7c3b8', paddingBottom:10, marginBottom:10}}>
                  {/* {this.converDate()} */} {this.state.eventDate}
                  </Text>

                  <Text style = {{fontSize:16, fontWeight:'bold', fontFamily:'Roboto'}}>Agenda Name :</Text>
                  <Text style = {{fontSize:16, fontFamily:'Roboto', borderBottomWidth:0.5, borderBottomColor:'#d7c3b8', paddingBottom:10, marginBottom:10}}>{this.state.eventName}</Text>

                  <Text style = {{fontSize:16, fontWeight:'bold', fontFamily:'Roboto'}}>Venue : <Text style = {{fontSize:16, fontFamily:'Roboto', fontWeight:'normal'}}>{this.state.eventVenue}</Text> </Text>
                  

              </View>

              <View style = {styles.containerTiga}>

                  <TextInput
                  style={styles.TextInputStyleClass}
                  onChangeText={(text) => this.searchAttende(text)}
                  value={this.state.searchAttendeText}
                  underlineColorAndroid='transparent'
                  placeholder="Search name here"
                    />

              </View>

          { this.state.ActivityIndicator == false ? 
        
        <View style={[styles.containerOverlay, styles.horizontalOverlay]}>
        <ActivityIndicator size="large" color="#666666" />
        </View>
      :  
      
              <View style = {styles.containerDua}>
              {/* <TouchableHighlight
              onPressOut = {this.searchRandomUser}
                  style = {{width:deviceWidth - 32, height:45,backgroundColor: 'green',justifyContent:"center",alignContent:"center"}}>
                        <Text style = {{fontSize:22,color: 'white',fontWeight: 'bold',textAlign:"center"}}>Reload Data</Text>
                  </TouchableHighlight> */}


              <FlatList
                data={this.state.data}
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.isFetching}
                keyExtractor = { (item, index) => index.toString() }
                renderItem={({item}) =>
                <View style = {styles.ContainerView}>
                <Content>
                <List>
                <ListItem avatar noBorder>
                  <Left>
                    <Thumbnail source={{ uri: 'https://eventapp.dexagroup.com/apiget_file?fd='+ item.image }} />
                  </Left>
                  <Body>
                    <Text>{item.audience_name}</Text>
                    <Text note>{item.email}</Text>
                  </Body>
                  <Right>
                    <Text note>{moment(item.waktubarcode, "HH:mm:ss").format("hh:mm A")}</Text>
                  </Right>
                </ListItem>
                </List>
                </Content>

                </View>
                }
                />
          </View> }

      </Container>
            )
          }

    }


  const mapStateToProps = state => ({
    reportData: state.reportData,
  });

  export default connect(mapStateToProps)(Dashboard);