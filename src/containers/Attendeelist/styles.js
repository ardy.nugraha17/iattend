import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';
import {
  Dimensions

} from 'react-native'
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Common.whiteColor,
  },
  aboutText: {
    padding: 20,
    paddingTop: 0,
  },
  textStyle: {
    fontSize: 16,
    marginTop: 20,
  },
  viewWrapper: {
    // flex: 9,
    flex:1
  },

  containerCamera: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: '#F5FCFF',
  // },
  preview: {
    width: '100%',
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20
  },
  containerDua: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:22, 
    marginBottom:22
  },
  containerTiga: {
    // flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    // marginTop:22, 
    // marginBottom:22,
    // backgroundColor:'#F5F5F5',
    borderBottomWidth:2,
    borderBottomColor:'#ececec',
    paddingLeft: 22,
    paddingRight: 22,
    paddingTop:22,
    paddingBottom: 22,
  },
  containerEmpat: {
    // flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    // marginTop:22, 
    // marginBottom:22,
    backgroundColor:'#F5F5F5',
    paddingLeft: 22,
    paddingRight: 22,
    paddingTop:22,
    paddingBottom: 22,
    // height:100,
    // flexDirection: "row",
    flexWrap: "wrap",
  },
  ContainerView:
  {
      // backgroundColor:'grey',
      marginBottom:0,
      paddingVertical:10,
      // backgroundColor: '#F5F5F5',

      borderBottomWidth:0.5,
      // borderBottomColor:'grey',
      borderBottomColor:'#ececec',
      width: deviceWidth-40,
      marginLeft:20,
      marginRight:20,
      marginTop:0,
      flexDirection:'row'
  },
  TextInputStyleClass:{

    // fontSize: 16,
    // textAlign: 'left',
    paddingLeft:22,
    // height: 50,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderRadius: 7 ,
    backgroundColor : "#ffffff",
    // marginTop: 10,
   
   },
   containerOverlay: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontalOverlay: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
});

module.exports = styles;
