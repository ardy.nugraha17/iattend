import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Common.whiteColor,
  },
  body: {
    flex: 1,
    backgroundColor: '#F2F2F2',
  },
  modalStyle: {
		position:'absolute',
		backgroundColor:'#000000af',
		left:0,
		top:0,
		bottom:0,
		right:0,
		alignItems:'center',
		justifyContent:'center'
	},
  modalContainer: {
    padding: 30,
    height: Common.deviceHeight / 1.2,
    width: Common.deviceWidth / 1.2,
    backgroundColor: Common.whiteColor,
  },
  textInput: {
    height: 100,
    borderColor: '#DCDCDC',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 15,
  },
  logInViewStyle: {

  },
  buttonStyle: {
    marginTop: 25,
    alignItems: 'center',
    backgroundColor: Common.lightGreen,
    borderRadius: 5,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 20,
  },
  profileLogoView: {
    padding: 20,
    paddingBottom: 10,
    alignSelf: 'center',
    borderBottomWidth: 0.3,
    borderColor: Common.lightDarkColor,
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  profileText: {
    padding: 10,
  },
  designation: {
    padding: 20,
    textAlign: 'center',
  },
  rowStyle: {
    padding: 20,
    paddingLeft: 80,
    paddingRight: 80,
  },
  colStyle: {
    alignItems: 'center',
  },
  bioParaViewStyle: {
    backgroundColor: Common.lightGray,
    padding: 20,
  },
  bioTitle: {
    fontSize: 16,
    color: Common.darkBlack,
  },
  biographyText: {
    paddingTop: 20,
    fontSize: 12,
  },
  showMore: {
    paddingTop: 8,
  },
});

module.exports = styles;
