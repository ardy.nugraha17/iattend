import React, { Component } from 'react'
import {
  View,
  StatusBar,
  Text,
  ScrollView,
  TouchableOpacity,
  Alert,
  Modal,
  AsyncStorage,
  TextInput
} from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import { Thumbnail, Row, Col } from 'native-base';
import Icon from "react-native-vector-icons/SimpleLineIcons";
import Icons from "react-native-vector-icons/Feather";
import styles from './styles';
import Header from '../../component/Header';
import Button from '../../component/Button';
import * as Helper from '../../common/helper';
import { profileData, eventImage, options } from '../../common/constant';
import DrawerView from '../../component/DrawerView';
import SideMenu from '../SideMenu'

class UserProfile extends Component {

  constructor(props) {
    super(props);
    this.eventRef = this.updateRef.bind(this, 'code');
    this.agendaRef = this.updateRef.bind(this, 'code');
    this.speakerRef = this.updateRef.bind(this, 'code');
    this.state = {
      modalVisible: false,
      text: '',
    };
  }

  closeDrawer = () => {
    this.drawer._root.close()
  }

  openDrawer = () => {
    this.drawer._root.open()
  }

  updateRef(name, ref) {
      this[name] = ref;
  }

  onEventSelect(data) {
    this.setState({
      event: data,
    })
  }
  onAgendaSelect(data) {
    this.setState({
      agenda: data,
    })
  }
  onSpeakerSelect(data) {
    this.setState({
      speaker: data,
    })
  }

  onButtonPress() {
    if(this.state.text !== '') {
      this.setState({modalVisible: false})
      Helper._commonAlert(Alert, "post successfully", this, "NotesList")
    } else {
      alert("Enter comments")
    }
  }

  body() {
    return (
      <ScrollView style={styles.body}>
        <View style={styles.profileLogoView}>
          <Thumbnail round large source={eventImage} />
          <Text style={styles.profileText}>{profileData.name}</Text>
          <Text style={styles.designation}>{profileData.designation}</Text>
        </View>
        <Row style={styles.rowStyle}>
          <Col>
            <TouchableOpacity style={styles.colStyle} onPress={() => this.setState({ modalVisible: true })}>
              <Icon name='note' size={20} />
              <Text>Note</Text>
            </TouchableOpacity>
          </Col>
          <Col>
            <TouchableOpacity style={styles.colStyle}>
              <Icons name='bookmark' size={20} />
              <Text>Bookmark</Text>
            </TouchableOpacity>
          </Col>
        </Row>
        <View style={styles.bioParaViewStyle}>
          <Text style={styles.bioTitle}>Bio</Text>
          <Text style={styles.biographyText}>{profileData.biography}</Text>
          {!this.state.showMore && <Text style={styles.showMore} onPress={() => this.setState({showMore: true})}>Show More</Text>}
          {this.state.showMore && <Text style={styles.biographyText}>{profileData.biography}</Text>}
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => this.setState({modalVisible: false})}>
            <View style={styles.modalStyle}>
              <View style={styles.modalContainer}>
                <Dropdown
                  ref={this.eventRef}
                  label="Select Event"
                  data={options}
                  value={this.state.event}
                  onChangeText={data => this.onEventSelect(data)}
                />
                <Dropdown
                  ref={this.agendaRef}
                  label="Select Agenda"
                  data={options}
                  value={this.state.agenda}
                  onChangeText={data => this.onAgendaSelect(data)}
                />
                <Dropdown
                  ref={this.speakerRef}
                  label="Select Speaker"
                  data={options}
                  value={this.state.speaker}
                  onChangeText={data => this.onSpeakerSelect(data)}
                />
                <TextInput
                  style={styles.textInput}
                  onChangeText={text => this.setState({ text })}
                  multiline
                  numberOfLines={4}
                  value={this.state.text}
                  underlineColorAndroid="#ffffff"
                  placeholder="Your comment"
                />
                <View style={styles.logInViewStyle}>
                  <Button
                    buttonTextStyle={styles.buttonTextStyle}
                    text="Submit"
                    style={styles.buttonStyle}
                    onPress={() => this.onButtonPress()}
                  />
                </View>
              </View>
            </View>
        </Modal>
      </ScrollView>
    )
  }

  render () {
    return (
      <DrawerView
        onClose={() => this.closeDrawer()}
        referVar={(ref) => { this.drawer = ref; }}
        content={<SideMenu {...this.props} />}
      >
        <View style={styles.container}>
          <StatusBar
             hidden={true}
           />
          <Header
             headerLeft={{padding: 10}}
             onLeftIconClick={() => this.openDrawer()}
             leftIcon="ios-menu"
             title={profileData.name}
           />
           {this.body()}
        </View>
      </DrawerView>
    )
  }
}
export default UserProfile;
