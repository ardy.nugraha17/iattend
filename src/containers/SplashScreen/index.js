import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { connect } from 'react-redux';
import * as Helper from '../../common/helper';
import styles from './styles';
import { versionDevice } from '../../common/constant';
var DeviceInfo = require('react-native-device-info');
import axios from 'axios';

class SplashScreen extends Component {

  componentWillMount(){

    console.log("Program jalan", 
        DeviceInfo.getUniqueID(),
        DeviceInfo.getTimezone(),
        DeviceInfo.getDeviceId(), 
        DeviceInfo.getModel(),
        DeviceInfo.isTablet(),
        DeviceInfo.getPhoneNumber(),
        DeviceInfo.getSystemName(),
        DeviceInfo.getSystemVersion()
     )

    const value = {
      versionDevice,
      'getUniqueID':DeviceInfo.getUniqueID(),
      'getTimezone':DeviceInfo.getTimezone(),
      'getDeviceId':DeviceInfo.getDeviceId(), 
      'getModel':DeviceInfo.getModel(),
      'isTablet':DeviceInfo.isTablet(),
      'getPhoneNumber':DeviceInfo.getPhoneNumber(),
      'getSystemName':DeviceInfo.getSystemName(),
      'getSystemVersion':DeviceInfo.getSystemVersion()
    }
    this.props.dispatch({
      type: 'CHECK_VERSION',
      payload: value
    });  
  

  }

  componentWillReceiveProps(nextProps){
    const { checkversionData } = nextProps;
    // console.log("program " + nextProps + JSON.stringify(checkversionData) + checkversionData.checkversion );
    
    console.log("program " + checkversionData.checkversion.statusDevices );
    
  }


  componentDidMount() {

    setTimeout(() => {
      Helper.resetNavigation(this, 'Login', null);
    }, 2000);
  }


  render() {
    return (
      // <View style={styles.container} />
      <View
        style={styles.container}
      >
      <Image
        source={require('../../images/logoIAttend.png')}
        style={styles.paragraph}
      >
      </Image>
    </View >
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(SplashScreen);
