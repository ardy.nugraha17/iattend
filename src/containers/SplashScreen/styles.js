import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor:Common.lightGreen,
    backgroundColor:'#ffffff',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  splashStyle: {
    height: Common.deviceHeight,
    width: Common.deviceWidth,
    resizeMode: 'stretch',
  },
  image: {
    flexGrow:1,
    height:null,
    width:null,
    alignItems: 'center',
    justifyContent:'center',
  },
  paragraph: {
    textAlign: 'center',
    width:'30%',
    height:'30%'
  },
});

module.exports = styles;
