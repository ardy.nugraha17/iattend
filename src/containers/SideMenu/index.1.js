import React, { Component } from 'react';
import { TouchableOpacity, ScrollView, Text, AsyncStorage } from 'react-native';
import { View, Icon } from 'native-base';
import styles from './styles';
import * as Constant from '../../common/constant';
import * as Helper from '../../common/helper';
import DrawerMenu from '../../component/DrawerMenu';
import DrawerItemView from '../../component/DrawerItemView';

export default class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
      expand: true,
      loginPressed: false,
      accessToken: '',
    };
    this.navigateTo = this.navigateTo.bind(this);
    this.myItemsMenu = this.myItemsMenu.bind(this);
  }

  componentWillMount() {

    // console.log("ss anya",this.props.navigation.state.routeName);

    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        this.setState({
          accessToken: JSON.parse(data).token,
        })
      }
    }).done();
  }

  navigateTo(screen) {    
    
    if(screen) {
      Helper.navigateToPage(this, screen);
    } else {
      AsyncStorage.setItem("userData", "");
      Helper.resetNavigation(this, 'Home');
    }
  }

  drawerMenu() {
    return (
      Constant.drawerMain.map((data, i) => (
        <DrawerMenu
          key={i}
          iconName={data.icon}
          text={data.text}
          menuStyle={styles.drawerMenuStyle}
          textStyle={styles.drawerMenuText}
          onPress={() => this.navigateTo(data.screen)}
        />
      ))
    );
  }

  onExpandingHeader() {
    this.setState({ clicked: !this.state.clicked });
  }

  onExpandingEventHeader() {
    this.setState({ expand: !this.state.expand });
  }

  myItemsMenu() {
    return (
      <DrawerItemView onPress={() => this.onExpandingHeader()} menuText="My Items" iconChange={this.state.clicked} >
        {this.myItemsListMenu()}
      </DrawerItemView>
    );
  }

  myItemsListMenu() {
    return (
      Constant.myItems.map((data, i) => (
        <DrawerMenu key={i}
          iconName={data.icon}
          text={data.menuOption}
          menuStyle={styles.myItemsMenuStyle}
          textStyle={styles.drawerMenuItemsText}
          onPress={() => this.navigateTo(data.screen)}
        />
      ))
    );
  }

  eventGuideMenu() {
    return (
      <DrawerItemView onPress={() => this.onExpandingEventHeader()} menuText="Event Guides" iconChange={this.state.expand} >
        {this.eventGuideListItems()}
      </DrawerItemView>
    );
  }

  eventGuideListItems() {
    return (
      Constant.eventGuide.map((data, i) => (
        <DrawerMenu
          key={i}
          iconName={data.icon}
          text={data.menuOption}
          menuStyle={styles.myItemsMenuStyle}
          textStyle={styles.drawerMenuItemsText}
          onPress={() => this.navigateTo(data.screen)}
        />
      ))
    );
  }

  loginFeature() {
    return (
      <TouchableOpacity style={styles.loginFeature} onPress={() => this.setState({ loginPressed: !this.state.loginPressed })}>
        <Text>{Constant.loginFeature}</Text>
        <Icon name="md-arrow-dropdown" />
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.drawerContainer}>
        {(this.state.accessToken === '' ) ? this.loginFeature(): null}
        {this.state.loginPressed ?
          <View style={styles.loginSetting}>
            <DrawerMenu text="Login" textStyle={styles.textWrapperStyle} onPress={() => this.navigateTo('Login')} />
            <View style={styles.separatorStyle} />
            <DrawerMenu text="Register" textStyle={styles.textWrapperStyle} onPress={() => this.navigateTo('Register')} />
            <View style={styles.separatorStyle} />
            <DrawerMenu text="Settings" textStyle={styles.textWrapperStyle} />
          </View> :
          <ScrollView>
            {this.state.accessToken !=='' ? <View>
              {this.drawerMenu()}
              <View style={styles.separatorStyle} />
              {this.myItemsMenu()}
              <View style={styles.separatorStyle} />
            </View>: <View/>}
            {this.eventGuideMenu()}
          </ScrollView>
        }
      </View>
    );
  }
}
