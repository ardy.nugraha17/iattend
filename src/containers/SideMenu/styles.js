import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({
  drawerContainer: {
    // backgroundColor: Common.whiteColor,
    backgroundColor:'#2c3240',
    flex: 1
  },
  drawerContainerHeader: {
    // backgroundColor: Common.whiteColor,
    backgroundColor:'#22262f',
    padding:10,
    width:'100%',
  },
  teksColorHeader:{
    color:'#e7e7e9'
  },
  teksColorSubHeader:{
    color:'#808a94'
  },
  loginFeature: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Common.lightGray,
    padding: 15,
    flexDirection: 'row'
  },
  drawerMenuStyle: {
    padding: 15,
    flexDirection: 'row'
  },
  drawerMenuText: {
    paddingLeft: 8,
    marginTop: 5,
  },
  myItemsMenuStyle: {
    padding: 8,
    paddingLeft: 20,
    flexDirection: 'row',
    borderBottomWidth:1,
    borderBottomColor: '#1b2531',
  },
  myItemsIconStyle:{
    color:'#eab936',
    padding: 8,
    marginRight: 20,
    fontSize:20
  },
  myItemsIconStyleClose:{
    color:'#eab936',
    padding: 8,
    marginRight: 22,
    fontSize:30
  },
  myItemsIconStyleAttend:{
    color:'#eab936',
    padding: 8,
    marginRight: 17,
    fontSize:20
  },
  drawerMenuItemsText: {
    color:'#808a93',
    padding: 8,
    width:'100%',
    marginTop: 5
  },
  menuSliderText: {
    fontWeight: 'bold'
  },
  loginSetting: {
    flex: 9
  },
  textViewWrapper: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between'
  },
  textWrapperStyle: {
    padding: 10,
  },
  separatorStyle: {
    borderBottomWidth: 0.5
  }
});

module.exports = styles;
