import React, { Component } from 'react';
import { TouchableOpacity, ScrollView, Text, AsyncStorage } from 'react-native';
import { View, Icon } from 'native-base';
import styles from './styles';
import * as Constant from '../../common/constant';
import * as Helper from '../../common/helper';
import DrawerMenu from '../../component/DrawerMenu';
import DrawerItemView from '../../component/DrawerItemView';
import RNExitApp from 'react-native-exit-app';

export default class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
      expand: true,
      loginPressed: false,
      accessToken: '',
    };
    this.navigateTo = this.navigateTo.bind(this);
    this.myItemsMenu = this.myItemsMenu.bind(this);
  }

  componentWillMount() {

    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        this.setState({
          accessToken: JSON.parse(data).token,
        })
      }
    }).done();

    
  }

  navigateTo(screen) {    
    
    if(screen) {

      if(this.props.navigation.state.params){
        // console.log("sidemenu trace",this.props);

        this.props.onClose()

        let obj={
          eventAddress:this.props.navigation.state.params.eventAddress,
          eventTitle:this.props.navigation.state.params.eventTitle,
          eventDescription:this.props.navigation.state.params.eventDescription,
          source:this.props.navigation.state.params.source,
          eventId:this.props.navigation.state.params.eventId,
          lat_long:this.props.navigation.state.params.lat_long,
          eventDescrip:this.props.navigation.state.params.eventDescrip
        };
  
        Helper.navigateToPage(this, screen, obj);

      }else{  
        Helper.navigateToPage(this, screen);
      }

    } else {
      AsyncStorage.setItem("userData", "");
      Helper.resetNavigation(this, 'Home');
    }
  }

  drawerMenu() {
    return (
      Constant.drawerMain.map((data, i) => (
        <DrawerMenu
          key={i}
          iconName={data.icon}
          text={data.text}
          menuStyle={styles.drawerMenuStyle}
          textStyle={styles.drawerMenuText}
          onPress={() => this.navigateTo(data.screen)}
        />
      ))
    );
  }

  onExpandingHeader() {
    this.setState({ clicked: !this.state.clicked });
  }

  onExpandingEventHeader() {
    this.setState({ expand: !this.state.expand });
  }

  myItemsMenu() {
    return (
      <DrawerItemView onPress={() => this.onExpandingHeader()} menuText="My Items" iconChange={this.state.clicked} >
        {this.myItemsListMenu()}
      </DrawerItemView>
    );
  }

  myItemsListMenu() {
    return (
      Constant.myItems.map((data, i) => (
        <DrawerMenu key={i}
          iconName={data.icon}
          text={data.menuOption}
          menuStyle={styles.myItemsMenuStyle}
          textStyle={styles.drawerMenuItemsText}
          onPress={() => this.navigateTo(data.screen)}
        />
      ))
    );
  }

  eventGuideMenu() {
    return (
      <DrawerItemView onPress={() => this.onExpandingEventHeader()} menuText="Event Guides" iconChange={this.state.expand} >
        {this.eventGuideListItems()}
      </DrawerItemView>
    );
  }

  eventListFormLeft(){
    return(
      // <View style={styles.loginSetting}>
      <View>
      <DrawerMenu text="Login" textStyle={styles.textWrapperStyle} onPress={() => this.navigateTo('Login')} />
      <View style={styles.separatorStyle} />
      <DrawerMenu text="Register" textStyle={styles.textWrapperStyle} onPress={() => this.navigateTo('Register')} />
      <View style={styles.separatorStyle} />
      <DrawerMenu text="Settings" textStyle={styles.textWrapperStyle} />
      </View>
    );
  }

  eventGuideListItems() {
    return (
      Constant.eventGuide.map((data, i) => (
        <DrawerMenu
          key={i}
          iconName={data.icon}
          text={data.menuOption}
          menuStyle={styles.myItemsMenuStyle}
          textStyle={styles.drawerMenuItemsText}
          onPress={() => this.navigateTo(data.screen)}
        />
      ))
    );
  }

  loginFeature() {
    return (
      <TouchableOpacity style={styles.loginFeature} onPress={() => this.onPressSetState()}>
        <Text>{Constant.loginFeature}</Text>
        <Icon name="md-arrow-dropdown" />
      </TouchableOpacity>
    );
  }

  onPressEventBeforeLoginUser(param){
    if(param == "Home"){
      Helper.resetNavigation(this, 'Home')
    }
    else if (param == 'Exit'){
      RNExitApp.exitApp();
    }else{
      this.navigateTo(param)
    }
    
  }

  onPressSetState(){
    this.setState({ loginPressed: !this.state.loginPressed })
    // console.log("ss anya",this.state.loginPressed);
  }


  eventLoginUser() {
    return (
      <View>
        {(this.state.accessToken === '' ) ? this.loginFeature(): null}
        {this.state.loginPressed == true ?
          this.eventListFormLeft()
           :
          <ScrollView>
            {this.state.accessToken !=='' ? <View>
              {this.drawerMenu()}
              <View style={styles.separatorStyle} />
              {this.myItemsMenu()}
              <View style={styles.separatorStyle} />
            </View>: <View/>}
            {this.eventGuideMenu()}
          </ScrollView>
        }
      </View>
    );
  }

  eventBeforeLoginUser() {
    return (
      Constant.eventBeforeLoginUser.map((data, i) => (
        <DrawerMenu
          key={i}
          iconName={data.icon}
          text={data.menuOption}
          menuStyle={styles.myItemsMenuStyle}
          textStyle={styles.drawerMenuItemsText}
          onPress={() => this.onPressEventBeforeLoginUser(data.screen)}
        />
      ))
    );
  }  

  render() {
    return (
      <View style={styles.drawerContainer}>

        {this.props.navigation.state.routeName == "Home" 
        
          ? 
          this.eventBeforeLoginUser()
          :
          // this.eventLoginUser()
          this.eventBeforeLoginUser()
        }
      </View>
    );
  }

  
}
