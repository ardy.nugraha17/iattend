import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({
  drawerContainer: {
    backgroundColor: Common.whiteColor,
    flex: 1
  },
  loginFeature: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Common.lightGray,
    padding: 15,
    flexDirection: 'row'
  },
  drawerMenuStyle: {
    padding: 15,
    flexDirection: 'row'
  },
  drawerMenuText: {
    paddingLeft: 8,
    marginTop: 5,
  },
  myItemsMenuStyle: {
    padding: 15,
    flexDirection: 'row'
  },
  drawerMenuItemsText: {
    paddingLeft: 8
  },
  menuSliderText: {
    fontWeight: 'bold'
  },
  loginSetting: {
    flex: 9
  },
  textViewWrapper: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between'
  },
  textWrapperStyle: {
    padding: 10,
  },
  separatorStyle: {
    borderBottomWidth: 0.5
  }
});

module.exports = styles;
