import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage,
  TouchableHighlight, Image, ScrollView
} from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';
import PieChart from 'react-native-pie-chart';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';


const eventListEvaluation = [
  {id:1, name:'David Beckham'},
  {id:2, name:'Paolo Maldini'},
  {id:3, name:'Michael Jordan'},
  {id:4, name:'Scotie Pippen'},
  {id:5, name:'evaluation lima'},
  {id:6, name:'evaluation enam'},
  {id:7, name:'evaluation tujuh'},
  {id:8, name:'evaluation delapan'},
  {id:9, name:'evaluation sembilan'},
  {id:10, name:'evaluation sepuluh'},
  {id:11, name:'evaluation empat'},
  {id:12, name:'evaluation empat'},
  {id:13, name:'evaluation empat'},
  {id:14, name:'evaluation empat'},
  {id:15, name:'evaluation empat'},
  {id:16, name:'evaluation empat'},
  {id:17, name:'evaluation empat'},
  {id:18, name:'evaluation empat'},
  {id:19, name:'evaluation empat'},
  {id:20, name:'evaluation empat'},
  {id:21, name:'evaluation empat'},
  {id:22, name:'evaluation empat'},
  {id:23, name:'evaluation empat'},
  {id:24, name:'evaluation empat'},
];


const eventListEvaluationAudience = [
  {id:1, name:'Isu-isu yang didiskusikan menjawab permasalahan', answer: 'Sangat tidak setuju'},
  {id:2, name:'Saya Berpartisipasi aktif dalam pelatihan', answer: 'Maybe'},
  {id:3, name:'Fasilitas dan suasana tempat pelatihan mendukung saya belajar', answer:'Setuju'},
  {id:4, name:'Ruangan sangat nyaman untuk event ini', answer:'Setuju'},
  {id:5, name:'Tempatnya nyaman dan baik', answer:'Setuju'},
  {id:6, name:'Saya berpatisipasi',answer:'Setuju'},
  {id:7, name:'Pembicara event ini sangat menguasai materi',answer:'Setuju'},
];


const chart_wh = 200
const series = [123, 321, 123]
const sliceColor = ['#F44336','#2196F3','#FFEB3B']

class AddEvaluation extends Component {


  constructor(props) {
    super(props);

    this.state = {
      listEvaluation: eventListEvaluation,
      listEvaluationAudience: eventListEvaluationAudience,
      dialogVisible:false,
      statusQuestion:''
    };

    this.onButtonViewPress = this.onButtonViewPress.bind(this);
    this.onChangeText = this.onChangeText.bind(this);

  }

  // onChangeText(text) {
  //   ['name', 'code', 'sample', 'typography']
  //     .map((name) => ({ name, ref: this[name] }))
  //     .filter(({ ref }) => ref && ref.isFocused())
  //     .forEach(({ name, ref }) => {
  //       this.setState({ [name]: text });
  //     });
  // }

  onChangeText(text) {
    this.setState({statusQuestion:text});
  }


  componentWillMount(){
    console.log("register event",this.props.navigation.state.params.item);
    this.setState({
      name:this.props.navigation.state.params.item.name,
      email:this.props.navigation.state.params.item.email,
      id:this.props.navigation.state.params.item.id,
      audience_id:this.props.navigation.state.params.item.audience_id,
      agendaId:this.props.navigation.state.params.item.agendaId,
      answer_value:this.props.navigation.state.params.item.answer_value,
      ceated_date:this.props.navigation.state.params.item.ceated_date,
      lastupdate:this.props.navigation.state.params.item.lastupdate,
      image:this.props.navigation.state.params.item.image,
      status:this.props.navigation.state.params.item.status
    })
  }

  componentDidMount(){
    
  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  
  onButtonViewPress(){
    console.log("onButtonViewPress");
    this.setState({
      dialogVisible:true
    })    

  }


  onButtonCloseDialog(){
    console.log("onButtonViewPress");
    this.setState({
      dialogVisible:false
    })    

  }


  submitButton(){
    
    console.log("submitButton");
    AsyncStorage.getItem("userData").then((data) => {

        console.log("AsyncStorage");

        const statusQuestion = this.state.statusQuestion;
      
        // ApiCaller('addnote', 'post', JSON.parse(data).token, {
        //   text: this.state.text, heading:titleNote, id:JSON.parse(data).audienceid }).then(response =>{
        //     console.log("ss anya balikan addnote",response);
        //   if(response.affectedRows == 1){
          
        //   Helper.navigateToPage(this, 'Home')
        //   }
        // })


        ApiCaller('editquestionmoderator', 'post', JSON.parse(data).token, {
          moderatorId : JSON.parse(data).audienceid,
          statusQuestion: statusQuestion,
          id:this.state.id,
          agendaId:this.state.agendaId
          
      
        }).then(response =>{

          console.log("editquestionmoderator", response);

          if(response == "sukses"){

            // this.props.dispatch({
            //   type: 'FETCH_EVENT_LIST',
            // });


            console.log("componentDidMount",JSON.parse(data))

            const agendaId = JSON.parse(data).agendaId;

            const value = {
              'agendaId': JSON.parse(data).agendaId,
              'moderatorId': JSON.parse(data).audienceid
            }
        
        
            this.props.dispatch({
              type: 'FETCH_EVENT_LIST',
              payload: value
            });

            ToastAndroid.show('success to submit', ToastAndroid.SHORT);
            this.props.navigation.navigate('Home');
          }else{
            ToastAndroid.show('Failed to submit', ToastAndroid.SHORT);
          }

        
        })

      
    }).done();





  
    // this.props.navigation.navigate('Login');
  }




  render() {


    // const { attendees } = this.props.attendeesData;
    // const {eventAddress, eventTitle, eventDescription, source,} = this.props.navigation.state.params;

    const dataQuestion = [{ value: 'pending', label :'Pending'}, { value: 'answered', label : 'Answered'},];


    return (
      
        <Container style={styles.container}>

          <Header
          headerContainer={styles.headerContainerStyle}
          headerTextStyle={styles.headerTextStyle}
          headerLeft={{padding: 10}}
          onLeftIconClick={() => this.props.navigation.goBack(null)}
          // onRightIconClick={() => this.onPressShare()}
          leftIcon="md-arrow-back"
          // rightIcon="share"
          title="Detail Question"
          />
          <Content style={{padding:20}}>

          <View style={styles.MainContainerMoveOrder}>


              <List thumbnail>
                  <ListItem thumbnail
                  style={{paddingLeft:0, marginBottom:40, marginLeft:0}}
                  >
                    <Left>
                      <Thumbnail source={{ uri: 'http://eventapp.dexagroup.com/apiget_file?fd='+this.state.image }} />
                      {/* <IconFontAwesome size={36} active name="hourglass-half" /> */}
                    </Left>
                    <Body>
                      <Text>{this.state.name}</Text>
                      <Text note numberOfLines={1}>{this.state.email}</Text>
                    </Body>
                    <Right>
                      <Button 
                        transparent
                        >
                        <IconFontAwesome size={24} active name="user" />
                      </Button>
                    </Right>
                  </ListItem>
              </List>

          </View>

          <View 
          style={{height:400, flexDirection:'row', 
          borderBottomColor:'#e9ecf1', borderBottomWidth:1, paddingBottom:20,
          }}
          >


          <ScrollView
          style={{paddingRight:20}}
          >

              <Text style={{flexWrap:'wrap', fontSize:14, lineHeight:18}}>
              {this.state.answer_value}
              </Text>

          </ScrollView>

          </View>


          <View style={{height:50, marginTop:20, marginBottom:20}}>

              {/* <Label
              style={{ fontSize:14, fontWeight:'bold'}}
              >Status question</Label> */}
              <Dropdown
                labelFontSize={14}
                label= "Status question"
                // labelHeight={0}
                data={dataQuestion}
                value = {this.state.status}
                // containerStyle={{ borderWidth: 1,borderColor: '#ececec',}} 
                onChangeText={this.onChangeText}
              />

          </View>

          

          </Content>

          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

            <Row style={{ marginTop: 0, marginBottom: 0 }}>

                    <Button 
                      light
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.props.navigation.goBack(null)}
                      
                    >
                      <Text>Back</Text>
                    </Button>

                    <Button 
                      success
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.submitButton()}
                      
                    >
                      <Text>Submit</Text>
                    </Button>

            </Row>

          </Grid>

          </Footer>
        </Container>
    );
  }
} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(AddEvaluation);