import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  Grid,
  Col,
  Row,
  // CheckBox,
  // Radio
  Footer
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, AsyncStorage, TextInput, BackHandler } from 'react-native';
import styles from '../Question/styles';

import Header from '../../component/Header';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';

import RadioButton from 'radio-button-react-native';
import ApiCaller from '../../common/apiCaller';

class Detail extends Component {


  constructor(props) {

    super(props);
    this.state = {
      questionExist:false,
      text:'',
      accessToken:'',
      audienceid:''
    }
  }


  componentDidMount(){
    console.log("componentDidMount question", this.props);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  goBack(){
    this.props.navigation.goBack(null);
  }


  componentWillReceiveProps(next){
    if(next.questionData.question.exist == true){
      this.setState({
        questionExist:  true
      }) 
    }
  }

  componentWillMount() {
    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        this.setState({
          accessToken: JSON.parse(data).token,
          audienceid: JSON.parse(data).audienceid
        })
      }
    }).done();
  }

  onPressClose(){
    this.props.navigation.goBack(null);
  }


  onPressSubmit(){


    AsyncStorage.getItem("userData").then((data) => {

      if(this.state.text == ''){
        ToastAndroid.show('Please write some question', ToastAndroid.SHORT);
        return;
      }

      ApiCaller('mobileIattend_question_insert', 'post', this.state.accessToken, {
        audienceid : this.state.audienceid,
        eventId : this.props.navigation.state.params.navigation.state.params.navs.state.params.eventId,
        agendaId : this.props.navigation.state.params.navigation.state.params.eventId,
        anserwer_value : this.state.text,
    
      }).then(response =>{

          console.log("mobileIattend_question_insert", response);

        if(response.insert == true){
          ToastAndroid.show('Question has been submit', ToastAndroid.SHORT);
          this.props.navigation.goBack(null);

        }else{
          ToastAndroid.show('Question failed to submit', ToastAndroid.SHORT);

        }
      
      })


    }).done();

  }


  footer(){

    if(this.state.questionExist == false){

      return(

        <Grid>

        <Row style={{ marginTop: 0, marginBottom: 0 }}>

        <Button 
          onPress ={() => this.onPressClose()}
          light
          style = {{
            flexDirection: "row",
            flexWrap: "wrap",
            flex: 1,
            justifyContent: "center",
            borderWidth:0,
            height:55,
            borderRadius:0,
          }}
          
        >
          <Text>Close</Text>
        </Button>

        <Button 
          onPress={() => this.onPressSubmit()}
          success
          style = {{
            flexDirection: "row",
            flexWrap: "wrap",
            flex: 1,
            justifyContent: "center",
            borderWidth:0,
            height:55,
            borderRadius:0,
          }}
          
        >
          <Text>Submit</Text>
        </Button>

        </Row>

        </Grid>

        )
    }else{

      return (

      <Grid>

      <Row style={{ marginTop: 0, marginBottom: 0 }}>

      <Button 
        onPress ={() => this.onPressClose()}
        success
        style = {{
          flexDirection: "row",
          flexWrap: "wrap",
          flex: 1,
          justifyContent: "center",
          borderWidth:0,
          height:55,
          borderRadius:0,
        }}
        
      >
        <Text>Close</Text>
      </Button>

      </Row>

      </Grid>

      )
    }

  }

  render() {
   
    const speaker= this.props.navigation.state.params;
    var items = this.state.listPollingChoose;
    const {navigation} = this.props;

    return (
        <Container style={styles.container}>

            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Question"
            />
  
          <Content>
          {this.state.questionExist == false ?  
          <View style={{backgroundColor:'#ececec', padding:20}}>
             <Text 
             style={{textAlign:'center', alignContent:'center',}}>Please input your question here :
             </Text>
          </View>
          : null 
          }

          <View style={styles.aboutText}>

          {this.state.questionExist == false ?
                <View style={styles.textAreaContainer}>
                <TextInput
                autoFocus={true}
                style={{textAlignVertical: 'top',fontSize:15,lineHeight:27}}
                multiline={true}
                numberOfLines={1000}
                value={this.state.text}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                onChangeText={(value) => this.setState({text:value})}
                value={this.state.text}/>
                </View>
                :

                <View style={{padding:20, backgroundColor:'#ececec'}}>
                <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold'}}>Mohon maaf. Anda sebelumnya sudah pernah memberikan pertanyaan di event ini.</Text>
                </View>
          }

           </View>
             
          </Content>
  
          <Footer style={{ backgroundColor: "transparent" }}>

            {this.footer()}              

          </Footer>

        </Container>
      );


  }
}


const mapStateToProps = state => ({
  questionData: state.questionData,
});

export default connect(mapStateToProps)(Detail);