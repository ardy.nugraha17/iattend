import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage,
  TouchableHighlight, Image
} from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';
import PieChart from 'react-native-pie-chart';


const eventListEvaluation = [
  {id:1, name:'David Beckham'},
  {id:2, name:'Paolo Maldini'},
  {id:3, name:'Michael Jordan'},
  {id:4, name:'Scotie Pippen'},
  {id:5, name:'evaluation lima'},
  {id:6, name:'evaluation enam'},
  {id:7, name:'evaluation tujuh'},
  {id:8, name:'evaluation delapan'},
  {id:9, name:'evaluation sembilan'},
  {id:10, name:'evaluation sepuluh'},
  {id:11, name:'evaluation empat'},
  {id:12, name:'evaluation empat'},
  {id:13, name:'evaluation empat'},
  {id:14, name:'evaluation empat'},
  {id:15, name:'evaluation empat'},
  {id:16, name:'evaluation empat'},
  {id:17, name:'evaluation empat'},
  {id:18, name:'evaluation empat'},
  {id:19, name:'evaluation empat'},
  {id:20, name:'evaluation empat'},
  {id:21, name:'evaluation empat'},
  {id:22, name:'evaluation empat'},
  {id:23, name:'evaluation empat'},
  {id:24, name:'evaluation empat'},
];


const eventListEvaluationAudience = [
  {id:1, name:'Isu-isu yang didiskusikan menjawab permasalahan', answer: 'Sangat tidak setuju'},
  {id:2, name:'Saya Berpartisipasi aktif dalam pelatihan', answer: 'Maybe'},
  {id:3, name:'Fasilitas dan suasana tempat pelatihan mendukung saya belajar', answer:'Setuju'},
  {id:4, name:'Ruangan sangat nyaman untuk event ini', answer:'Setuju'},
  {id:5, name:'Tempatnya nyaman dan baik', answer:'Setuju'},
  {id:6, name:'Saya berpatisipasi',answer:'Setuju'},
  {id:7, name:'Pembicara event ini sangat menguasai materi',answer:'Setuju'},
];


const chart_wh = 200
const series = [123, 321, 123]
const sliceColor = ['#F44336','#2196F3','#FFEB3B']

class AddEvaluation extends Component {


  constructor(props) {
    super(props);

    this.state = {
      listEvaluation: eventListEvaluation,
      listEvaluationAudience: eventListEvaluationAudience,
      dialogVisible:false
    };

    this.onButtonViewPress = this.onButtonViewPress.bind(this);

  }

  componentDidMount(){
    console.log("register event",this.props.navigation.state.params);
  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  
  onButtonViewPress(){
    console.log("onButtonViewPress");
    this.setState({
      dialogVisible:true
    })    

  }


  onButtonCloseDialog(){
    console.log("onButtonViewPress");
    this.setState({
      dialogVisible:false
    })    

  }


  LoopingBody() {


    return (
      <View>
        <Dialog 
        visible={this.state.dialogVisible} 
        title="Audience evaluation form"
        onTouchOutside={() => this.setState({dialogVisible: false})} 
        >
        <View 
        style={{height:300}}
        >

                  {
                    this.state.listEvaluationAudience != '' ? 

                    <List 
                    style={{paddingLeft:0, marginLeft:0,paddingRight:10}}
                    dataArray={this.state.listEvaluationAudience}
                    renderRow={(item) =>
                      //  console.log("trace PollingDetail bawah dua",item.id)
                      <ListItem
                      style={{paddingLeft:0, marginLeft:0, paddingRight:10}}>
                      <Body>
                        <Text>{item.name}</Text>
                        <Text note numberOfLines={1}>Answer : {item.answer}</Text>
                      </Body>
                    </ListItem>
                    }>
                </List>
                      : null
                  }

        </View>

        <View style={{height:40, marginTop:10}}>
        <Button 
        onPress={() => this.onButtonCloseDialog()}
        block light>
            <Text>Close</Text>
        </Button>
        </View>

        </Dialog>
      </View>
    )
  }




  render() {


    // const { attendees } = this.props.attendeesData;
    // const {eventAddress, eventTitle, eventDescription, source,} = this.props.navigation.state.params;

    return (
      
        <Container style={styles.container}>


          {this.LoopingBody()}

          <Header
          headerContainer={styles.headerContainerStyle}
          headerTextStyle={styles.headerTextStyle}
          headerLeft={{padding: 10}}
          onLeftIconClick={() => this.props.navigation.goBack(null)}
          // onRightIconClick={() => this.onPressShare()}
          leftIcon="md-arrow-back"
          // rightIcon="share"
          title="Detail Question"
          />
          <Content contentContainerStyle={{ flex: 1 }}>


                    <List dataArray={this.state.listEvaluation} thumbnail>

                      <ListItem thumbnail noBorder>
                      <Left>
                        <Thumbnail small source={{ uri: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIPDw8PDxIQDw8PDw8PDw8PDw8PEA8PFREWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NECsZFRkrLSsrKystLSsrKysrLSsrKysrKystKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEBAAMBAQEAAAAAAAAAAAAAAQIEBQMGB//EADQQAQEAAQEFBQUHBAMAAAAAAAABAhEDBRJRkQQhMWFxIjJBgaFCUmKxwdHwEzNy4RQj8f/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A/W+Gcp0hwzlOkUVE4ZynSHDOU6RQE4ZynSHDOU6RQE4ZynSHDOU6RQE4ZynSHDOU6RQE4ZynSHDOU6RQE4ZynSHDOU6RQE4ZynSHDOU6RQE4ZynSHDOU6RQE4ZynSHDOU6RQE4ZynSHDOU6RQE4ZynSJwzlOkZICcM5TonDOU6KUGPDOU6Q4ZynRUBNJynSCgPQAAAAAAAAAAAAAAAAAAAAEASqgCACFEoAgD2AAAAAAAABM8pJbbpJ42gry23acMPeykvLxvRze17yuXds/Zx+99q/s0KDqbTe0+zjr55XT6PDLemfwmM+TRAbs3ptPw9Hrhva/axl9Lo5oDu7Ht+GXdrw3ll3fVtPmGz2btmWz8Lrj92+Hy5A7w8uzdpx2k1x+HjL4x6ggIAhQBBAEEoGoAPcAAAAAAAEzyklt7pO+1wu29ru0vLGeE/W+bY3t2nW/054Y+953k5wACgAAAAADLZbS42ZY3Sx3ex9qm0x18LPenK/s+fevZdvdnlMp4eGU5xB9CJjlLJZ4WawoCACIFAqCUDQNAGwAAAAAA8+07Xgwyy5Tu9fg9HP31npjjj96635A5NuvffG999UBQAAAAAAQAEAHW3RttcbhfHHvn+N/233D3bnw7XH8WuP86O4gJRAEEoGqUS0UViCNsAAAAAByN9X28Jyxv5uu5G+Z7eP+P6g54CgAAAACACACKgPTYZaZ4X8U/N9FXzmxnt4/5Y/m+iqCIqAiU1QBKVABNQG6AAAAAA5u+sO7DLlbOv8A46Tx7ZsePZ5Y/HTWesB88AoAAAgAIACAAgNjd+HFtcfL2uju1zdz7Luyz5+zP1dFAqFqAiWqxASiAgvy+oDeAAAAAAVAHF3n2fgz4p7uff6X4xpPpNvspnjccvC/S83A7RsLs8uHL5X4WcweQCiAAgACCAq7LZ3PKYzxt09PNjJrdJ32uz2Dsn9Oa33r4+U5INnZbOY4zGeEmilQBKIBWNWsdQKgloL/ADxRNQHRAAAAAAAAee32GO0nDlPS/GXnHoA4Ha+x5bP8WPwyn68ms+nrT2+7sMu+exfLw6A4iN7abrznhccp66X6vDLse0n2MvzB4I9/+JtPuZdHphu7aX4TH1oNNnstllndMZrfpPV0tluuT37xeU7o3sMJjNMZJOUBrdj7FNn332s+fwno2atS0ESrUBKlEtBKhqlBdWJUoGogDpgAAAAAA0e2bwmHs4+1l9Mf3BuZ5zGa5WSc60dtvXGd2EuXne6OVttrlndcrb+U9IwBuZbz2muvszy07mzst64335cfOd8/dyQH0OHacMvDLG/PR6avmSX1+VB9NXlntccfHKT1sfPXK871rEHa2u8sJ4a5Xynd1aW03nnb3aYzlpr1aSA6Wy3r9/H54/tW7se0Y5+7dfLwvR8+S6XWay853WA+k1Y1y+zbys7tp3z73xnrzdLHKWay6z4WeALUEoDG1axA1SlrEATT+aAOsAAAAqNHefauCcOPvZfTEHlvHt/jhhfLLKflHLQAEFAogAJQKgAiKxoAIBXv2Ttd2d543xn6xroD6LHOZSWXWXvlLXI3f2ngvDfdy+l5uqgJVtYgWsatrGga/wA1E1AdgAAAGO0zmMuV8JLa+d221ueVyvjb08nT3xttMccJ9rvvpHIABFAEABAEVAEolAqCAItYgVAoJXY3ft+PDS+9j3Xznwrjvfd+14c5yy9m/og7KUrG0DVFqABqA7AAAAOFvPacW1y/Dpj0ajPbZa5ZXnllfq8wAFEABKCUBDVKAgAJRAQEAtQQBNS1Ad/DLWS85L9FrX7Blrs8fLWdK90BABdf53CagOyAAxqpQfM5fFFy+KKCAAggCAAxWoAggFQ1TUCpqIBUtEARWNB1923/AK565fm2mru3+3PXJs1ASiAqsQHbpQBjUyAHzWXj80BREqgMSgCJQBKADGlABjQBEAEvwSoARAB192/256382zQQT/RAAAB//9k=' }} />
                      </Left>
                      <Body>
                        <View style={{marginRight:20}}>


      <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
        <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
        <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
      </View>


                          {/* <View>
                            <Image source={require('../../images/rightarrow.png')} />
                          </View> */}
                          {/* <View style={{backgroundColor:'#ececec', width:'100%'}}>
                          <Text style={{flexWrap: 'wrap'}}>You miss fdddddd dddddddd You miss fdddddd dddddddd</Text>
                          </View> */}
                          {/* <View style={{ flex:0.1}}><Text>aaaaa</Text></View> */}
                          {/* <View 
                          style={{
                            flexDirection:'row', flex:0.8,
                            backgroundColor:'#ececec',
                            padding:10, 
                            marginRight:20
                          }}>
                                <Text style={{flexWrap: 'wrap'}}>You miss fdddddd dddddddd You miss fdddddd dddddddd</Text>
                          </View> */}
                        </View>

                      </Body>
                    </ListItem>

                    </List>

          

          </Content>

          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

            <Row style={{ marginTop: 0, marginBottom: 0 }}>

                    <Button 
                      light
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.props.navigation.goBack(null)}
                      
                    >
                      <Text>Back</Text>
                    </Button>

            </Row>

          </Grid>

          </Footer>
        </Container>
    );
  }
} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(AddEvaluation);