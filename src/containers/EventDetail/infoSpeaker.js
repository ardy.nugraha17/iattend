import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Right,
  Body
} from "native-base";
import styles from "./styles";

const gambars = require('../../../assets/image/logo.png');

const datas = [
  {
    img: gambars,
    text: "Jess No Limit",
    note: "Founder EVOS",
    time: "3:43 pm"
  },
  {
    img: gambars,
    text: "Tuturu",
    note: "CTO RRQ O2",
    time: "1:12 pm"
  },
  {
    img: gambars,
    text: "Lemon",
    note: "Lead Programs RRQ O2",
    time: "10:03 am"
  },
  {
    img: gambars,
    text: "Warpath",
    note: "Curtin University",
    time: "5:47 am"
  },
  {
    img: gambars,
    text: "Oura",
    note: "Co Founder GOJEK",
    time: "11:11 pm"
  },
  {
    img: gambars,
    text: "Vanesha Prescilla",
    note: "Kang Tambal Ban",
    time: "8:54 pm"
  }
];

class infoSpeaker extends Component {
  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem avatar>
                <Left>
                  <Thumbnail small source={data.img} />
                </Left>
                <Body>
                  <Text>
                    {data.text}
                  </Text>
                  <Text numberOfLines={1} note>
                    {data.note}
                  </Text>
                </Body>
                <Right>
                  <Text note>
                    {data.time}
                  </Text>
                </Right>
              </ListItem>}
          />
        </Content>
      </Container>
    );
  }
}

export default infoSpeaker;
