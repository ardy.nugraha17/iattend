import React, { Component } from "react";
import { Image, Dimensions } from "react-native";
import { Content, Card, CardItem, Text, Body,Left,Right } from "native-base";
const deviceWidth = Dimensions.get("window").width;

export default class infoTab extends Component {
  render() {
      const {eventTitle,eventAddress,eventDescription,source} = this.props.infoDetail
    return (
      <Content padder style={{ marginTop: 0 }}>
        <Card >
                        <CardItem bordered>
                        <Left>
                            <Body>
                            <Text>{eventTitle}</Text>
                            <Text note>{eventDescription}</Text>
                            </Body>
                        </Left>
                        </CardItem>

                        <CardItem>
                        <Body>
                            <Image
                            style={{
                                alignSelf: "center",
                                height: 150,
                                resizeMode: "cover",
                                width: deviceWidth / 1.18,
                                marginVertical: 5
                            }}
                            source={source}
                            />
                            <Text>
                            {eventAddress}
                            </Text>
                        </Body>
                        </CardItem>
                    </Card>
      </Content>
    );
  }
}
