import React, { Component } from 'react';
import { View, TouchableOpacity, AsyncStorage, ToastAndroid, BackHandler } from 'react-native';
import {
  // Icon,
  Text,
  Header,
  Body,
  Title,
  Container,
  Content,
  Grid, Col, Row,
  Icon
} from 'native-base';
import Share, { ShareSheet, Button } from 'react-native-share';
import { connect } from 'react-redux';
import ApiCaller from '../../common/apiCaller';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';

// import Icon from 'react-native-vector-icons/SimpleLineIcons';

import styles from './styles';
import getDirections from 'react-native-google-maps-directions';

class EventDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: '',
      audienceid:'',
    };



  }

  

  componentDidMount(){
    console.log("trace eventDetail", this.props);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillMount() {
    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        // console.log("trace anya",data);
        this.setState({
          accessToken: JSON.parse(data).token,
          audienceid: JSON.parse(data).audienceid
        })
      }
    }).done();
    
    // console.log("trace anya props",this.props.infoDetail);
    console.log("trace anya props",this.props);


  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  onPressAgenda(id) {

    const value = {
      accessToken: this.state.accessToken,
      eventId: {
        eventId: id,
      }
     
    }

    this.props.dispatch({
      type: 'FETCH_AGENDA_LIST',
      payload: { ...value }
    });

    this.props.navigation.navigate('ScheduleByDay', { ...this.props.infoDetail }); 

  }


  onPressSpeakers(id) {
    const eventId = {
      eventId: id
    }
    this.props.dispatch({
      type: 'FETCH_SPEAKERS_LIST',
      payload: { ...eventId }
    });
    this.props.navigation.navigate('Speakers', { ...this.props.infoDetail }); 
  }


  onPressSponser(id) {
    const eventId = {
      eventId: id
    }
    this.props.dispatch({
      type: 'FETCH_SPONSERS_LIST',
      payload: { ...eventId }
    });
    this.props.navigation.navigate('Sponsors', { ...this.props.infoDetail });
  }


  onPressAbout(id) {
    this.props.navigation.navigate('AboutUs', { ...this.props.infoDetail });
  }


  onPressMap(lat,lang){
    
    console.log("trace anya onPressMap", lat,lang);

    const data = {
      source: {
      //  latitude: '-6.283550',
      //  longitude: '106.713660'
      latitude: lat,
      longitude: lang
     },
     destination: {
      latitude: lat,
      longitude: lang
     },
     params: [
       {
         key: "travelmode",
         value: "driving"        // may be "walking", "bicycling" or "transit" as well
       },
       {
         key: "dir_action",
         value: "navigate"       // this instantly initializes navigation using the given travel mode 
       }
     ]
   }

   getDirections(data)

  }


  onPressEvaluationForm(id){

    const eventId = {
      eventId: id,
      audienceId: this.state.audienceid
    }
    this.props.dispatch({
      type: 'FETCH_EVALUATION',
      payload: { ...eventId }
    });

    this.props.navigation.navigate('Evaluation', { ...this.props.infoDetail });

  } 


  onPressQuestion(id){

    const eventId = {
      eventId: id,
      audienceId: this.state.audienceid
    }
    this.props.dispatch({
      type: 'FETCH_QUESTION',
      payload: { ...eventId }
    });

    this.props.navigation.navigate('Question', { ...this.props.infoDetail });

  } 


  onPressAttend(id){

    this.props.navigation.navigate('Attend', { ...this.props.infoDetail });

  } 


  onPressPolling(id){

    const eventId = {
      eventId: id,
      audienceId: this.state.audienceid
    }
    this.props.dispatch({
      type: 'FETCH_POLLING',
      payload: { ...eventId }
    });

    this.props.navigation.navigate('Polling', { ...this.props.infoDetail });

  } 


  onPressFile(id){

    const eventId = {
      eventId: id,
      audienceId: this.state.audienceid
    }
    this.props.dispatch({
      type: 'FETCH_FILE',
      payload: { ...eventId }
    });

    this.props.navigation.navigate('File', { ...this.props.infoDetail });

  } 

  goBack(){
    this.props.navigation.goBack(null);
  }


  render() {

    const { eventId, eventTitle, eventAddress, eventDate, eventDescrip, source, lat_long} = this.props.infoDetail;

    const latlng = lat_long.split(',');
    const lat = parseFloat(latlng[0]);
    const lang = parseFloat(latlng[1]);

    const {navigation} = this.props;

    
    return (

      <Container>


        <Header style={styles.headerStyle}>
          <Body>
            <Text style={styles.forTitle} >{this.props.navigation.state.params.eventTitle}</Text>
            <Text style={styles.forSubTitle}>{this.props.navigation.state.params.eventAddress}</Text>
            <Text style={styles.forSubTitle}>{this.props.navigation.state.params.eventDate}</Text>
          </Body>
        </Header>

        <Content padder>

          {/* Baris pertama */}
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingRight:'13%', paddingLeft:'13%', paddingBottom:'2%', paddingTop:'2%', height: 100 }}>
              
              {/* Agenda */}
              <View style={styles.viewIcon}>
                  <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressAgenda(eventId)}>
                  <Icon active type="EvilIcons" name='calendar' style={{ fontSize: 30 }} />
                  <Text style={styles.iconText}>
                        Agenda
                  </Text>
                  </TouchableOpacity>
              </View>


              {/* Speakers */}
              <View style={styles.viewIcon}>
                  <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressSpeakers(eventId)}>
                  <Icon type="EvilIcons" active name="user" style={{ fontSize: 30 }} />
                  <Text style={styles.iconText}>
                        Speakers
                  </Text>
                  </TouchableOpacity>
              </View>


              <View style={styles.viewIcon}>
                  <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressSponser(eventId)}>
                  <Icon type="EvilIcons" active name="trophy" style={{ fontSize: 30 }} />
                  <Text style={styles.iconText}>
                        Sponsors
                  </Text>
                  </TouchableOpacity>
              </View>                

          </View>

          {/* Baris kedua */}
          <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'space-between', paddingRight:'13%', paddingLeft:'13%', paddingBottom:'2%', paddingTop:'2%', height: 100  }}>
            
            <View style={styles.viewIcon}>
              <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressAbout(eventId)}>
              <Icon type="EvilIcons" active name="exclamation" style={{ fontSize: 30 }} />
              <Text style={styles.iconText}>
                    About
              </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.viewIcon}>
              {/* <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressEvaluationForm(eventId)}>
              <Icon type="EvilIcons" active name="chart" style={{ fontSize: 30 }} />
              <Text style={styles.iconText}>
                    Evaluation Form
              </Text>
              </TouchableOpacity> */}

              {/* <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressPolling(eventId)}>
              <Icon type="EvilIcons" active name="tag" style={{ fontSize: 30 }} />
              <Text style={styles.iconText}>
                    Polling
              </Text>
              </TouchableOpacity> */}

              <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressAttend(eventId)}>
              <Icon type="EvilIcons" active name="archive" style={{ fontSize: 30 }} />
              <Text style={styles.iconText}>
                    Attend
              </Text>
              </TouchableOpacity>


            </View>
            <View style={styles.viewIcon}>
                <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressMap(lat,lang)}>
                <Icon type="EvilIcons" active name="location" style={{ fontSize: 30 }} />
                <Text style={styles.iconText}>
                      Maps
                </Text>
                </TouchableOpacity>
            </View>

          </View>


          {/* Baris ketiga */}
          <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'space-between', paddingRight:'13%', paddingLeft:'13%', paddingBottom:'2%', paddingTop:'2%', height: 120  }}>
            
            {/* <View style={styles.viewIcon}>
              <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressQuestion(eventId)}>
              <Icon type="EvilIcons" active name="question" style={{ fontSize: 30 }} />
              <Text style={styles.iconText}>
                    Question
              </Text>
              </TouchableOpacity>
            </View> */}

            <View style={styles.viewIcon}>
              {/* <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressAttend(eventId)}>
              <Icon type="EvilIcons" active name="archive" style={{ fontSize: 30 }} />
              <Text style={styles.iconText}>
                    Attend
              </Text>
              </TouchableOpacity> */}
            </View>

            <View style={styles.viewIcon}>
              {/* <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressPolling(eventId)}>
              <Icon type="EvilIcons" active name="tag" style={{ fontSize: 30 }} />
              <Text style={styles.iconText}>
                    Polling
              </Text>
              </TouchableOpacity> */}
            </View>


            <View style={styles.viewIcon}>

            </View>

          </View>



          {/* Baris keempat */}
          {/* <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'space-between', paddingRight:'13%', paddingLeft:'13%', paddingBottom:'2%', paddingTop:'2%', height: 120  }}>
            
            <View style={styles.viewIcon}>
              <TouchableOpacity style={styles.viewIcon} onPress={() => this.onPressFile(eventId)}>
              <Icon type="EvilIcons" active name="link" style={{ fontSize: 30 }} />
              <Text style={styles.iconText}>
                    Files
              </Text>
              </TouchableOpacity>
            </View>

          </View> */}


          </Content>

      </Container>


    );
  }
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps)(EventDetail);
