import { StyleSheet, Dimensions } from 'react-native';
import * as Common from '../../common/common';


const styles = StyleSheet.create({

    viewStyle: {
        backgroundColor: '#ececec',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
      },

    buttonTextStyle: {
    color: Common.darkColor
    },
    
    headertitleviewevaluation:{
        backgroundColor:'#cccccc',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,
    },

    titleListParticipant:{
        backgroundColor:'#ececec',
        width:'100%',
        paddingLeft:20,
        paddingTop:10,
        paddingBottom:10
    },
    listViewEvaluation : {
        width:'100%'
    },
    containerMateri: {
        // flex: 1,
        // justifyContent: 'flex-start',
        // alignItems: 'center',
        // marginTop: 25,
        // height:30,
        backgroundColor:'#F2F2F2',
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 10,
        paddingBottom: 10,
    },
    textMateri:{
        fontSize:14,
        fontWeight: 'bold',
    },
    containerPdf: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    }

});
module.exports = styles;