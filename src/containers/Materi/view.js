import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  // Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage,
  TouchableHighlight, Platform,
  ActivityIndicator,
  NativeAppEventEmitter,
  DeviceEventEmitter,
  NativeModules,
  NativeEventEmitter,
  Dimensions
} from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';
import PieChart from 'react-native-pie-chart';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Pdf from 'react-native-pdf';


class ViewMateri extends Component {


  constructor(props) {
    super(props);

    this.state = {

    };


  }

  componentDidMount(){
    // download progress
    console.log("componentDidMount",this.props);
    this.setState({
      fileName: this.props.navigation.state.params.item.fileName,
      name: this.props.navigation.state.params.item.name,
      pathFile: this.props.navigation.state.params.item.pathFile
    })
  }

  componentWillUnmount (){

  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  render() {
    // const source = require('./document.pdf');

    const source = {uri:'http://192.168.3.117:1337/get_file_moderator?fd='+ this.state.pathFile,cache:true};

    return (
        <Container style={styles.container}>


        <Header
        headerContainer={styles.headerContainerStyle}
        headerTextStyle={styles.headerTextStyle}
        headerLeft={{padding: 10}}
        onLeftIconClick={() => this.props.navigation.goBack(null)}
        onRightIconClick={() => this.onPressShare()}
        leftIcon="md-arrow-back"
        // rightIcon="share"
        title="Detail Materi"
        />
        <Content contentContainerStyle={{ flex: 1 }}>
        
        <View style={styles.containerMateri}>

          <Text style={styles.baseText}>
            <Text style={styles.textMateri}>Materi name : </Text>
            <Text numberOfLines={5} style={{color:'#666666'}}>{this.state.fileName}</Text>
          </Text>

          <Text style={styles.baseText}>
            <Text style={styles.textMateri}>Agenda : </Text>
            <Text numberOfLines={5} style={{color:'#666666'}}>{this.state.name}</Text>
          </Text>

         
        </View>

        <View style={styles.containerPdf}>
            <Pdf
                source={source}
                onLoadComplete={(numberOfPages,filePath)=>{
                    console.log(`number of pages: ${numberOfPages}`);
                }}
                onPageChanged={(page,numberOfPages)=>{
                    console.log(`current page: ${page}`);
                }}
                onError={(error)=>{
                    console.log(error);
                }}
                style={styles.pdf}/>
        </View>


        {/* <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View> */}


        </Content>

        <Footer style={{ backgroundColor: "transparent" }}>
            <Button 
              light
              style = {{
                flexDirection: "row",
                flexWrap: "wrap",
                flex: 1,
                justifyContent: "center",
                borderWidth:0,
                height:55,
                borderRadius:0,
                marginRight:1
              }}

              onPress= {() => this.props.navigation.goBack(null)}
              
            >
              <Text>Close</Text>
            </Button>
        </Footer>
        </Container>
    )
  }


} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(ViewMateri);