import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';



class AddPolling extends Component {


  constructor(props) {
    super(props);

    this.onChangeItem = this.onChangeItem.bind(this);

    this.state = {
      chooseOption: 'Please Choose one',
      dialogVisible:false,
      textOption:0,
      countItem:0,
      question:"",


      answer0:'',
      answer1:'',
      answer2:'',
      answer3:'',
      answer4:'',
      answer5:'',
      answer6:'',
      answer7:'',
      answer8:'',
      answer9:'',

    };

  }

  componentDidMount(){
    console.log("register event",this.props.navigation.state.params);
  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  cancelButton(){
    console.log("cancelButton");
    this.props.navigation.goBack(null);
  }

  submitButton(){

    this.props.navigation.navigate('Home');
  }

  onChangeItem(text){
    console.log("trace anya onChangeItem",text);
    this.setState({
      countItem:text
    });
  }

  onChangeQuestionList(text){
    console.log("trace anya onChangeItem",text);
    this.setState({
      question:text
    });
  }

  LoopingAnswer() {

    var answer = [];

    for(let i = 0; i < this.state.countItem; i++){


      switch (i) {
        case 0:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    <Input
                    style={{fontSize:14}}
                    value = {this.state.answer0}
                    onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                    />
                  </Item>
                </View>
              </View>
            )
            break;
        case 1:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    <Input
                    style={{fontSize:14}}
                    value = {this.state.answer1}
                    onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                    />
                  </Item>
                </View>
              </View>
            )
            break;
        case 2:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    <Input
                    style={{fontSize:14}}
                    value = {this.state.answer2}
                    onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                    />
                  </Item>
                </View>
              </View>
            )
            break;
        case 3:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                      <Label
                      style={{fontSize:14, fontWeight:'bold'}}
                      >Answer {i + parseInt(1)} : </Label>
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer3}
                      onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                      />
                    </Item>
                </View>
              </View>
            )
            break;
        case 4:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                      <Label
                      style={{fontSize:14, fontWeight:'bold'}}
                      >Answer {i + parseInt(1)} : </Label>
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer4}
                      onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                      />
                    </Item>
                </View>
              </View>
            )
            break;
        case 5:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                      <Label
                      style={{fontSize:14, fontWeight:'bold'}}
                      >Answer {i + parseInt(1)} : </Label>
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer5}
                      onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                      />
                    </Item>
                </View>
              </View>
            )
            break;
        case 6:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                      <Label
                      style={{fontSize:14, fontWeight:'bold'}}
                      >Answer {i + parseInt(1)} : </Label>
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer6}
                      onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                      />
                    </Item>
                </View>
              </View>
            )
            break;
        case 7:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                      <Label
                      style={{fontSize:14, fontWeight:'bold'}}
                      >Answer {i + parseInt(1)} : </Label>
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer7}
                      onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                      />
                    </Item>
                </View>
              </View>
            )
            break;
        case 8:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                      <Label
                      style={{fontSize:14, fontWeight:'bold'}}
                      >Answer {i + parseInt(1)} : </Label>
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer8}
                      onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                      />
                    </Item>
                </View>
              </View>
            )
            break;
        case 9:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                      <Label
                      style={{fontSize:14, fontWeight:'bold'}}
                      >Answer {i + parseInt(1)} : </Label>
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer9}
                      onChangeText = {(text) => this.onChangeAnswerList(i, text)}
                      />
                    </Item>
                </View>
              </View>
            )
      }

      

    }


    return (
      <View
      style={{}}
      >
            <View>

            {answer}

            </View>

      </View>
    )
  }


  onChangeAnswerList(answerNumber, text){

    console.log('trace onChangeAnswerList', answerNumber, text)

      switch (answerNumber) {
        case 0:
            this.setState({
              answer0:text
            })
            break;
        case 1:
            this.setState({
              answer1:text
            })
            break;
        case 2:
            this.setState({
              answer2:text
            })
            break;
        case 3:
            this.setState({
              answer3:text
            })
            break;
        case 4:
            this.setState({
              answer4:text
            })
            break;
        case 5:
            this.setState({
              answer5:text
            })
            break;
        case 6:
            this.setState({
              answer6:text
            })
            break;
        case 7:
            this.setState({
              answer7:text
            })
            break;
        case 8:
            this.setState({
              answer8:text
            })
            break;
        case 9:
            this.setState({
              answer9:text
            })
      }
    

  }


  render() {

    let { chooseOption } = this.state;

    let data = [
      { value: '1', label: '1' }, 
      { value: '2', label: '2'}, 
      { value: '3', label: '3'},
      { value: '4', label: '4'},
      { value: '5', label: '5'},
      { value: '6', label: '6'},
      { value: '7', label: '7'},
      { value: '8', label: '8'},
      { value: '9', label: '9'},
      { value: '10', label: '10'},
    ];

    return (
        <Container style={styles.container}>


          <Header
          headerContainer={styles.headerContainerStyle}
          headerTextStyle={styles.headerTextStyle}
          headerLeft={{padding: 10}}
          onLeftIconClick={() => this.props.navigation.goBack(null)}
          onRightIconClick={() => this.onPressShare()}
          leftIcon="md-arrow-back"
          // rightIcon="share"
          title="Add Polling"
          />
          <Content padder
          style={{ padding:15}}
          >


              <Label
              style={{marginBottom:3, marginTop:3, fontSize:14, fontWeight:'bold'}}
              >Title Polling Form</Label>
              <Item regular>
              <Input 
              style={{padding:5, paddingLeft:10, fontSize:16, height:40, backgroundColor:'#ffffff', borderWidth:0, borderColor:'#ececec'}}
              placeholder="Please insert title polling here ..." />
              </Item>

              <Dropdown
                value={chooseOption}
                label='Item Choose'
                labelFontSize="15"
                fontSize= {15}
                data={data}
                ref={this.sampleRef}
                onChangeText={this.onChangeItem}
              />

              <Label
              style={{marginBottom:3, marginTop:3, fontSize:14, fontWeight:'bold'}}
              >Question :</Label>
              <Textarea 
              rowSpan={5} 
              bordered 
              placeholder="Please insert text evaluation here ..."
              onChangeText ={(text) => this.onChangeQuestionList(text)} 
              />

              {this.LoopingAnswer()}

          </Content>

          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

            <Row style={{ marginTop: 0, marginBottom: 0 }}>

                    <Button 
                      danger
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.cancelButton()}
                      
                    >
                      <Text>Cancel</Text>
                    </Button>

                    <Button 
                      success
                        style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0
                      }}

                      onPress= {() => this.submitButton()}

                    >
                      <Text>Submit</Text>
                    </Button>


            </Row>

          </Grid>

          </Footer>
        </Container>
    );
  }
} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(AddPolling);