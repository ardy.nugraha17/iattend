import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  // Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage,
  TouchableHighlight, Platform,
  ActivityIndicator,
  NativeAppEventEmitter,
  DeviceEventEmitter,
  NativeModules,
  NativeEventEmitter,
} from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';
import PieChart from 'react-native-pie-chart';
import Icon from 'react-native-vector-icons/MaterialIcons';

import OpenFile from 'react-native-doc-viewer';
var RNFS = require('react-native-fs');
var SavePath = Platform.OS === 'ios' ? RNFS.MainBundlePath : RNFS.DocumentDirectoryPath;


import Pdf from 'react-native-pdf';



class ViewMateri extends Component {


  constructor(props) {
    super(props);

    this.state = {
      animating: false,
      progress: "",
      donebuttonclicked: false,
    };

    this.eventEmitter = new NativeEventEmitter(NativeModules.RNReactNativeDocViewer);
    this.eventEmitter.addListener('DoneButtonEvent', (data) => {
      /*
      *Done Button Clicked
      * return true
      */
      console.log(data.close);
      this.setState({donebuttonclicked: data.close});
    })
    this.handlePress = this.handlePress.bind(this);

  }

  componentDidMount(){
    // download progress
    this.eventEmitter.addListener(
      'RNDownloaderProgress',
      (Event) => {
        console.log("Progress - Download "+Event.progress  + " %")
        this.setState({progress: Event.progress + " %"});
      }

    );
  }

  componentWillUnmount (){
    this.eventEmitter.removeListener();
  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  handlePress = () => {
    this.setState({animating: true});
    if(Platform.OS === 'ios'){
      OpenFile.openDoc([{
        url:"https://calibre-ebook.com/downloads/demos/demo.docx",
        fileNameOptional:"test filename"
      }], (error, url) => {
         if (error) {
          this.setState({animating: false});
         } else {
          this.setState({animating: false});
           console.log(url)
         }
       })
    }else{
      //Android
      this.setState({animating: true});
      OpenFile.openDoc([{
        url:"https://calibre-ebook.com/downloads/demos/demo.docx",
        fileName:"test filename",
        // url:"https://www.huf-haus.com/fileadmin/Bilder/Header/ART_3/Header_HUF_Haus_ART_3___1_.jpg", // Local "file://" + filepath
        // fileName:"sample",
        cache:false,
        fileType:"doc"
      }], (error, url) => {
         if (error) {
          this.setState({animating: false});
           console.error(error);
         } else {
          this.setState({animating: false});
           console.log(url)
         }
       })
    }

  }



  handlePressLocal = () => {
    this.setState({animating: true});
    if(Platform.OS === 'ios'){
        OpenFile.openDoc([{url:SavePath+"/react-native-logo.jpg",
        fileNameOptional:"test filename"
      }], (error, url) => {
         if (error) {
          this.setState({animating: false});
         } else {
          this.setState({animating: false});
           console.log(url)
         }
       })
    }else{
      OpenFile.openDoc([{url:SavePath+"/demo.jpg",
        fileName:"sample",
        cache:false,
        fileType:"jpg"
      }], (error, url) => {
         if (error) {
          this.setState({animating: false});
         } else {
          this.setState({animating: false});
           console.log(url)
         }
       })

    }
  }

  render() {
    // const source = require('./document.pdf');
    const source = "https://www.ets.org/Media/Tests/TOEFL/pdf/SampleQuestions.pdf";

    return <Pdf
              source={source}
              style={styles.pdf}
            />;
  }


} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(ViewMateri);