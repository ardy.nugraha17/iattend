import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerStyle: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  image: {
    resizeMode: 'stretch',
  },
  headerTextStyle: {
    color: Common.whiteColor,
    fontWeight: 'bold',
  },
  headerContainerStyle: {
    flex: 1,
    margin: 5,
  },
  text: {
    color: '#fff',
    fontSize: 14,
  },
  viewWrapper: {
    flex: 9,
  },
  tabStyle: {
    backgroundColor: Common.lightGreen,
  },
  viewSubheaderTab:{
    padding: 15,
    paddingLeft: 25,
    // backgroundColor: Common.lightGray,
    backgroundColor: "#f2f2f2"
  },
  viewSubheaderTabText:{
    fontSize:16,
    fontWeight:'bold'
  },
  titleItemName:{
    fontSize:15,
    fontWeight: 'bold',
    color: "#666666",
  },
  subtitleItemName:{
    fontSize:13,
    color: "#666666",
  },

  containerSpinner: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontalSpinner: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
});

module.exports = styles;
