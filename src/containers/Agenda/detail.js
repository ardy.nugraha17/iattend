import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid,
  // Col,
  // Row
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, TouchableOpacity, CameraRoll, AsyncStorage, Vibration, BackHandler  } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../Agenda/styles';

import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';
import email from 'react-native-email';
import { RNCamera } from 'react-native-camera';
import { Col, Row, Grid } from "react-native-easy-grid";
import store from 'react-native-simple-store';
import ImageResizer from 'react-native-image-resizer';
import ApiCaller from '../../common/apiCaller';
import axios from 'axios';

class Detail extends Component {


  constructor(props) {

    super(props);
    this.state = {
        accessToken:'',
        audienceid:'',
        email:'',
        name:''
    }
  }

  componentDidMount(){

    console.log("componentDidMount",this.props);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  componentWillMount(){


    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        // console.log("trace anya",data);
        this.setState({
          accessToken: JSON.parse(data).token,
          audienceid: JSON.parse(data).audienceid,
          email: JSON.parse(data).mail,
          name: JSON.parse(data).name,

        })
      }
    }).done();


  }


  onPressQuestion(){

    const eventId = {
      eventId: this.props.navigation.state.params.navs.state.params.eventId,  
      agendaId: this.props.navigation.state.params.eventId,
      audienceId: this.state.audienceid
    }
    this.props.dispatch({
      type: 'FETCH_QUESTION',
      payload: { ...eventId }
    });

    this.props.navigation.navigate('Question', { ...this.props });

  } 

  onPressPolling(){

    // cek apakah ada polling untuk agenda ini
    ApiCaller('mobileIattend_polling_cek', 'post', this.state.accessToken, {
      audience_id : this.state.audienceid,
      eventId : this.props.navigation.state.params.navs.state.params.eventId, 
      agendaId : this.props.navigation.state.params.eventId,
  
    }).then(response =>{

      console.log("trace mobileIattend_polling_cek", response);

      if(response.statusPolling == "ada"){
        // ToastAndroid.show('ada', ToastAndroid.SHORT);
        // this.props.navigation.goBack(null);
        // this.setState({ dialogVisible: true })
        // console.log("onPressAttendees",response);


        const eventId = {
          eventId: this.props.navigation.state.params.navs.state.params.eventId,  
          agendaId: this.props.navigation.state.params.eventId,
          audienceId: this.state.audienceid
        }
        this.props.dispatch({
          type: 'FETCH_POLLING',
          payload: { ...eventId }
        });
    
        this.props.navigation.navigate('Polling', { ...this.props });


      }else{

        // ToastAndroid.show('You are already registered, {\n} please check your email', ToastAndroid.SHORT);
        // console.log("trace registerPolling",response.message);
        ToastAndroid.show('Currently the poll for this agenda is not yet available', ToastAndroid.SHORT, ToastAndroid.CENTER);

      }
    
    })



  } 


  onPressFile(){

    const eventId = {
        eventId: this.props.navigation.state.params.navs.state.params.eventId,  
        agendaId: this.props.navigation.state.params.eventId,
        audienceId: this.state.audienceid
    }
    this.props.dispatch({
      type: 'FETCH_FILE',
      payload: { ...eventId }
    });

    this.props.navigation.navigate('File', { ...this.props });

  } 


  goBack(){
    this.props.navigation.goBack(null)
  }



  render() {
  
    const {navigation} = this.props;
    
    return (
        <Container style={styles.container}>

            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Agenda menu"
            />  
  
            <Content>
              <List>
                <ListItem style={{paddingLeft:8, marginLeft:0}} button onPress={() => this.onPressFile()}>
                  <Body>
                    <Text>Files</Text>
                  </Body>
                </ListItem>
                <ListItem style={{paddingLeft:8, marginLeft:0}} button onPress={() => this.onPressPolling()}>
                  <Body>
                    <Text>Polling</Text>
                  </Body>
                </ListItem>
                <ListItem style={{paddingLeft:8, marginLeft:0}} button onPress={() => this.onPressQuestion()}>
                  <Body>
                    <Text>Question</Text>
                  </Body>
                </ListItem>
              </List>
            </Content>

        </Container>
      );


  }
}


const mapStateToProps = state => ({
  aboutData: state.aboutData,
});

export default connect(mapStateToProps)(Detail);