import React, { Component } from 'react'
import {
  View,
  Image,
  StatusBar,
  Text,
  AsyncStorage,
  ScrollView,
  ActivityIndicator,
  BackHandler
} from 'react-native'
import { Tabs, Tab, TabHeading } from "native-base"
import { connect } from 'react-redux';
import Icon from "react-native-vector-icons/FontAwesome";
import styles from './styles';
import DrawerView from '../../component/DrawerView';
import EventList from '../../component/EventList/eventlistrandom';
import { eventList, eventListJune , mainText, mainTabText} from '../../common/constant';
import Header from '../../component/Header';
import SideMenu from '../SideMenu';



var moment = require('moment');

class ScheduleByDay extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: false,
      showDrawer: true,
      stateTitle:"Agenda",
      initialPage: 0, 
      activeTab: 0,
      // stateTitleId:0
      
    }

    this.setTitleNameId = this.setTitleNameId.bind(this);

  }

  componentDidMount() {

    console.log("trace anya agenda",this.props);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

  }


  componentWillMount() {
    AsyncStorage.getItem('userData').then((data) => {
      // console.log("data@@componentWillMount",data)
      if(data) {
        this.setState({showDrawer: true})
      }
    }).done();

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  goBack(){
    this.props.navigation.goBack(null);
  }


  closeDrawer = () => {
    this.drawer._root.close()
  }

  openDrawer = () => {
    this.drawer._root.open()
  }

  
  getMappedagendaData(data){
    let arr={};
    // monthNames = ["January", "February", "March", "April", "May", "June",
    // "July", "August", "September", "October", "November", "December"
    // ];  
    data.forEach(function(each) {

      // let d=each.eventDescription;
      // let key=monthNames[parseInt(d.slice(5,7))-1]+'-'+d.slice(0,4);
      let key = moment(each.time).format('h:mm A'); 
      // console.log("trace anya", arr[key]);
      if(!arr[key]) arr[key]=[];
      arr[key].push(each);
      }); 

      
    return arr;
  }


  getMappedData(data){
    let arr={};
    monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];  
    data.forEach(function(each) {

      // console.log("trace anya", each);

      let d=each.eventDescription;
      let key=monthNames[parseInt(d.slice(5,7))-1]+'-'+d.slice(0,4);
      if(!arr[key]) arr[key]=[];
      arr[key].push(each);
      }); 

      
    return arr;
  }



  setTitleNameId(pram){
    if(pram == 0){
      // this.props.navigation.setParams({title: "A"})
      // this.props.navigation.setTitle({title: 'A'})
          this.setState({stateTitle: "SCHEDULE"});
    }else if(pram == 1){
      // this.props.navigation.setTitle({title: 'B'})
      // this.props.navigation.setParams({title: "B"})
      this.setState({stateTitle: "MY SCHEDULE"});
    }else{
      // this.props.navigation.setTitle({title: 'C'})
      // this.props.navigation.setParams({title: "C"})
      this.setState({stateTitle: "INVITATIONS"});

    }
    // const s = otherState.getambilnya();
    // this.setState({stateTitleId: s});
  }


  loading(){
    return(
      <View style={[styles.containerSpinner, styles.horizontalSpinner]}>
      <ActivityIndicator size="large" color="#b3b3b3" />
      {/* <ActivityIndicator size="small" color="#00ff00" />
      <ActivityIndicator size="large" color="#0000ff" />
      <ActivityIndicator size="small" color="#00ff00" /> */}
    </View>
    )

  }


  body() {

    return (
    <View style={{flex: 1}}>
      <ScrollView>

          {
          Object.keys(this.getMappedagendaData(this.props.agendaData.agenda.byDate)).map((data, i) => (
          <EventList eventList={this.getMappedagendaData(this.props.agendaData.agenda.byDate)[data]} date={data} jumlah={this.props.agendaData.agenda.byDate[this.props.agendaData.agenda.byDate.length - 1].id} nav={this.props.navigation} {...this.props}/>
          ))
          } 

      </ScrollView>
    </View>
    )
  }


  render () {
    let { showDrawer } = this.state;
    const agendaData = (this.props.agendaData.agenda.byDate !== undefined ? this.props.agendaData.agenda.byDate : '')
    const {navigation} = this.props;

    return (
      <DrawerView
        onClose={() => this.closeDrawer()}
        referVar={(ref) => { this.drawer = ref; }}
        content={<SideMenu {...this.props} onClose={() => this.closeDrawer()} />}
      >
        <View style={styles.container}>




          <StatusBar
             hidden={true}
           />
          <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle} 
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            leftIcon="md-arrow-back"
            title={this.state.stateTitle}
          />



                {this.props.agendaData.agenda.byDate !== undefined ? this.body() : this.loading()}
                


        </View>
      </DrawerView>
    )
  }
}


  const mapStateToProps = state => ({
    eventData: state.eventData,
    agendaData: state.agendaData,
  });

  export default connect(mapStateToProps)(ScheduleByDay);