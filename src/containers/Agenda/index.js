import React, { Component } from "react";
import { Dimensions } from "react-native";
import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body
} from "native-base";
import HeaderStyles from "../../component/Header/styles";
import styles from "./styles"
import AgendaDetail from "../../component/AgendaDetail"
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';

class Agenda extends Component {
  closeDrawer = () => {
    this.drawer._root.close()
  }

  openDrawer = () => {
    this.drawer._root.open()
  }

    render() {
        const { eventAddress, eventTitle, eventDescription, source } = this.props.navigation.state.params
        return (
          <DrawerView
            onClose={() => this.closeDrawer()}
            referVar={(ref) => { this.drawer = ref; }}
            content={<SideMenu {...this.props} onClose={() => this.closeDrawer()} />}
          >
            <Container style={styles.container}>
              <Header
                headerContainer={styles.headerContainerStyle}
                headerTextStyle={styles.headerTextStyle}
                headerLeft={{padding: 10}}
                onLeftIconClick={() => this.openDrawer()}
                leftIcon="ios-menu"
                title="Agenda"
              />
                {/* <Content padder>
                    <AgendaDetail infoDetail={this.props.navigation.state.params}  nav={this.props.navigation} />
                </Content> */}
                <Content>
                    <AgendaDetail infoDetail={this.props.navigation.state.params}  nav={this.props.navigation} />
                </Content>
            </Container>
          </DrawerView>
    );
  }
}

export default Agenda;
