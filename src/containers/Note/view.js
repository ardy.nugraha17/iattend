import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage, TextInput } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';

const eventListClass = [];
const eventListTrack = [];

const answer1 = [];
const answer2 = [];
const answer3 = [];
const answer4 = [];
const answer5 = [];
const answer6 = [];
const answer7 = [];
const answer8 = [];
const answer9 = [];
const answer10 = [];

class ViewNote extends Component {


  constructor(props) {
    super(props);

    this.state = {

    };

  }

  componentDidMount(){
    console.log("componentDidMount",this.props);
    
    this.setState({
      text:this.props.navigation.state.params.text,
      id:this.props.navigation.state.params.id
    })
  }

  componentWillReceiveProps(next){
    console.log("componentWillReceiveProps",next);

  }

  cancelButton(){
    console.log("cancelButton");
    this.props.navigation.goBack(null);
  }

  submitButton(){
    
    console.log("submitButton");
    AsyncStorage.getItem("userData").then((data) => {

        console.log("AsyncStorage");

        const titleNote = this.state.heading;
        const textNote = this.state.text;
      
        // ApiCaller('addnote', 'post', JSON.parse(data).token, {
        //   text: this.state.text, heading:titleNote, id:JSON.parse(data).audienceid }).then(response =>{
        //     console.log("ss anya balikan addnote",response);
        //   if(response.affectedRows == 1){
          
        //   Helper.navigateToPage(this, 'Home')
        //   }
        // })


        ApiCaller('editnotemoderator', 'post', JSON.parse(data).token, {
          moderatorId : JSON.parse(data).audienceid,
          text: textNote,
          id:this.props.navigation.state.params.id,
          heading : titleNote,
          
      
        }).then(response =>{

          console.log("editnotemoderator", response);

          if(response == "sukses"){

            // this.props.dispatch({
            //   type: 'FETCH_EVENT_LIST',
            // });


            console.log("componentDidMount",JSON.parse(data))

            const agendaId = JSON.parse(data).agendaId;

            const value = {
              'agendaId': agendaId
            }
        
        
            this.props.dispatch({
              type: 'FETCH_EVENT_LIST',
              payload: value
            });

            ToastAndroid.show('success to submit', ToastAndroid.SHORT);
            this.props.navigation.navigate('Home');
          }else{
            ToastAndroid.show('Failed to submit', ToastAndroid.SHORT);
          }

        
        })

      
    }).done();





  
    // this.props.navigation.navigate('Login');
  }


  render() {


    // const { attendees } = this.props.attendeesData;
    // const {eventAddress, eventTitle, eventDescription, source,} = this.props.navigation.state.params;


    return (
        <Container style={styles.container}>


          <Header
          headerContainer={styles.headerContainerStyle}
          headerTextStyle={styles.headerTextStyle}
          headerLeft={{padding: 10}}
          onLeftIconClick={() => this.props.navigation.goBack(null)}
          // onRightIconClick={() => this.onPressShare()}
          leftIcon="md-arrow-back"
          // rightIcon="share"
          title="Edit Note"
          >
          
          </Header>
          <Content>

            <View style={styles.textAreaContainer}>

            <TextInput
            autoFocus={true}
            style={{textAlignVertical: 'top',fontSize:15,lineHeight:27}}
            multiline={true}
            numberOfLines={10}
            value={this.state.text}
            underlineColorAndroid="transparent"
            onChangeText={(value) => this.setState({
              text:value,
              heading: value
            })}
            value={this.state.text}/>

            </View>

          

          </Content>

          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

            <Row style={{ marginTop: 0, marginBottom: 0 }}>

                    <Button 
                      danger
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.cancelButton()}
                      
                    >
                      <Text>Cancel</Text>
                    </Button>

                    <Button 
                      success
                        style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0
                      }}

                      onPress= {() => this.submitButton()}

                    >
                      <Text>Submit</Text>
                    </Button>


            </Row>

          </Grid>

          </Footer>
        </Container>
    );
  }
} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(ViewNote);