import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage, TextInput } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';

const eventListClass = [];
const eventListTrack = [];

const answer1 = [];
const answer2 = [];
const answer3 = [];
const answer4 = [];
const answer5 = [];
const answer6 = [];
const answer7 = [];
const answer8 = [];
const answer9 = [];
const answer10 = [];

class AddNote extends Component {


  constructor(props) {
    super(props);

    this.state = {
      text:'',
      heading:''

    };

  }

  componentDidMount(){
    console.log("register event",this.props.navigation.state.params);
  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  onChangeTextNote(value){
    this.setState({
      text: value,
      heading: value
    });
  }

  cancelButton(){
    console.log("cancelButton");
    this.props.navigation.goBack(null);
  }

  submitButton(){
    
    console.log("submitButton");
    AsyncStorage.getItem("userData").then((data) => {

        console.log("AsyncStorage");

        const titleNote = this.state.heading;
        const textNote = this.state.text;
      
        // ApiCaller('addnote', 'post', JSON.parse(data).token, {
        //   text: this.state.text, heading:titleNote, id:JSON.parse(data).audienceid }).then(response =>{
        //     console.log("ss anya balikan addnote",response);
        //   if(response.affectedRows == 1){
          
        //   Helper.navigateToPage(this, 'Home')
        //   }
        // })


        ApiCaller('addnotemoderator', 'post', JSON.parse(data).token, {
          moderatorId : JSON.parse(data).audienceid,
          text: textNote,
          heading : titleNote,
          
      
        }).then(response =>{
    
          console.log("addnotemoderator",response);

          const test = "asdasdas";

          if(response == "sukses"){

            const value = {
              'agendaId': JSON.parse(data).agendaId,
              'moderatorId': JSON.parse(data).audienceid
            }
        
            this.props.dispatch({
              type: 'FETCH_EVENT_LIST',
              payload: value
            });

            ToastAndroid.show('success to submit', ToastAndroid.SHORT);
            this.props.navigation.navigate('Home');
          }else{
            ToastAndroid.show('Failed to submit', ToastAndroid.SHORT);
          }
          

          // if(response.message == ""){
          //   ToastAndroid.show('Register has been success', ToastAndroid.SHORT);
          //   // this.setState({ dialogVisible: true })
          //   // console.log("addnotemoderator",response);
    
          // }else{
    
          //   // ToastAndroid.show('You are already registered, {\n} please check your email', ToastAndroid.SHORT);
          //   ToastAndroid.show('You allready registered', ToastAndroid.SHORT);
            
          // }
        
        })

      
    }).done();





  
    // this.props.navigation.navigate('Login');
  }


  render() {

    let { chooseOption } = this.state;

    let data = [
      { value: '1', label: '1' }, 
      { value: '2', label: '2'}, 
      { value: '3', label: '3'},
      { value: '4', label: '4'},
      { value: '5', label: '5'},
      { value: '6', label: '6'},
      { value: '7', label: '7'},
      { value: '8', label: '8'},
      { value: '9', label: '9'},
      { value: '10', label: '10'},
    ];




    // const { attendees } = this.props.attendeesData;
    // const {eventAddress, eventTitle, eventDescription, source,} = this.props.navigation.state.params;


    return (
        <Container style={styles.container}>


          <Header
          headerContainer={styles.headerContainerStyle}
          headerTextStyle={styles.headerTextStyle}
          headerLeft={{padding: 10}}
          onLeftIconClick={() => this.props.navigation.goBack(null)}
          onRightIconClick={() => this.onPressShare()}
          leftIcon="md-arrow-back"
          // rightIcon="share"
          title="Add Note"
          >
          
          </Header>
          <Content>

            <View style={styles.textAreaContainer}>

              <TextInput
              autoFocus={true}
              style={{textAlignVertical: 'top',fontSize:17,lineHeight:27}}
              multiline={true}
              numberOfLines={10}
              underlineColorAndroid="transparent"
              onChangeText={(text) => this.onChangeTextNote({text})}
              value={this.state.text}/>

            </View>

          

          </Content>

          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

            <Row style={{ marginTop: 0, marginBottom: 0 }}>

                    <Button 
                      danger
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.cancelButton()}
                      
                    >
                      <Text>Cancel</Text>
                    </Button>

                    <Button 
                      success
                        style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0
                      }}

                      onPress= {() => this.submitButton()}

                    >
                      <Text>Submit</Text>
                    </Button>


            </Row>

          </Grid>

          </Footer>
        </Container>
    );
  }
} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(AddNote);