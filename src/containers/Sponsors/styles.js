import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerStyle: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  image: {
    resizeMode: 'stretch',
  },
  headerTextStyle: {
    color: Common.whiteColor,
    fontWeight: 'bold',
  },
  headerContainerStyle: {
    flex: 1,
    margin: 5,
  },
  text: {
    color: '#fff',
    fontSize: 16,
  },
  viewWrapper: {
    flex: 9,
  },
  tabStyle: {
    backgroundColor: Common.lightGreen,
  },
  viewSubheaderTab:{
    padding: 15,
    paddingLeft: 25,
    // backgroundColor: Common.lightGray,
    backgroundColor: "#f2f2f2"
  },
  viewSubheaderTabText:{
    fontSize:16,
    fontWeight:'bold'
  },
  SectionHeaderStyle:{
    backgroundColor : '#CDDC39',
    fontSize : 20,
    padding: 5,
    paddingLeft:20,
    color: '#fff'
  },
  viewStyle: {
    paddingTop: 15,
    paddingLeft: 0,
    paddingRight: 0,
    paddingBottom: 0,
    flex: 1,
  },
  gridStyle: {
    borderBottomWidth: 1,
    borderBottomColor: Common.lightGray,
    paddingBottom: 15,
  },
  titleItemName:{
    fontSize:15,
    fontWeight: 'bold',
    color: "#666666",
  },
  subtitleItemName:{
    fontSize:13,
    color: "#666666",
  }
});

module.exports = styles;
