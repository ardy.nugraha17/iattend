import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid,
  // Col,
  // Row
} from 'native-base';

import { Col, Row, Grid } from "react-native-easy-grid";

import { ToastAndroid, View, StatusBar, Image } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../Speakers/styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';

class DetailSponsors extends Component {


  onPressShare(){
    ToastAndroid.show('Still Development', ToastAndroid.SHORT);
  }

  render() {
    const sponsor=this.props.navigation.state.params;


    return (
        <Container style={styles.container}>

            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            // onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title={sponsor.name}
            />

            <Content>
            <View>
              <Content>
              {/* this is for icon avatar */} 


              <View style={styles.imageSpeaker}>

                <Thumbnail 
                style={{width: 200, height: 200, borderRadius: 200/2}}
                source={{uri: 'https://eventapp.dexagroup.com/apiget_file?fd='+sponsor.image}} />

              </View>

              {/* this is for name */}
              <View style={styles.titleSpeaker}>
              <Text style={styles.titleSpeakerText} >{sponsor.name}</Text>
              </View>

              <Content padder style={{paddingLeft:30,paddingRight:30}}>

              {/* this is for description */}
              <View style={styles.descriptionSpeaker}>
              <Text style={styles.descriptionSpeakerText}>{sponsor.description}</Text>
              </View>

              </Content>

              </Content>
            </View>
            </Content>
  
        </Container>
      );


  }
}


export default DetailSponsors;