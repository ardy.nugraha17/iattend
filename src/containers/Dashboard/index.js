import React, { Component } from 'react'
import {
  View,
  Image,
  StatusBar,
  Text,
  AsyncStorage, 
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  CameraRoll, 
  ToastAndroid,
  Alert,
  Vibration
} from 'react-native'
import { Tabs, Tab, Container, Content, Item, Input, Label, Footer,
   Icon,
  // Button
} from 'native-base';
import { connect } from 'react-redux';
// import Icon from "react-native-vector-icons/FontAwesome";
import styles from './styles';
import DrawerView from '../../component/DrawerView';
import EventList from '../../component/EventList';
import { eventList, eventListJune , mainText} from '../../common/constant';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import * as Helper from '../../common/helper';

import Spinner from 'react-native-loading-spinner-overlay';
import ButtonInternal from '../../component/Button';
// import { Button } from 'react-native-elements';

// import Icon from 'react-native-vector-icons/MaterialIcons';

import validate from '../../common/validate.js'


import Camera from 'react-native-camera';

// untuk capture camera 
import { RNCamera } from 'react-native-camera';
import ImageResizer from 'react-native-image-resizer';
import SignatureCapture from 'react-native-signature-capture';

import { Col, Row, Grid } from "react-native-easy-grid";
import ApiCaller from '../../common/apiCaller';
import axios from 'axios';
// import { Dialog  } from 'react-native-simple-dialogs';
import RNExitApp from 'react-native-exit-app';
// import QRCode from 'react-native-qrcode';

import {Dimensions} from "react-native";

const { height, width } = Dimensions.get('window');

export const w = percent => (width * percent) / 150;
export const h = percent => (height * percent) / 150;
export const totalSize = num => (Math.sqrt((height * height) + (width * width)) * num) / 150;
import Dialog, { DialogFooter, DialogButton, DialogContent, DialogTitle, SlideAnimation } from 'react-native-popup-dialog';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const timer = require('react-native-timer');
const DURATION = 10000;
const PATTERN = [1000, 2000, 3000];

const constraints = {
  name: {
    presence: {
      allowEmpty:false,
      message: '^Please write your name here'
    }
},
  instansi: {
  presence: {
    allowEmpty:false,
    message: '^Please write your company here'
  }
},
  jabatan: {
  presence: {
    allowEmpty:false,
    message: '^Please write your position here'
  }
},
  email: {
    presence: {
      allowEmpty:false,
      message: '^Please enter an email address'
    },
    email: {
      message: '^Please enter a valid email address'
    }
},
  password: {
    presence: {
      allowEmpty:false,
      message: '^Please enter a password'
    },
    length: {
      minimum: 5,
      message: '^Your password must be at least 5 characters'
    }
  },

  phone: {
    presence: {
      allowEmpty:false,
      message: '^Please write your phone number here'
    }
  },

}


const validator = (field, value) => {
  // Creates an object based on the field name and field value
  // e.g. let object = {email: 'email@example.com'}
  let object = {}
  object[field] = value

  let constraint = constraints[field]
  

  // Validate against the constraint and hold the error messages
  const result = validate(object, { [field]: constraint })
  

  // If there is an error message, return it!
  if (result) {
    // Return only the field error message if there are multiple
    return result[field][0]
  }

  return null
}




class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placeholderUsername:'Email address here',
      visibleDialog: false,
      search: false,
      showDrawer: true,
      showMore: true,
      torchMode: 'on',
      cameraType: 'back',
      dialogVisible:false,
      camereBarcodeValue:false,
      name:'',
      email:'',
      audienceid:'',
      counter:0,
      value:'',
      eventListCurrent:'',
      eventListPast:'',
      uid:'',
      phoneForm:'',
      emailForm:'',
      satu:0,
      statusDialog:0,
      cekInputUsername:false,
      emailError: '',
      pressComplete:false
  

    }


  }
  
  async componentDidMount(){

    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    // console.log("trace header dashboard",this.props.eventData.event.eventListCurrent);
    console.log("componentDidMount trace header dashboard",this.props);
    // console.log("imgPathTtd",this.props.navigation.state.params.imgPathTtd);
    this.checkPermission();
    this.createNotificationListeners(); //add this line



    // if(this.state.satu == 1){
    //   var str = this.props.navigation.state.params.mail;
    //   var n = str.indexOf("@");
  
    //   console.log("pressAutoComplete dashboard",str, n);
    //   if(n == -1){
    //     this.pressAutoComplete();
    //     this.setState({
    //       statusDialog:1,
    //       satu:2})
    //   }else{
        
    //     this.setState({
    //       statusDialog:0,
    //       satu:2})
    //   }


    // }


    

  }



  
  async componentWillMount() {

    console.log("trace header dashboard satu dua",this.state.satu);

    console.log("trace header dashboard satu",this.props.navigation.state.params.mail);
    this.setState({satu:1,
    email:this.props.navigation.state.params.mail});


    if(this.props.eventData.event.eventListCurrent != undefined){
      this.setState({
        emptyDataCurrent: false,
        eventListCurrent:this.props.eventData.event.eventListCurrent

      })
    }

    if(this.props.eventData.event.eventListPast != undefined){
      this.setState({
        emptyDataPast:false,
        eventListPast:this.props.eventData.event.eventListPast
      })
    }


    AsyncStorage.getItem("userData").then((data) => {
   
      console.log("barcodeReceived audienceId",JSON.parse(data));
      // console.log("submitButton", this.state.eventId + "data userdata" + data + ":" + this.state.textClass + ":" + this.state.textTrack);

      this.setState({
        email : JSON.parse(data).mail,
        name : JSON.parse(data).name,
        text: JSON.parse(data).audienceid + 
        ':' + JSON.parse(data).name + 
        ':' + JSON.parse(data).mail + 
        ':' + JSON.parse(data).deviceid,
        audienceid: JSON.parse(data).audienceid,
        uid:JSON.parse(data).uid
      });

    }).done();

    
  }


  componentWillReceiveProps (nextProps) {
    console.log("componentWillReceiveProps",nextProps.eventData);
    
    if(nextProps.eventData.event.eventListCurrent != undefined){
      this.setState({
        emptyDataCurrent: false,
        eventListCurrent:nextProps.eventData.event.eventListCurrent

      })
    }

    if(nextProps.eventData.event.eventListPast != undefined){
      this.setState({
        emptyDataPast:false,
        eventListPast:nextProps.eventData.event.eventListPast
      })
    }

    ToastAndroid.show('Sync data success ...', ToastAndroid.SHORT);

  }

  // just before the update
  componentWillUpdate (nextProps, nextState) {
    console.log("componentWillUpdate dua",nextProps.eventData.event.dataListCurrent);
    console.log("componentWillUpdate",nextProps.eventData.event.eventListCurrent);
    // this.setState({
    //   eventListCurrent: nextProps.eventListCurrent
    // })
  }

  // immediately after the update
  componentDidUpdate (prevProps, prevState) {
    console.log("componentDidUpdate")
  }


  closeDrawer = () => {
    this.drawer._root.close()
  }

  openDrawer = () => {
    this.drawer._root.open()
  }

  logoutAplikasi(){
    AsyncStorage.removeItem('userdata');

    let obj={
      'logout':'yes'
    };

    Helper.navigateToPage(this, 'Login', obj);
  }

  testAlert(){

    Alert.alert(
      'Logout Application',
      'Are you sure want to Logout the application?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => this.logoutAplikasi()},
      ],
      { cancelable: false }
    )
    return true;
  }


  getMappedData(data){
    let arr={};
    monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];  
    data.forEach(function(each) {
      let d=each.eventDate;
      let key=monthNames[parseInt(d.slice(5,7))-1]+'-'+d.slice(0,4);
      if(!arr[key]) arr[key]=[];
      arr[key].push(each);
      }); 
    return arr;
  }


  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  goBack(){
    if(this.state.statusDialog == 0){
      this.testAlert()
    }
  }



  showAlert(title, body) {
    Alert.alert(
      title, body,
      [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }

  onRightIconClick(){
    ToastAndroid.show('Sync data progress ...', ToastAndroid.SHORT);


    const value = {
      email:this.state.email
    }

    this.props.dispatch({
      type: 'FETCH_EVENT_LIST',
      payload: value
    });

  }

  // pressAutoComplete(){
  //   this.setState({
  //     visibleDialog:true,
  //     satu:2
  //   })
  //   // ToastAndroid.show('icon Plus', ToastAndroid.SHORT);
  // }

  pressCLoseDialog(){

    this.setState({
      visibleDialog:false,
      pressComplete:true
    });

    setTimeout(() => {
      this.setState({
        email:this.state.emailForm
      }); 

    }, 1000);


  }


  pressAutoCompleteHide(){

    console.log("phoneForm", this.state.phoneForm, this.state.emailForm);

    if(this.state.emailForm !='' && this.state.phoneForm !=''){


      ApiCaller('mobileIattend_update', 'post', 123456, {
        email : this.state.emailForm,
        name: this.state.name,
        phone : this.state.phoneForm,
        
    
      }).then(response =>{

        this.setState({
          statusDialog:0,
          visibleDialog:true,
          // email:this.state.emailForm
        });
  
      
      });



    }else{
      this.onBlurUsername(this.state.emailForm);
      this.onBlurPhone(this.state.phoneForm);
      ToastAndroid.show('Please fill the information', ToastAndroid.SHORT);
    }


    // ToastAndroid.show('icon Plus', ToastAndroid.SHORT);
  }

  onBlurUsernameRelease(){

    if(this.state.placeholderUsername == ''){
      this.setState({
        placeholderUsername:'Email address here'
      })
    }else{

    }
 }
 
 
   onBlurUsername(value) {

    this.setState({emailForm: value.toLowerCase().trim()});
    let emailError = validator('email', value);
    if(emailError){
      this.setState({
        toggle:true,
        emailError: emailError,
      })

      if(this.state.emailForm == ''){
        this.setState({
          placeholderUsername:'Email address here'
        })
      }

    }else{
      this.setState({
        toggle:false,
        emailError: emailError,
        cekInputUsername:true
      })
    }


  }

  onTouchStartUsername(){
    this.setState({placeholderUsername: ''});
  }

  onBlurPhone(value){
      // this.setState({phone: value.trim()});

      console.log('onBlurPhone', value);
      if(value == ''){
        let phoneError = "Please write your phone number here";

        this.setState({
          phoneForm: value.trim(),
          togglePhone: false,
          phoneError: phoneError
        });

      }else{
        this.setState({
          phoneForm: value.trim(),
          togglePhone: true,
          phoneError: ''
        });
      }
  }

  onBody(){


    if(this.state.email !== ''){
      // var str = this.props.navigation.state.params.mail;
      var str = this.state.email;
      var n = str.indexOf("@");
  
      console.log("pressAutoComplete dashboard",str, n);
      if(n == -1){

        // this.pressAutoComplete();
        // this.setState({
        //   statusDialog:1,
        //   satu:2})

        let { pressComplete, showDrawer, showMore, emailError, phoneError } = this.state;
        const {navigation} = this.props;

        return (
          <Container>
            
            <StatusBar
                hidden={true}
              />
            <Header
                headerContainer={styles.headerContainerStyleDua}
                headerTextStyle={styles.headerTextStyleDua}
                headerLeft={{padding: 10}}
                // onLeftIconClick={showDrawer ? () => this.openDrawer(): null}
                // onRightIconClick={() => this.setState({ search : !this.state.search})}
                // onRightIconClick={() => this.onRightIconClick()}
                // onMoreIconClick={showMore ? () => this.openDrawer(): null}
                // leftIcon={showDrawer ? "ios-menu" : null}
                // rightIcon="ios-search"
                // search={this.state.search}
                // rightIconTiga="sync"
                title={"Update information"}
                {...this.props}
            />


            <Content style={{padding:20}}>

              <View>
                
                {/* Dialog content */}

                <Dialog
                width= {0.8}
                visible={this.state.visibleDialog}
                dialogTitle={<DialogTitle title="Update Success" align="left" />}
                footer={
                  <DialogFooter
                  >
                    <DialogButton
                      textStyle={{color:'#666666', fontSize:wp('2.2%')}}
                      text="CLOSE"
                      onPress={() => {this.pressCLoseDialog()}}
                    />
                  </DialogFooter>
                }
                dialogAnimation={new SlideAnimation({
                  slideFrom: 'bottom',
                })}
                >
                <DialogContent>
                <View style={{height:hp('15%'), marginTop:10}}>
                  <ScrollView style={{ flex: 1 }}>
                    <Text style={{textAlign:'center', alignContent:'center', alignItems: 'center',}}>Registration Success. Thank you {this.state.name} for registering IAttend</Text>
                  </ScrollView>
                  </View>  
                  </DialogContent>
                </Dialog>


                {/* Akhir dialog */}


            
                <Label style={styles.label}>Email</Label>
                <Item style={styles.inputStyleForm}>
                <Icon active name='ios-person' style={{color:'#af1e1e'}} />
                <Input
                  autoCorrect={false} 
                  onChangeText={(value) => this.onBlurUsername(value)}
                  onBlur={() => this.onBlurUsernameRelease()}
                  onTouchStart={()=> this.onTouchStartUsername()}
                  value={this.state.emailForm}
                  placeholder={this.state.placeholderUsername}
                  style={styles.InputTextStyle}
                  placeholderTextColor= '#666666'
                />
                </Item>
                {emailError ? 
                  <Text style={{color:"#e71636", paddingTop:3, paddingBottom:0}}>{emailError}</Text>
                  : null }


                <Label style={styles.label}>Phone Number</Label>
                <Item style={styles.inputStyleForm}>
                <Icon active name='ios-call' style={{color:'#af1e1e'}} />
                <Input
                  autoCorrect={false} 
                  onChangeText={(value) => this.onBlurPhone(value)}
                  value={this.state.phoneForm}
                  placeholder={"Phone number here"}
                  style={styles.InputTextStyle}
                  placeholderTextColor= '#666666'
                />
                </Item>
                {phoneError ? 
                <Text style={{color:"#e71636", paddingTop:0, paddingBottom:3}}>{phoneError}</Text>
                : null }



                <AndroidBackButton
                        onPress={() => {
                            console.log("onPress dua");
                            this.testAlert();
                            return "ssss";
                        }}
                    />


                </View> 

            </Content>

            { !pressComplete ? 

                        <Footer style={{backgroundColor:'#ececec'}}>
                        <View style={{width:Dimensions.get('window').width}}>
                          <ButtonInternal
                            buttonTextStyle={styles.buttonTextStyleDua}
                            text="Submit"
                            style={styles.buttonStyleDua}
                            onPress={() => this.pressAutoCompleteHide()}
                          />
                        </View>
                      </Footer>

                      : null
            }




          </Container> 
        )


      }else{
        
        // this.setState({
        //   statusDialog:0,
        //   satu:2})

        let { showDrawer, showMore, emailError, phoneError } = this.state;
        const {navigation} = this.props;
    
    
    
    
    
        return (
          <DrawerView
            onClose={() => this.closeDrawer()}
            referVar={(ref) => { this.drawer = ref; }}
            content={<SideMenu {...this.props} onClose={() => this.closeDrawer()} />}
          >
            <View style={styles.container}>
    
              <StatusBar
                 hidden={true}
               />
              <Header
                headerContainer={styles.headerContainerStyle}
                headerTextStyle={styles.headerTextStyle}
                headerLeft={{padding: 10}}
                onLeftIconClick={showDrawer ? () => this.openDrawer(): null}
                // onRightIconClick={() => this.setState({ search : !this.state.search})}
                onRightIconClick={() => this.onRightIconClick()}
                onMoreIconClick={showMore ? () => this.openDrawer(): null}
                leftIcon={showDrawer ? "ios-menu" : null}
                // rightIcon="ios-search"
                // search={this.state.search}
                rightIconTiga="sync"
                title={mainText}
                {...this.props}
              />
              <View style={styles.viewWrapper}>
    
              <Tabs initialPage={0}>
                  <Tab heading="CURRENT & UPCOMING"
                    tabStyle={styles.tabStyle}
                    activeTextStyle={styles.text}
                    activeTabStyle={styles.tabStyle}
                    textStyle={styles.text}> 
    
    
                    { 
                      this.state.emptyDataCurrent == true ?
                      <View style={{width:'100%', padding:10, backgroundColor:'#ececec'}}>
                      <Text style={{textAlignVertical: "center",textAlign: "center",}}>Sorry, there is currently no event</Text>
                      </View>
                      
                    :
    
                    <ScrollView>
                      {
                        Object.keys(this.getMappedData(this.state.eventListCurrent)).map((data, i) => (
                        <EventList eventList={this.getMappedData(this.state.eventListCurrent)[data]} date={data} jumBaris="12" nav={this.props.navigation} {...this.props} scrolldata="current"/>
                      ))}
                      
                    </ScrollView>
                    
                    }
    
    
    
                  </Tab>
                  <Tab heading="PAST"
                    tabStyle={styles.tabStyle}
                    activeTextStyle={styles.text}
                    activeTabStyle={styles.tabStyle}
                    textStyle={styles.text}>
    
                    { 
                      this.state.emptyDataPast == true ?
                      <View style={{width:'100%', padding:10, backgroundColor:'#ececec'}}>
                      <Text style={{textAlignVertical: "center",textAlign: "center",}}>Sorry, there is empty past event</Text>
                      </View>
                      
                    :
    
                      <ScrollView>
                      {
                          Object.keys(this.getMappedData(this.state.eventListPast)).map((data, i) => (
                          <EventList eventList={this.getMappedData(this.state.eventListPast)[data]} date={data} nav={this.props.navigation} {...this.props} scrolldata="past"/>
                      ))}
                      </ScrollView>
                    
                    }
    
    
    
                  </Tab>
                </Tabs>
    
    
              </View>
    
    
            </View>
          </DrawerView>
        )

      }


    }

  }

  render () {

    let { showDrawer, showMore, emailError, phoneError } = this.state;
    const {navigation} = this.props;


    return (
      this.onBody()
      
    )
  }
}


  const mapStateToProps = state => ({
    eventData: state.eventData,
  });

  export default connect(mapStateToProps)(Dashboard);