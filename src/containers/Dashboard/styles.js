import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerStyle: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  image: {
    resizeMode: 'stretch',
  },
  headerTextStyle: {
    color: Common.whiteColor,
    fontWeight: 'bold',
  },
  headerTextStyleDua: {
    color: Common.whiteColor,
    fontWeight: 'bold',
  },
  headerContainerStyle: {
    flex: 1,
    margin: 5,
  },
  headerContainerStyleDua: {
    flex: 1,
    margin: 5,
  },
  label:{
    fontWeight:'bold', 
    marginTop:wp('2%'),
    fontSize:hp('1.5%')
  },
  text: {
    color: '#fff',
    // fontSize: 13,
    // fontSize:16,
    fontSize: 13
  },
  viewWrapper: {
    flex: 9,
    // flex:1,
    // padding:25
  },
  tabStyle: {
    backgroundColor: Common.lightGreen,
  },

  bodyForm: {
    flex: 1,
    paddingTop:40,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#F2F2F2',
    // height:'100%',
    flexDirection: 'column',
    // backgroundColor: 'black'
  },
  logInViewStyle: {
    marginLeft: 20,
    marginRight: 20,
    flex: 2.5,
  },
  inputStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    // paddingLeft:10,
    backgroundColor: Common.whiteColor,
    borderBottomWidth:0,
    marginBottom:1,
    // backgroundColor:"#ffffff"
  },
  textInputAlt: {
    borderColor: '#e71636',
    borderTopWidth: 0.8,
    borderRightWidth:0.8,
    borderBottomWidth:0.8,
    borderLeftWidth: 0.8,
    // paddingLeft:10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: Common.whiteColor,
    marginBottom:0
  },
  buttonTextStyle: {
    color: Common.darkColor
  },
  buttonStyle: {
    flex: 1, justifyContent: "center", 
    alignItems: "center", height: 50,
    marginTop: 25,
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#666666',
    borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 10,
  },

  buttonTextStyleDua: {
    color: Common.darkColor
  },
  buttonStyleDua: {
    flex: 1, justifyContent: "center", 
    alignItems: "center", 
    // height: 50,
    // marginTop: 25,
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#ececec',
    // borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 10,
  },

  buttonStyleIsi: {
    flex: 1, justifyContent: "center", 
    alignItems: "center", height: 50,
    marginTop: 25,
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#BBFF96',
    borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 10,
  },
  buttonStyleCamera: {
    flex: 1, justifyContent: "center", 
    alignItems: "center", height: 50,
    marginTop: 25,
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#666666',
    borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 10,
    marginRight: 10,
  },
  buttonStyleCameraIsi: {
    flex: 1, justifyContent: "center", 
    alignItems: "center", height: 50,
    marginTop: 25,
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#BBFF96',
    borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 10,
    marginRight: 10,
  }, 
  signature: {
    flex: 1,
    borderColor: '#000033',
    borderWidth: 1,
  },
  // buttonStyle: {
  //     flex: 1, justifyContent: "center", alignItems: "center", height: 50,
  //     backgroundColor: "#eeeeee",
  //     color:'#666666',
  //     margin: 10
  // },
  preview: {
    width: '100%',
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20
  }, 
  containerCamera: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  submitButtonStyle:{
    color:'#666666'
  },
  signature: {
    flex: 1,
    borderColor: '#000033',
    borderWidth: 1,
  },


  borderImage:{
    // flex: 1,
    // These below are most important, they center your border view in container
    // ref: https://css-tricks.com/snippets/css/a-guide-to-flexbox/
    // alignItems: "center",
    // justifyContent: "center"
    // width:'70%',
    // height:'50%'
    flex: 1,
    aspectRatio: 0.4, 
    resizeMode: 'contain',
  }
});

module.exports = styles;
