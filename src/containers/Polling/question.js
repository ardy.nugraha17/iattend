import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  Grid,
  Col,
  Row,
  // CheckBox,
  // Radio
  Footer
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, AsyncStorage, BackHandler } from 'react-native';
import styles from '../Polling/styles';

import Header from '../../component/Header';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';

import RadioButton from 'radio-button-react-native';
import ApiCaller from '../../common/apiCaller';

class Detail extends Component {


  constructor(props) {

    super(props);
    this.state = {
      showDialog: false,
      dialogVisible: false,
      listPollingChoose:'',
      Question:'',
      selectedFriendId:false,
      value:0,
      accessToken:'',
      audienceid:'',
      pollingId:'',
      exist:false,
      dataNull:true,
      dataLengthPolling:'',
      pollingId:'',
      eventId:''

    }
  }


  componentDidMount(){
    // console.log("trace detail", this.props.navigation.state.params.ListChooseData);
    console.log("trace detail pol", this.props.navigation.state.params.navigation);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  goBack(){
    this.props.navigation.goBack(null);
  }



  componentWillReceiveProps(next){
    console.log("dataLengthPolling",next.pollingdetailData.pollingQuestion.exist);

    if(next.pollingdetailData.pollingQuestion.exist == true){
      // Jika audience ini sudah pernah melakukan polling di number ini
      this.setState({
        Question:'',
        exist:true
      })

    }else{

         this.setState({

            Question:next.navigation.state.params.pollingQuestion,
            pollingId:next.pollingdetailData.pollingQuestion.datanya.byListPolling[0].pollingNumber,
            listPollingChoose:next.pollingdetailData.pollingQuestion.datanya.byListChoose,
            eventId: next.pollingdetailData.pollingQuestion.datanya.byListPolling[0].eventId,
            agendaId: next.pollingdetailData.pollingQuestion.datanya.byListPolling[0].agendaId,

            // Question: next.pollingData.polling.datanya.byListPolling[0].pollingQuestion,
            // pollingId: next.pollingData.polling.datanya.byListPolling[0].id,
            // listPollingChoose:next.pollingData.polling.datanya.byListChoose,
            dataNull:false,
            exist:false
          }) 


    }

    // this.setState({dataLengthPolling:next.pollingdetailData.pollingQuestion.datanya.byListPolling.length});

    // if(next.pollingdetailData.polling.length == undefined){

    //   console.log("trace detail bawah pertama",next);

    //   if(next.pollingdetailData.pollingQuestion.datanya.exist == false){

    //     if(next.pollingdetailData.pollingQuestion.datanya.byListPolling.length == 0){

    //       console.log("trace detail bawah pertama abc",next);

    //     }else{

    //       console.log("trace detail bawah pertama abcd",next);

    //       this.setState({

    //         Question:next.navigation.state.params.pollingQuestion,
    //         pollingId:next.pollingdetailData.pollingQuestion.datanya.byListPolling[0].pollingNumber,
    //         listPollingChoose:next.pollingdetailData.pollingQuestion.datanya.byListChoose,
    //         eventId: next.pollingdetailData.pollingQuestion.datanya.byListPolling[0].eventId,
    //         agendaId: next.pollingdetailData.pollingQuestion.datanya.byListPolling[0].agendaId,

    //         // Question: next.pollingData.polling.datanya.byListPolling[0].pollingQuestion,
    //         // pollingId: next.pollingData.polling.datanya.byListPolling[0].id,
    //         // listPollingChoose:next.pollingData.polling.datanya.byListChoose,
    //         dataNull:false
    //       }) 

    //     }






    //   }else{
    //     this.setState({
    //       exist:true,
    //       dataNull:false
    //     })
    //   }


    // }





  }
  

  componentWillMount() {
    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        // console.log("trace anya",data);
        this.setState({
          accessToken: JSON.parse(data).token,
          audienceid: JSON.parse(data).audienceid
        })
      }
    }).done();


  }


  onCheckBoxPress(id) {

    this.setState({
      selectedFriendId: true
    })
    console.log("trace detail box", this.state.selectedFriendId);
    
  }

  handleOnPress(value){
    console.log("trace detail back", value);
    this.setState({value:value})
  }

  onPressClose(){
    this.props.navigation.goBack(null);
  }


  onPressSubmit(){

    if(this.state.value == 0){
      ToastAndroid.show('Please choose one', ToastAndroid.SHORT);
      return;
    }else{
      ApiCaller('mobileIattend_polling_register', 'post', this.state.accessToken, {
        audience_id : this.state.audienceid,
        event_id : this.state.eventId,
        agenda_id : this.state.agendaId,
        anserwer_value : this.state.value,
        polling_id : this.state.pollingId
    
      }).then(response =>{

        console.log("trace mobileIattend_polling_register", response);

        if(response.insert == true){
          ToastAndroid.show('Polling has been submit', ToastAndroid.SHORT);
          this.props.navigation.goBack(null);
          // this.setState({ dialogVisible: true })
          // console.log("onPressAttendees",response);

        }else{

          // ToastAndroid.show('You are already registered, {\n} please check your email', ToastAndroid.SHORT);
          // console.log("trace registerPolling",response.message);
          ToastAndroid.show('Polling failed to submit', ToastAndroid.SHORT);

        }
      
      })
    }

  }


  body(items){

      if(this.state.exist == false){
          return(
            <View style={styles.aboutText}>
            <List>

            {
              items != '' ? 
            
              items.map((item, index, key)=>(

                <ListItem>
                  <RadioButton 
            
                  // currentValue={this.state.value} value={item.id} onPress={this.handleOnPress.bind(this)}></RadioButton>
                  currentValue={this.state.value} value={item.id} onPress={this.handleOnPress.bind(this)}></RadioButton>
                  
                  <Body>
                  <Text>{item.answer_text}</Text>
                  </Body>

                </ListItem>

                ))

                : 

                null
                
            }


          </List>
        </View>
        )
      }else{

        return(
          <View style={{padding:20, backgroundColor:'#ececec'}}>
          <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold'}}>Sorry. You have previously polled for this poll number</Text>
          </View>
        )

      }



  }


  footer(){


    if(this.state.dataNull != true){

      if(this.state.exist == false){

        return(
  
            <Grid>
  
            <Row style={{ marginTop: 0, marginBottom: 0 }}>
  
              <Button 
                onPress ={() => this.onPressClose()}
                light
                style = {{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  flex: 1,
                  justifyContent: "center",
                  borderWidth:0,
                  height:55,
                  borderRadius:0,
                }}
                
              >
                <Text>Close</Text>
              </Button>
  
              <Button 
                onPress={() => this.onPressSubmit()}
                success
                style = {{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  flex: 1,
                  justifyContent: "center",
                  borderWidth:0,
                  height:55,
                  borderRadius:0,
                }}
                
              >
                <Text>Submit</Text>
              </Button>
  
            </Row>
  
            </Grid>
  
      )
      }else{
  
        return (
  
        <Grid>
  
        <Row style={{ marginTop: 0, marginBottom: 0 }}>
  
        <Button 
          onPress ={() => this.onPressClose()}
          success
          style = {{
            flexDirection: "row",
            flexWrap: "wrap",
            flex: 1,
            justifyContent: "center",
            borderWidth:0,
            height:55,
            borderRadius:0,
          }}
          
        >
          <Text>Close</Text>
        </Button>
  
        </Row>
  
        </Grid>
  
        )
      }

    }else{

      return (
  
        <Grid>
  
        <Row style={{ marginTop: 0, marginBottom: 0 }}>
  
        <Button 
          onPress ={() => this.onPressClose()}
          success
          style = {{
            flexDirection: "row",
            flexWrap: "wrap",
            flex: 1,
            justifyContent: "center",
            borderWidth:0,
            height:55,
            borderRadius:0,
          }}
          
        >
          <Text>Close</Text>
        </Button>
  
        </Row>
  
        </Grid>
  
        )

    }


  }



  render() {
   
    const speaker= this.props.navigation.state.params;
    var items = this.state.listPollingChoose;
    const {navigation} = this.props;

    return (
        <Container style={styles.container}>


            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Detail Polling"
            />
  
          <Content>


          {
          
          this.state.exist == false ? 

          <View style={{backgroundColor:'#ececec', padding:20}}>
          <Text 
          style={{textAlign:'left'}}>{this.state.Question}
          </Text>
          </View>

          :

          null
          
          }




          {this.body(items)}
             
          </Content>
  
          <Footer style={{ backgroundColor: "transparent" }}>

          {this.footer()} 

          </Footer>

        </Container>
      );


  }
}


const mapStateToProps = state => ({
  pollingdetailData: state.pollingdetailData,
});

export default connect(mapStateToProps)(Detail);