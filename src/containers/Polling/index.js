import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  Grid,
  Col,
  Row,
  // CheckBox,
  // Radio
  Footer
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, AsyncStorage, BackHandler } from 'react-native';
import styles from '../Polling/styles';

import Header from '../../component/Header';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';

import RadioButton from 'radio-button-react-native';
import ApiCaller from '../../common/apiCaller';

class Detail extends Component {


  constructor(props) {

    super(props);
    this.state = {
      listPollingChoose:'',
      accessToken:'',
      audienceid:'',

    }
  }


  componentDidMount(){
    // console.log("trace detail", this.props.navigation.state.params.ListChooseData);
    console.log("trace detail pol", this.props.navigation.state.params.navigation);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  goBack(){
    this.props.navigation.goBack(null);
  }



  componentWillReceiveProps(next){
    console.log("dataLengthPolling",next.pollingData.polling.datanya.byListPolling);

    this.setState({

      listPollingChoose:next.pollingData.polling.datanya.byListPolling

    })

  }
  

  componentWillMount() {
    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        // console.log("trace anya",data);
        this.setState({
          accessToken: JSON.parse(data).token,
          audienceid: JSON.parse(data).audienceid
        })
      }
    }).done();


  }

  onPressPolling(props){

    const eventId = {
      eventId: props.eventId,  
      agendaId: props.agendaId,
      audienceId: this.state.audienceid,
      pollingNumber:props.pollingNumber
    }
    this.props.dispatch({
      type: 'FETCH_POLLING_QUESTION',
      payload: { ...eventId }
    });

    this.props.navigation.navigate('PollingQuestion', { ...props });


  }

  onPressClose(){
    this.props.navigation.goBack(null);
  }



  render() {
   
    var items = this.state.listPollingChoose;
    const {navigation} = this.props;

    return (
        <Container style={styles.container}>


            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="List Polling"
            />
  
          <Content>


          <View style={{backgroundColor:'#ececec', padding:20}}>
            <Text 
            style={{textAlign:'left'}}>
            Next is the list of polls. Please choose then answer it
            </Text>
          </View>


          <View style={styles.aboutText}>
            <List>

            {
              items != '' ? 
            
              items.map((item, index, key)=>(

                <ListItem style={{paddingLeft:8, marginLeft:0}} button onPress={() => this.onPressPolling(item)}>
                  <Body>
                  <Text note>Polling {index + 1}.</Text>  
                  <Text>{item.pollingQuestion}</Text>
                  </Body>

                </ListItem>

                ))

                : 

                null
                
            }


            </List>
          </View>
             
          </Content>
  
          {/* <Footer style={{ backgroundColor: "transparent" }}>

            <Grid>
    
            <Row style={{ marginTop: 0, marginBottom: 0 }}>

            <Button 
              onPress ={() => this.onPressClose()}
              success
              style = {{
                flexDirection: "row",
                flexWrap: "wrap",
                flex: 1,
                justifyContent: "center",
                borderWidth:0,
                height:55,
                borderRadius:0,
              }}
              
            >
              <Text>Close</Text>
            </Button>

            </Row>

            </Grid>

          </Footer> */}

        </Container>
      );


  }
}


const mapStateToProps = state => ({
  pollingData: state.pollingData,
});

export default connect(mapStateToProps)(Detail);