import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';



class AddPolling extends Component {


  constructor(props) {
    super(props);

    // this.onChangeItem = this.onChangeItem.bind(this);

    this.state = {

      dialogVisible:false,
      textOption:0,
      countItem:0,
      question:"",

      answer1:'',
      answer2:'',
      answer3:'',

    };

  }

  componentDidMount(){
    console.log("register event",this.props.navigation.state.params);
  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  cancelButton(){
    console.log("cancelButton");
    this.props.navigation.goBack(null);
  }

  submitButton(){
    

    AsyncStorage.getItem("userData").then((data) => {

      console.log("submitButton",JSON.parse(data));



      ApiCaller('addpolling', 'post', JSON.parse(data).token, {
        // moderatorId : JSON.parse(data).audienceid,
        agendaId : JSON.parse(data).agendaId,
        question : this.state.question,
        answer1 : this.state.answer1,
        answer2 : this.state.answer2,
        answer3 : this.state.answer3
        
    
      }).then(response =>{


        console.log("response", response);

        if(response == "sukses"){


          const value = {
            'agendaId': JSON.parse(data).agendaId,
            'moderatorId': JSON.parse(data).audienceid
          }
      
          this.props.dispatch({
            type: 'FETCH_EVENT_LIST',
            payload: value
          });


          ToastAndroid.show('success to submit', ToastAndroid.SHORT);
          this.props.navigation.navigate('Home');

        }else{
          ToastAndroid.show('Failed to submit', ToastAndroid.SHORT);
        }
        


      
      })











      
    }).done();

  
    this.props.navigation.navigate('Home');
  }

  onChangeQuestionList(text){
    console.log("trace anya onChangeItem",text);
    this.setState({
      question:text
    });
  }


  onChangeAnswerList(answerNumber, text){

    console.log('trace onChangeAnswerList', answerNumber, text)

      switch (answerNumber) {
        case 1:
            this.setState({
              answer1:text
            })
            break;
        case 2:
            this.setState({
              answer2:text
            })
            break;
        case 3:
            this.setState({
              answer3:text
            })
            
      }
    

  }


  render() {

    return (
        <Container style={styles.container}>


          <Header
          headerContainer={styles.headerContainerStyle}
          headerTextStyle={styles.headerTextStyle}
          headerLeft={{padding: 10}}
          onLeftIconClick={() => this.props.navigation.goBack(null)}
          onRightIconClick={() => this.onPressShare()}
          leftIcon="md-arrow-back"
          // rightIcon="share"
          title="Add Polling"
          />
          <Content padder
          style={{ padding:15}}
          >

              <Label
              style={{marginBottom:3, marginTop:3, fontSize:14, fontWeight:'bold'}}
              >Question :</Label>
              <Textarea 
              rowSpan={5} 
              bordered 
              placeholder="Please insert text question polling here ..."
              onChangeText ={(text) => this.onChangeQuestionList(text)} 
              />

              <View>
                <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer 1 : </Label>
                    <Input
                    style={{fontSize:14}}
                    value = {this.state.answer1}
                    onChangeText = {(text) => this.onChangeAnswerList(1, text)}
                    />
                  </Item>
              </View>

              <View>
                <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer 2 : </Label>
                    <Input
                    style={{fontSize:14}}
                    value = {this.state.answer2}
                    onChangeText = {(text) => this.onChangeAnswerList(2, text)}
                    />
                  </Item>
              </View>

              <View>
                <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer 3 : </Label>
                    <Input
                    style={{fontSize:14}}
                    value = {this.state.answer3}
                    onChangeText = {(text) => this.onChangeAnswerList(3, text)}
                    />
                  </Item>
              </View>

          </Content>

          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

            <Row style={{ marginTop: 0, marginBottom: 0 }}>

                    <Button 
                      danger
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.cancelButton()}
                      
                    >
                      <Text>Cancel</Text>
                    </Button>

                    <Button 
                      success
                        style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0
                      }}

                      onPress= {() => this.submitButton()}

                    >
                      <Text>Submit</Text>
                    </Button>


            </Row>

          </Grid>

          </Footer>
        </Container>
    );
  }
} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(AddPolling);