import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  // Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage,
  TouchableHighlight,
  ActivityIndicator
} from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';
import PieChart from 'react-native-pie-chart';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

// const eventListEvaluation = [
//   {id:1, name:'Tom Cruise',image:'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Tom_Cruis.jpg/250px-Tom_Cruis.jpg'},
//   {id:2, name:'Johnny Depp',image:'https://m.media-amazon.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1_UY317_CR4,0,214,317_AL_.jpg'},
//   {id:3, name:'Kate Winslet', image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKRaFPucye6r1C1gJpCy7QX0XMJlQZzFW7JqZKajM686SZBl8h'},
//   {id:4, name:'Natalie Portman', image:'https://www.biography.com/.image/t_share/MTE4MDAzNDEwNzU3MDYwMTEw/natalie-portman-9542326-1-402.jpg'},
//   {id:5, name:'Cecilia Cheung', image:'https://i.mydramalist.com/kAqRwc.jpg'},
//   {id:6, name:'Andy Lau', image:'https://s.kaskus.id/r480x480/images/fjb/2016/03/29/koleksi_film_andy_lau_725487_1459244874.jpg'},
//   {id:7, name:'Stephen Chow', image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdMtWGVTQtAH5SoB_XKBRuvVCAR5OmurfuljnGIUupYAWNUwp1'}
// ];


const eventListEvaluationAudience = [
  {id:1, name:'Isu-isu yang didiskusikan menjawab permasalahan', answer: 'Sangat tidak setuju'},
  {id:2, name:'Saya Berpartisipasi aktif dalam pelatihan', answer: 'Maybe'},
  {id:3, name:'Fasilitas dan suasana tempat pelatihan mendukung saya belajar', answer:'Setuju'},
  {id:4, name:'Ruangan sangat nyaman untuk event ini', answer:'Setuju'},
  {id:5, name:'Tempatnya nyaman dan baik', answer:'Setuju'},
  {id:6, name:'Saya berpatisipasi',answer:'Setuju'},
  {id:7, name:'Pembicara event ini sangat menguasai materi',answer:'Setuju'},
];


const chart_wh = 250
const arrayJawaban = []

const seriesJawab = [1,1,1]

class AddEvaluation extends Component {


  constructor(props) {
    super(props);

    this.state = {
      listEvaluation: '',
      listEvaluationAudience: eventListEvaluationAudience,
      dialogVisible:false,
      detailpollingchoose:'',
      stateNol:1,
      stateSatu:1,
      stateDua:1,
      listAnswer:'',
      listAnswerArray:[],
      listPollingArray:[],
      listNol:[1],
      listSatu:[1],
      listDua:[1]
    };


  }


  componentDidMount(){
    console.log("componentDidMount",this.props.navigation.state.params.pollingQuestion);

  }

  componentWillMount(){

    console.log("componentWillMount",this.props);

    // id:this.props.navigation.state.params.id,
    // agendaId:this.props.navigation.state.params.id,
    // pollingNumber:this.props.navigation.state.params.id,
    // pollingQuestion:this.props.navigation.state.params.pollingQuestion,
    // heading : titleNote,





  }



  componentWillReceiveProps(next){
    console.log("componentWillReceiveProps",next.questionData.question);

    this.setState({
      listEvaluationSum:next.questionData.question.questionListAudience.length,
      listEvaluation:next.questionData.question.questionListAudience,
      listAnswer:next.questionData.question.questionListPolling
    })

    next.questionData.question.questionListPolling.map((item, index, key)=>(
      this.state.listAnswerArray.push(item.answer_text)
    ))

    next.questionData.question.questionListPolling.map((item, index, key)=>(
      this.state.listPollingArray.push({"index":index, "answer_text":item.answer_text})
    ))

    var i;
    for (i = 0; i < next.questionData.question.questionListAudience.length; i++) { 
        
        let obj = this.state.listPollingArray.find(o => o.answer_text === next.questionData.question.questionListAudience[i].jawaban);
        console.log("trace anya view dua", obj);

        if(obj.index == 0){
          this.state.listNol.push(1);
        }
        else if(obj.index == 1){
          this.state.listSatu.push(1);
        }
        else{
          this.state.listDua.push(1);
        }

    }


  }




  render() {


    // const { attendees } = this.props.attendeesData;
    // const {eventAddress, eventTitle, eventDescription, source,} = this.props.navigation.state.params;


    const series = [this.state.listNol.length,this.state.listSatu.length,this.state.listDua.length];
    const sliceColor = ['#F44336','#2196F3','#FFEB3B']


    return (
      
        <Container style={styles.container}>

          <Header
          headerContainer={styles.headerContainerStyle}
          headerTextStyle={styles.headerTextStyle}
          headerLeft={{padding: 10}}
          onLeftIconClick={() => this.props.navigation.goBack(null)}
          // onRightIconClick={() => this.onPressShare()}
          leftIcon="md-arrow-back"
          // rightIcon="share"
          title="Detail Polling"
          />
          <Content contentContainerStyle={{ flex: 1 }}>


          <Grid>
              <Row size={40}>


                  <Grid>
                      <Col size={50}>
                      
                      <View
                      style={styles.headertitleviewevaluationKiri}
                      >

                      <PieChart
                        chart_wh={chart_wh}
                        series={series}
                        sliceColor={sliceColor}
                      />

                      </View>
                      
                      </Col>

                      <Col size={50}>

                      <View style={styles.headertitleviewevaluation}>

                      <View style={{
                          flex: 1,
                          flexDirection: 'column',
                          justifyContent: 'center',
                          alignItems: 'stretch',
                        }}>
                          <View style={{height: 60, paddingBottom:10 }} >
                          <Text style={styles.teksTitle}>
                          {this.props.navigation.state.params.pollingQuestion}
                          </Text>
                          </View>

                          <View style={{height: 15, marginTop:20}}>
                          <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{width: 10, height: 12, backgroundColor: '#F44336', marginTop:3, marginRight:10, marginBottom:10}} />
                            <View style={{height: 50, marginBottom:10}}><Text style={{fontSize:14}}>
                            {this.state.listAnswerArray != undefined ? this.state.listAnswerArray[0] : null } :  
                            {this.state.listNol.length == 1 ? 0 : parseInt(this.state.listNol.length) - parseInt(1)}
                            </Text> 

                            </View>
                          </View>
                          </View>


                          <View style={{height: 15, marginTop:5}}>
                          <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{width: 10, height: 12, backgroundColor: '#2196F3', marginTop:3, marginRight:10, marginBottom:10}} />
                            <View style={{height: 50, marginBottom:10}}><Text style={{fontSize:14}}>
                            {this.state.listAnswerArray != undefined ? this.state.listAnswerArray[1] : null } :  
                            {this.state.listSatu.length == 1 ? 0 : parseInt(this.state.listSatu.length) - parseInt(1)}
                            </Text>

                            </View>
                          </View>
                          </View>

                          <View style={{height: 15, marginTop:5}}>
                          <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{width: 10, height: 12, backgroundColor: '#FFEB3B', marginTop:3, marginRight:10, marginBottom:10}} />
                            <View style={{height: 50, marginBottom:10}}><Text style={{fontSize:14}}>
                            {this.state.listAnswerArray != undefined ? this.state.listAnswerArray[2] : null } :  
                            {this.state.listDua.length == 1 ? 0 : parseInt(this.state.listDua.length) - parseInt(1)}
                            </Text>

                            </View>
                          </View>
                          </View>


                        </View>

                      </View>

                      </Col>
                  </Grid>
              

              </Row>

              {/* baris kedua */}
              <Row size={5}>
              <View style={styles.titleListParticipant}>
                <Text>List participant : {this.state.listEvaluationSum}</Text>
              </View>
              </Row>
              
              {/* baris ketiga */}
              <Row size={50}>
              
              <Content>  

              <View>
                  {
                    this.state.listEvaluation != '' ? 

                    <List dataArray={this.state.listEvaluation} thumbnail
                    renderRow={(item) =>
                      //  console.log("trace PollingDetail bawah dua",item.id)
                      <ListItem thumbnail>
                      <Left>
                        <Thumbnail source={{ uri: 'http://192.168.90.95:1337/get_file?fd='+item.image }} />
                      </Left>
                      <Body>
                        <Text>{item.name}</Text>
                        <Text style={styles.baseText}>
                                <Text style={styles.titleTextAtas} onPress={this.onPressTitle}>Answer : </Text>
                                <Text numberOfLines={5} style={styles.titleText}>{item.jawaban}</Text>
                        </Text>
                        <Text style={styles.baseText}>
                                <Text style={styles.titleTextAtas} onPress={this.onPressTitle}>Email : </Text>
                                <Text numberOfLines={5} style={styles.titleText}>{item.email}</Text>
                        </Text>
                        <Text style={styles.baseText}>
                                <Text style={styles.titleTextAtas} onPress={this.onPressTitle}>Phone : </Text>
                                <Text numberOfLines={5} style={styles.titleText}>{item.phone}</Text>
                        </Text>
                      </Body>
                      <Right>
                        <Button transparent>
                          <IconFontAwesome size={24} active name="user" />
                        </Button>
                      </Right>
                    </ListItem>
                    }>
                </List>
                      : 

                      <View style={[styles.containerIndicator, styles.horizontalIndicator]}>
                      <ActivityIndicator size="large" color="#666666" />
                      </View>

                  }
                  </View>        

              </Content>
                    

              </Row>
          </Grid>

          

          </Content>

          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

            <Row style={{ marginTop: 0, marginBottom: 0 }}>

                    <Button 
                      light
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.props.navigation.goBack(null)}
                      
                    >
                      <Text>Back</Text>
                    </Button>

            </Row>

          </Grid>

          </Footer>
        </Container>
    );
  }
} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
  questionData: state.questionData,
});

export default connect(mapStateToProps)(AddEvaluation);