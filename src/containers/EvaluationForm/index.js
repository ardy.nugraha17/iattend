import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  Grid,
  Col,
  Row,
  // CheckBox,
  // Radio
  Footer, 
  Form,
  Item,
  Input,
  Textarea,
  Label
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, AsyncStorage, TextInput } from 'react-native';
import styles from '../Question/styles';

import Header from '../../component/Header';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';

import RadioButton from 'radio-button-react-native';
import ApiCaller from '../../common/apiCaller';

class Detail extends Component {


  constructor(props) {

    super(props);
    this.state = {
      questionExist:false,
      text:'',
      accessToken:'',
      audienceid:''
    }
  }


  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  goBack(){
    this.props.navigation.goBack(null);
  }


  componentWillReceiveProps(next){
    if(next.questionData.question.exist == true){
      this.setState({
        questionExist:  true
      }) 
    }
  }

  componentWillMount() {
    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        this.setState({
          accessToken: JSON.parse(data).token,
          audienceid: JSON.parse(data).audienceid
        })
      }
    }).done();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  onPressClose(){
    this.props.navigation.goBack(null);
  }


  onPressSubmit(){


    AsyncStorage.getItem("userData").then((data) => {

      if(this.state.text == ''){
        ToastAndroid.show('Please write some question', ToastAndroid.SHORT);
        return;
      }

      ApiCaller('insertupdatequestionakar', 'post', this.state.accessToken, {
        audienceid : this.state.audienceid,
        eventId : this.props.navigation.state.params.eventId,
        anserwer_value : this.state.text,
    
      }).then(response =>{

        if(response.insert == "true"){
          ToastAndroid.show('Question has been submit', ToastAndroid.SHORT);
          this.props.navigation.goBack(null);

        }else{
          ToastAndroid.show('Question failed to submit', ToastAndroid.SHORT);

        }
      
      })


    }).done();

  }


  footer(){

    if(this.state.questionExist == false){

      return(

        <Grid>

        <Row style={{ marginTop: 0, marginBottom: 0 }}>

        <Button 
          onPress ={() => this.onPressClose()}
          light
          style = {{
            flexDirection: "row",
            flexWrap: "wrap",
            flex: 1,
            justifyContent: "center",
            borderWidth:0,
            height:55,
            borderRadius:0,
          }}
          
        >
          <Text>Close</Text>
        </Button>

        <Button 
          onPress={() => this.onPressSubmit()}
          success
          style = {{
            flexDirection: "row",
            flexWrap: "wrap",
            flex: 1,
            justifyContent: "center",
            borderWidth:0,
            height:55,
            borderRadius:0,
          }}
          
        >
          <Text>Submit</Text>
        </Button>

        </Row>

        </Grid>

        )
    }else{

      return (

      <Grid>

      <Row style={{ marginTop: 0, marginBottom: 0 }}>

      <Button 
        onPress ={() => this.onPressClose()}
        success
        style = {{
          flexDirection: "row",
          flexWrap: "wrap",
          flex: 1,
          justifyContent: "center",
          borderWidth:0,
          height:55,
          borderRadius:0,
        }}
        
      >
        <Text>Close</Text>
      </Button>

      </Row>

      </Grid>

      )
    }

  }


  bodyField(){
    return(
      <View>

            <View style={{backgroundColor:'#fcfcfc', padding:20, paddingLeft:5, paddingBottom:40}}>

            <Form>
            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Materi training :</Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>
            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Hari / tanggal :</Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>
            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Tempat / cabang :</Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>

            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Nama peserta :</Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>

            </Form>

            </View>


            {/* Materi */}

            <View style={{backgroundColor:'#fcfcfc', padding:20, paddingLeft:5, paddingBottom:40, marginTop:10}}>

              <Form>
              <Item stackedLabel>
                <Label style={{fontWeight:'bold'}}>Instruktur I </Label>
                <Input
                  style={{paddingLeft:0}}
                  placeholder="Please input answer here"
                />
              </Item>
              <Item stackedLabel>
                <Label style={{fontWeight:'bold'}}>Materi</Label>
                <Input
                  style={{paddingLeft:0}}
                  placeholder="Please input answer here"
                />
              </Item>

              </Form>

            </View>

            {/* End Materi  */}


            {/* Materi */}

            <View style={{backgroundColor:'#fcfcfc', padding:20, paddingLeft:5, paddingBottom:40, marginTop:10}}>

            <Form>
            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Instruktur II </Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>
            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Materi</Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>

            </Form>

            </View>

            {/* End Materi  */}
            
            {/* Materi */}

            <View style={{backgroundColor:'#fcfcfc', padding:20, paddingLeft:5, paddingBottom:40, marginTop:10}}>

            <Form>
            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Instruktur III </Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>
            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Materi</Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>

            </Form>

            </View>

            {/* End Materi  */}

            {/* Materi */}

            <View style={{backgroundColor:'#fcfcfc', padding:20, paddingLeft:5, paddingBottom:40, marginTop:10, marginBottom:30}}>

            <Form>
            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Instruktur IV </Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>
            <Item stackedLabel>
              <Label style={{fontWeight:'bold'}}>Materi</Label>
              <Input
                style={{paddingLeft:0}}
                placeholder="Please input answer here"
              />
            </Item>

            </Form>

            </View>

            {/* End Materi  */}


            {/* Bagian awal A */}

            <View style={{padding:20}}>

            <Text style={{marginBottom:20, fontWeight:'bold'}}>A. EVALUASI INSTRUKTUR</Text>
            <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5, borderTopColor:'#ececec', borderTopWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Kemampuan presentasi</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>

            </View>

            <View style={{marginBottom:10, borderBottomColor:'#ececec', paddingTop:10, paddingBottom:10, borderBottomWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Pengetahuan dan penguasaan atas materi</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>
            </View>

            <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Suara dan antusiasme</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>
            </View>

            <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Kemampuan menghidupkan suasana kelas</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>
            </View>     

            <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Kemampuan berinteraksi dengan peserta</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>
            </View>  

            <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Penguasaan penggunaan alat bantu training</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>
            </View>    

            <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Sistematika penyampaian materi</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>
            </View>   

            <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Sikap dan penampilan</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>
            </View>   

            <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Wawasan yang dimiliki</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>
            </View>

            <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5}}>
              <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Cara menjawab pertanyaan</Text>

              <View>

                <List>
                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Sangat Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Buruk</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik</Text>
                </Body>
                </ListItem>

                <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                <RadioButton 
                currentValue={this.state.value} value="1"></RadioButton>
                <Body style={{marginLeft:5}}>
                <Text>Baik Sekali</Text>
                </Body>
                </ListItem>

                </List>

              </View>
            </View>

            </View>

            {/* Akhir bagian A */}
            
            <View style={{padding:20}}>
              <Text style={{marginBottom:20, marginTop:20, fontWeight:'bold'}}>B. PROGRAM</Text>
              <View style={{borderBottomColor:'#ececec', paddingTop:10, paddingBottom:20, borderBottomWidth:0.5, borderTopColor:'#ececec', borderTopWidth:0.5}}>
                <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Kemampuan program memenuhi tujuan training</Text>

                <View>

                  <List>
                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Sangat Buruk</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Buruk</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Baik</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Baik Sekali</Text>
                  </Body>
                  </ListItem>

                  </List>

                </View>
              </View>

              <View style={{marginBottom:10, borderBottomColor:'#ececec', paddingTop:10, paddingBottom:10, borderBottomWidth:0.5}}>
                <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Manfaat program bagi peserta</Text>

                <View>

                  <List>
                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Sangat Buruk</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Buruk</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Baik</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Baik Sekali</Text>
                  </Body>
                  </ListItem>

                  </List>

                </View>
              </View>

              <View style={{marginBottom:10, borderBottomColor:'#ececec', paddingTop:10, paddingBottom:10, borderBottomWidth:0.5}}>
                <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Kemenarikan program untuk diikuti</Text>

                <View>

                  <List>
                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Sangat Buruk</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Buruk</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Baik</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Baik Sekali</Text>
                  </Body>
                  </ListItem>

                  </List>

                </View>
              </View>

              <View style={{marginBottom:10, borderBottomColor:'#ececec', paddingTop:10, paddingBottom:10, borderBottomWidth:0.5}}>
                <Text style={{marginBottom:10, fontSize:16, fontWeight:'bold'}}>Kesesuaian isi training & alokasi waktu yang disediakan</Text>

                <View>

                  <List>
                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Sangat Buruk</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Buruk</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Baik</Text>
                  </Body>
                  </ListItem>

                  <ListItem icon noBorder style={{paddingLeft:0, marginLeft:0}}>
                  <RadioButton 
                  currentValue={this.state.value} value="1"></RadioButton>
                  <Body style={{marginLeft:5}}>
                  <Text>Baik Sekali</Text>
                  </Body>
                  </ListItem>

                  </List>

                </View>
              </View>
            </View>

            <View style={{padding:20}}>
            <Text style={{marginBottom:0, fontWeight:'bold'}}>C. Evaluasi Hasil Training</Text>
            <Text note>Tuliskan manfaat training ini untuk anda</Text>

            <Textarea 
            visible-password
            autoCapitalize ={false}
            rowSpan={5} 
            bordered 
            // placeholder="Textarea"
            onChangeText={(value) => this.setState({text:value})}
            autoCorrect={false}
            style={{borderWidth:0.5, borderColor:'#ececec', textAlignVertical: 'top', padding:8}}
             />

            
            </View>

      </View>
    )


  }

  render() {
   
    const speaker= this.props.navigation.state.params;
    var items = this.state.listPollingChoose;
    const {navigation} = this.props;

    return (
        <Container style={styles.container}>

            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Evaluation Form"
            />
  
          <Content>
          {this.state.questionExist == false ?  
          <View style={{backgroundColor:'#ececec', padding:20}}>
             <Text 
             style={{textAlign:'center', alignContent:'center',}}>FORMULIR INI ADALAH BAGIAN DARI PROSES IMPROVEMENT 
             PENYELENGGARAAN training. LUANGKAN WAKTU ANDA TUJUH 
             MENIT UNTUK MENGISINYA.
             </Text>
          </View>
          : null 
          }

          <View style={styles.aboutText}>

          {this.state.questionExist == false ?
      

            this.bodyField()

                :

                <View style={{padding:20, backgroundColor:'#ececec'}}>
                <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold'}}>Mohon maaf. Anda sebelumnya sudah pernah memberikan pertanyaan di event ini.</Text>
                </View>
          }

           </View>
             
          </Content>
  
          <Footer style={{ backgroundColor: "transparent" }}>

            {this.footer()}              

          </Footer>

        </Container>
      );


  }
}


const mapStateToProps = state => ({
  evaluationData: state.evaluationData,
});

export default connect(mapStateToProps)(Detail);