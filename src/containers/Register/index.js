import React, { Component } from 'react'
import {
  View,
  StatusBar,
  Text,
  AsyncStorage,
  ToastAndroid,
  ActivityIndicator,
  Switch,
  ScrollView,
  BackHandler
} from 'react-native';
// import {Item, Icon, Input } from "native-base";
import { Content, List, ListItem, InputGroup, Input, Icon, Picker, Item} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import { connect } from 'react-redux';

import  styles from '../../common/commonStyle';
import Header from '../../component/Header';
import InputField from '../../component/InputField';
import Button from '../../component/Button';
import * as Helper from '../../common/helper';
import { RegisterConst, terms, privacyPolicy, and, termsUse, logInEntry } from '../../common/constant';
import validate from '../../common/validate.js'
import { Grid, Row } from 'react-native-easy-grid';
import Dimensions from 'Dimensions';

var DeviceInfo = require('react-native-device-info');

const {width, height} = Dimensions.get('window');

const constraints = {
  name: {
    presence: {
      allowEmpty:false,
      message: '^Please write your name here'
    }
},
  instansi: {
  presence: {
    allowEmpty:false,
    message: '^Please write your company here'
  }
},
  jabatan: {
  presence: {
    allowEmpty:false,
    message: '^Please write your position here'
  }
},
  email: {
    presence: {
      allowEmpty:false,
      message: '^Please enter an email address'
    },
    email: {
      message: '^Please enter a valid email address'
    }
},
  password: {
    presence: {
      allowEmpty:false,
      message: '^Please enter a password'
    },
    length: {
      minimum: 5,
      message: '^Your password must be at least 5 characters'
    }
  },

  phone: {
    presence: {
      allowEmpty:false,
      message: '^Please write your phone number here'
    }
  },

}

const validator = (field, value) => {  
  let object = {}
  object[field] = value
  let constraint = constraints[field]
  const result = validate(object, { [field]: constraint })
 
  // If there is an error message, return it!
  if (result) {
    // Return only the field error message if there are multiple
    return result[field][0]
  }
  return null
}

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      email: '',
      name:'',
      instansi:'',
      phone:'',   
      jabatan:'',   
      emailListitem:'none',
      toggleName:false,
      toggleInstansi:false,
      toggleJabatan:false,
      togglePhone:false,
      toggle:false,
      togglePassword:false,
      password: '',
      nameError:'',
      instansiError:'',
      jabatanError:'',
      phoneError:'',
      emailError: '',
      passwordError: ''      
    }
  }

  componentWillMount(){
    console.log("componentWillMount",this.props.registerData)
  }
 
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  goBack(){
    this.props.navigation.goBack(null);
  }


  onButtonPress() {

      let { name, email, password, phone, instansi, jabatan, nameError, phoneError, emailError, passwordError } = this.state;
    
      this.onBlurName(name);
      // this.onBlurInstansi(instansi);
      // this.onBlurJabatan(jabatan); 
      this.onBlurPhone(phone); 
      this.onBlurUsername(email);
      this.onBlurPassword(password);
    
      console.log("disini ajah", nameError, phoneError, emailError, passwordError);
    
      if(this.state.nameError === null && this.state.emailError === null && this.state.passwordError === null && this.state.phoneError === null){

        console.log("disini");
  
          const value = {
            name,
            instansi,
            jabatan,
            email,
            phone,
            password,
            deviceid : DeviceInfo.getUniqueID(),
            getuniqueid:DeviceInfo.getUniqueID(),
            gettimezone:DeviceInfo.getTimezone(),
            getdeviceid:DeviceInfo.getDeviceId(), 
            getmodel:DeviceInfo.getModel(),
            istablet:DeviceInfo.isTablet(),
            getsystemname:DeviceInfo.getSystemName(),
            getsystemversion:DeviceInfo.getSystemVersion(),
          }
        
         this.setState({loader: true})
          this.props.dispatch({
            type: 'USER_REGISTRATION',
            payload: value
          });  

        // ToastAndroid.show('Disini', ToastAndroid.SHORT);
  
      }else{
        console.log("disana");
      }

     

  }

 

  onBlurName(value) {
    this.setState({name: value});
    let nameError = validator('name', value);
    if (nameError == null) {
      
      this.setState({
        toggleName: false,
        nameError: nameError
      })
    } else {

      this.setState({
        toggleName: true,
        nameError: nameError
      })
    }
  }

  onBlurInstansi(value) {
    this.setState({instansi: value});
    // let instansiError = validator('instansi', value);
    // if (instansiError == null) {
      
    //   this.setState({
    //     toggleInstansi: false,
    //     instansiError: instansiError
    //   })
    // } else {

    //   this.setState({
    //     toggleInstansi: true,
    //     instansiError: instansiError
    //   })
    // }
  }

  onBlurJabatan(value) {
    
    if(value == 'undefined'){
      this.setState({jabatan: 'kosong'});
    }else{
      this.setState({jabatan: value});
    }

    // let jabatanError = validator('jabatan', value);
    // if (jabatanError == null) {
      
    //   this.setState({
    //     toggleJabatan: false,
    //     jabatanError: jabatanError
    //   })
    // } else {

    //   this.setState({
    //     toggleJabatan: true,
    //     jabatanError: jabatanError
    //   })
    // }
  }  

  onBlurUsername(value) {
    this.setState({email: value.trim()});
    let emailError = validator('email', value);
    if(emailError == null){
     this.setState({
       toggle:false,
       emailError: emailError,
     })
    }else{
     this.setState({
       toggle:true,
       emailError: emailError,
     })
    }
 }

 onBlurPhone(value) {
      // this.setState({phone: value.trim()});

      console.log('onBlurPhone', value);
      if(value == null){
        let phoneError = "Please write your phone number here";

        this.setState({
          phone: value.trim(),
          togglePhone: false,
          phoneError: phoneError
        });

      }else{
        this.setState({
          phone: value.trim(),
          togglePhone: true,
          phoneError: null
        });
      }
  

  }

  onBlurPassword(value) {
 
    //  this.setState({password: value.trim()});
     let password = this.state.password;
     
     let passwordError = validator('password', password);
     console.log("disini ajah", passwordError);
     if(passwordError == null){
      this.setState({
        password:value,
        togglePassword:false,
        passwordError: passwordError,
      })
     }else{
      this.setState({
        password:value,
        togglePassword:true,
        passwordError: passwordError,
      })
     }
  }
  
  componentWillReceiveProps(nextProps) {
    Helper.navigateToPage(this, 'back')
     const { registerData } = nextProps;
     console.log('ss anya', registerData.register.user);
    if(registerData.register.isExist){
      this.setState({
        toggle: true,
        emailError: 'Email alredy exist. Please login',
        loader: false
      })
     }else{
      AsyncStorage.setItem("userData", JSON.stringify(registerData.register.user));

      const value = {
        email:this.state.email
      }
  
      this.props.dispatch({
        type: 'FETCH_EVENT_LIST',
        payload: value
      });
      
      //  Helper.navigateToPage(this, 'Home');
       setTimeout(() => {
        this.setState({
          toggle: false,
          emailError: '',
          loader: false
        }); 
        Helper.navigateToPage(this, 'Home', registerData.register.user);
      }, 2000);



      
      // Helper.navigateToPage(this, 'Home');
     }
  }

  bodyDua() {

    const {nameError, emailError, passwordError, instansiError, jabatanError, phoneError } = this.state

    return (
      <View style={{ height:height-60}}>
        <View style={styles.logInTitle}>
          <Text style={styles.logInTitleText}>{RegisterConst.pageTitle}</Text>
          <Text style={styles.logInCaptionText}>{RegisterConst.subTitle}</Text>
        </View>

<View style={styles.logInViewStyleRegis}>

<Item style={[styles.inputStyle, this.state.toggleName && styles.textInputAlt]}>

<Icon active name='ios-person' style={{marginRight: 12}} />
<Input
  autoCorrect={false} 
  onChangeText={(value) => this.onBlurName(value)}
  value={this.state.name}
  placeholder={"Please write your name here"}
/>
</Item>
{nameError ? 
  <Text style={{color:"#e71636", paddingTop:0, paddingBottom:3}}>{nameError}</Text>
  : null }


<Item style={[styles.inputStyle, this.state.toggleInstansi && styles.textInputAlt]}>
<Icon active name='ios-document' style={{marginRight: 16}} />
<Input
  autoCorrect={false} 
  onChangeText={(value) => this.onBlurInstansi(value)}
  value={this.state.instansi}
  placeholder={"Please write your company/institution here"}
/>
</Item>
{instansiError ? 
  <Text style={{color:"#e71636", paddingTop:0, paddingBottom:3}}>{instansiError}</Text>
  : null }
  
{/* <Item style={[styles.inputStyle, this.state.toggleJabatan && styles.textInputAlt]}>
<Icon active name='ios-shirt' style={{marginRight: 9}} />
<Input
  autoCorrect={false} 
  onChangeText={(value) => this.onBlurJabatan(value)}
  value={this.state.jabatan}
  placeholder={"Please write your position here"}
/>
</Item>
{jabatanError ? 
  <Text style={{color:"#e71636", paddingTop:0, paddingBottom:3}}>{jabatanError}</Text>
  : null } */}

  
<Item style={[styles.inputStyle, this.state.toggle && styles.textInputAlt]}>
<Icon active name='ios-mail' style={{marginRight: 10}} />
<Input
  autoCorrect={false} 
  onChangeText={(value) => this.onBlurUsername(value)}
  value={this.state.email}
  placeholder={"Email Address"}
/>
</Item>
{emailError ? 
  <Text style={{color:"#e71636", paddingTop:0, paddingBottom:3}}>{emailError}</Text>
  : null }
     
<Item style={[styles.inputStyle, this.state.toggle && styles.textInputAlt]}>
<Icon active name='ios-call' style={{marginRight: 10}}/>
<Input
  autoCorrect={false} 
  onChangeText={(value) => this.onBlurPhone(value)}
  value={this.state.phone}
  placeholder={"Phone number"}
  keyboardType= {"phone-pad"}
/>
</Item>
{phoneError ? 
  <Text style={{color:"#e71636", paddingTop:0, paddingBottom:3}}>{phoneError}</Text>
  : null }
  
<Item style={[styles.inputStyle, this.state.togglePassword && styles.textInputAlt]}>
<Icon active name='ios-unlock' style={{marginRight: 10}} />
<Input 
  onChangeText={(value) => this.onBlurPassword(value)}
  value={this.state.password}
  secureTextEntry={true}
  placeholder={"Password"}
/>
</Item>
<Text style={{color:"#e71636", paddingTop:0, paddingBottom:0}}> {passwordError ? passwordError : null }</Text>


    <Spinner visible={this.state.loader} size="large" color="#0000ff" />
  <Button
    buttonTextStyle={styles.buttonTextStyle}
   text="Register"
   style={styles.buttonStyle}
   onPress={() => this.onButtonPress()}
  />
</View>
<View style={styles.termsConditionStyleDua}>
 <Text style={styles.termStyle}>{terms}
   <Text style={styles.privacyStyle}>{' ' + privacyPolicy}</Text>{' ' + and + ' '}
   <Text style={styles.termUseStyle}>{termsUse}</Text>
 </Text>
</View>

      </View>
    )


  }

  render () {
    
    const {nameError, emailError, passwordError, instansiError, jabatanError } = this.state
    const {navigation} = this.props;

    return (
        <View style={styles.container}>


          <StatusBar
             hidden={true}
           />
          <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
             headerLeft={{padding: 10}}
             onLeftIconClick={() => this.props.navigation.goBack(null)}
             leftIcon="md-arrow-back"
             title={RegisterConst.btnText}
           />

                <Grid>
                  <Row style={{backgroundColor:'#F2F2F2'}}>
                      <ScrollView
                              keyboardDismissMode="on-drag"
                              keyboardShouldPersistTaps={true} 
                      >
                      {this.bodyDua()}
                      </ScrollView>
                  </Row>
              </Grid>



        </View>
    )
  }
}
const mapStateToProps = state => ({
  registerData: state.registerData
});



export default connect(mapStateToProps)(Register);