import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

import {Dimensions} from "react-native";

const { height, width } = Dimensions.get('window');

export const w = percent => (width * percent) / 150;
export const h = percent => (height * percent) / 150;
export const totalSize = num => (Math.sqrt((height * height) + (width * width)) * num) / 150;
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: Common.whiteColor,
    backgroundColor:'#F2F2F2'
  },
  body: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#F2F2F2',
  },
  errorColor: {
    paddingTop: 5,
    paddingRight: 5,
    color: 'red',
  },
  // inputStyle: {
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   flexDirection: 'row',
  //   backgroundColor: Common.whiteColor
  // },
  inputStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft:10,
    backgroundColor: Common.whiteColor,
    borderBottomWidth:0,
    marginBottom:1
    // backgroundColor:"#ffffff"
  },
  iconStyle: {
    padding: 10
  },
  textInputStyle: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
  },
  logInTitle: {
    flex: 2,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logInTitleText: {
    color: Common.blackColor,
    fontSize: 20,
    fontWeight: '500'
  },
  logInCaptionText: {
    fontSize: 15,
  },
  logInViewStyle: {
    marginLeft: 20,
    marginRight: 20,
    flex: 2.5,
  },
  aboutText: {
    padding: 20,
    paddingTop: 0,
  },
  textStyle: {
    fontSize: 16,
    marginTop: 20,
  },
  buttonStyle: {
    marginTop: 25,
    alignSelf: 'stretch',
    alignItems: 'center',
    // backgroundColor: '#E0E0E0',
    backgroundColor: '#666666',
    // borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 13,
  },
  buttonTextStyle: {
    // color: Common.darkColor
    color:'#ffffff'
  },
  buttonStyleReset: {
    marginTop: 25,
    alignSelf: 'stretch',
    alignItems: 'center',
    // backgroundColor: '#E0E0E0',
    backgroundColor: '#9ABECB',
    // borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 13,
  },
  buttonTextStyleReset: {
    // color: Common.darkColor
    color:'#405056'
  },
  buttonStyleConfirmation: {
    marginTop: 5,
    alignSelf: 'stretch',
    alignItems: 'center',
    // backgroundColor: '#E0E0E0',
    backgroundColor: '#12AC5C',
    // borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 13,
  },
  buttonTextStyleConfirmation: {
    // color: Common.darkColor
    color:'#ffffff'
  },
  termsConditionStyle: {
    flex: 4.5,
    justifyContent: 'flex-end',
    paddingBottom: 15,
    alignSelf: 'center',
  },
  termStyle: {
    textAlign: 'center',
    fontSize: 14,
    color: Common.darkBlack
  },
  privacyStyle: {
    textDecorationLine: 'underline'
  },
  termUseStyle: {
    textDecorationLine: 'underline'
  },
  alertTeks:{
    marginTop:10,
    color: "#ff0000"
  },
  alertTeksEmail:{
    marginTop:5,
    marginBottom:5,
    color: "#ff0000"
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  searchIcon: {
      padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  teksinputan:{
    backgroundColor: "#fff",
    // flex: 1,
    paddingTop: 12,
    paddingRight: 10,
    paddingBottom: 13.3,
    paddingLeft: 10,
    color: '#424242'
  },
  textInputAlt: {
    borderColor: '#e71636',
    borderTopWidth: 0.8,
    borderRightWidth:0.8,
    borderBottomWidth:0.8,
    borderLeftWidth: 0.8,
    paddingLeft:10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: Common.whiteColor,
    marginBottom:0
  },
  backgroundImage: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // width: null,
    // height: null,
    flex: 1,
    width: null,
    height: null
  },



  containerRegister: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00b5ec',
  },
  inputContainerRegister: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputsRegister:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIconRegister:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainerRegister: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  signupButtonRegister: {
    backgroundColor: "#FF4DFF",
  },
  signUpTextRegister: {
    color: 'white',
  },
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#ececec',
    borderRadius: 4,
    color: '#000000',
    paddingRight: 30, // to ensure the text is never behind the icon
},
inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: '#ececec',
    borderRadius: 8,
    color: '#000000',
    paddingRight: 30, // to ensure the text is never behind the icon
},

inputStyleForm: {
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row',
  // paddingLeft:10,
  // padding:10,
  padding:wp('0.5%'),
  paddingTop: 0,
  paddingLeft: 10,
  backgroundColor: Common.whiteColor,
  borderBottomWidth:1,
  borderRightWidth: 1,
  borderTopWidth: 1,
  borderLeftWidth: 1,
  // marginBottom:w(4),
  // backgroundColor:"#ffffff",
  // borderRadius: 50,
  // borderRadius: w(10),
  borderColor: '#af1e1e'
},

});

module.exports = styles;
