import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid,
  // Col,
  // Row
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, TouchableOpacity, CameraRoll, AsyncStorage  } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../CameraScreen/styles';

import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';
import email from 'react-native-email';
import { RNCamera } from 'react-native-camera';
import { Col, Row, Grid } from "react-native-easy-grid";
import store from 'react-native-simple-store';
import ImageResizer from 'react-native-image-resizer';

class AboutUs extends Component {


  constructor(props) {

    super(props);
    this.state = {
      showDialog: false,
      dialogVisible: false
    }
  }

  componentDidMount(){

    console.log("componentDidMount",this.props);

  }

  takePicture = async function() {
    if(this.camera) {
      const options = { quality: 0.5, base64: true, forceUpOrientation: true, fixOrientation: true };
      const data = await this.camera.takePictureAsync(options)
      console.warn(data.uri);
      console.log("trace anya", data.uri);
      const imgPath = data.uri;
      

      ImageResizer.createResizedImage(data.uri, 200, 150, 'JPEG', 80)
      .then(({ uri }) => {
        CameraRoll.saveToCameraRoll(uri);

        setTimeout(function(){ 
          AsyncStorage.setItem('cameraScreen',uri);
        }, 2000);

        this.props.navigation.navigate('Home');

      })
      .catch(err => {
        console.log(err);
        // return Alert.alert('Unable to resize the photo', 'Check the console for full the error message');
      });
      

      
    }
  }

  render() {
   
    const speaker= this.props.navigation.state.params;
    return (
        <Container style={styles.container}>

            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Camera"
            />
  
  <Grid>
              <Row>
                  <RNCamera 
                  ref={ref => {
                    this.camera = ref;
                  }}
                  style = {styles.preview}
                  type={RNCamera.Constants.Type.back}
                  permissionDialogTitle={'Permission to use camera'}
                  permissionDialogMessage={'We need your permission to use your camera'}
                >
                  <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
                    <TouchableOpacity
                      onPress={this.takePicture.bind(this)}
                      style={styles.capture}
                    >
                      <Text style={{fontSize: 14}}> CAPTURE </Text>
                    </TouchableOpacity>
                  </View>
                </RNCamera>
              </Row>
          </Grid>
  
        </Container>
      );


  }
}


const mapStateToProps = state => ({
  aboutData: state.aboutData,
});

export default connect(mapStateToProps)(AboutUs);