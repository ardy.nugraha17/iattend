import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';

const eventListClass = [];
const eventListTrack = [];

const answer1 = [];
const answer2 = [];
const answer3 = [];
const answer4 = [];
const answer5 = [];
const answer6 = [];
const answer7 = [];
const answer8 = [];
const answer9 = [];
const answer10 = [];

class AddEvaluation extends Component {


  constructor(props) {
    super(props);

    this.onChangeItem = this.onChangeItem.bind(this);
    this.onChangeItemAnswer = this.onChangeItemAnswer.bind(this);

    this.state = {
      chooseOption: 'Please Choose one',
      dialogVisible:false,
      dialogVisibleCheckDrop: false,
      textOption:0,
      countItem:0,
      countItemAnswer:0,
      countItemGenerate:0,
      countItemAnswerGenerate:0,
      question1:"",
      question2:"",
      question3:"",
      question4:"",
      question5:"",
      question6:"",
      question7:"",
      question8:"",
      question9:"",
      question10:"",

      answer0:'',
      answer1:'',
      answer2:'',
      answer3:'',
      answer4:'',
      answer5:'',
      answer6:'',
      answer7:'',
      answer8:'',
      answer9:'',

    };

  }

  componentDidMount(){
    console.log("register event",this.props.navigation.state.params);
  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  onChangeItem(text){
    console.log("trace anya onChangeItem",text);
    this.setState({
      countItem:text
    });
  }

  onChangeItemAnswer(text){
    console.log("trace anya onChangeItemAnswer",text);
    this.setState({
      countItemAnswer:text
    });
  }

  onPressSpeakers(data) {
    
    this.props.navigation.navigate('SpeakersDetail', { ...data });
    
  }

  // generateButton(){

  //   this.setState({
  //     countItemGenerate:this.state.countItem,
  //     countItemAnswerGenerate:this.state.countItemAnswer,
  //   })

    
  // }

  // resetButton(){
  //   this.setState({
  //     countItem:0,
  //     countItemAnswer:0,
  //     countItemGenerate:0,
  //     countItemAnswerGenerate:0,
  //   })
  // }

  cancelButton(){
    console.log("cancelButton");
    this.props.navigation.goBack(null);
  }


  onChangeQuestionList(text,questionNumber){

      switch (questionNumber) {
        case 0:
            this.setState({
              question1:text
            })
            break;
        case 1:
            this.setState({
              question2:text
            })
            break;
        case 2:
            this.setState({
              question3:text
            })
            break;
        case 3:
            this.setState({
              question4:text
            })
            break;
        case 4:
            this.setState({
              question5:text
            })
            break;
        case 5:
            this.setState({
              question6:text
            })
            break;
        case 6:
            this.setState({
              question7:text
            })
            break;
        case 7:
            this.setState({
              question8:text
            })
            break;
        case 8:
            this.setState({
              question9:text
            })
            break;
        case 9:
            this.setState({
              question10:text
            })
    }

  }


  onChangeAnswerList(questionNumber, answerNumber, text){

    console.log('trace onChangeAnswerList',questionNumber, answerNumber, text)

      switch (answerNumber) {
        case 0:
            this.setState({
              answer0:text
            })
            break;
        case 1:
            this.setState({
              answer1:text
            })
            break;
        case 2:
            this.setState({
              answer2:text
            })
            break;
        case 3:
            this.setState({
              answer3:text
            })
            break;
        case 4:
            this.setState({
              answer4:text
            })
            break;
        case 5:
            this.setState({
              answer5:text
            })
            break;
        case 6:
            this.setState({
              answer6:text
            })
            break;
        case 7:
            this.setState({
              answer7:text
            })
            break;
        case 8:
            this.setState({
              answer8:text
            })
            break;
        case 9:
            this.setState({
              answer9:text
            })
      }
    

  }


  submitButton(){
    

    ApiCaller('insertevaluationform', 'post', null, {
      eventId : 22,
      titleEvaluation: 'ini title evaluation',
      question : 'testing audience name',
      status : 'email@gmail.com',
      question1: this.state.question1,
      question2: this.state.question2,
      question3: this.state.question3,
      question4: this.state.question4,
      question5: this.state.question5,
      question6: this.state.question6,
      question7: this.state.question7,
      question8: this.state.question8,
      question9: this.state.question9,
      question10: this.state.question10,

      answer0 : this.state.answer0,
      answer1: this.state.answer1,
      answer2: this.state.answer2,
      answer3 : this.state.answer3,
      answer4 : this.state.answer4,
      answer5 : this.state.answer5,
      answer6 : this.state.answer6,
      answer7 : this.state.answer7,
      answer8 : this.state.answer8,
      answer9 : this.state.answer9,
      
  
    }).then(response =>{

      if(response.message == ""){
        ToastAndroid.show('Register has been success', ToastAndroid.SHORT);
        // this.setState({ dialogVisible: true })
        // console.log("onPressAttendees",response);

      }else{

        // ToastAndroid.show('You are already registered, {\n} please check your email', ToastAndroid.SHORT);
        ToastAndroid.show('You allready registered', ToastAndroid.SHORT);

      }
    
    })



  
    // this.props.navigation.navigate('Login');
  }


  LoopingAnswer(iQuestion) {

    var answer = [];

    for(let i = 0; i < this.state.countItemAnswer; i++){


      switch (i) {
        case 0:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14,backgroundColor:'#ececec'}}
                      value = {this.state.answer0}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer0}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }

                  </Item>
                </View>
              </View>
            )
            break;
        case 1:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14,backgroundColor:'#ececec'}}
                      value = {this.state.answer1}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer1}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }
                  </Item>
                </View>
              </View>
            )
            break;
        case 2:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14,backgroundColor:'#ececec'}}
                      value = {this.state.answer2}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer2}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }
                  </Item>
                </View>
              </View>
            )
            break;
        case 3:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14,backgroundColor:'#ececec'}}
                      value = {this.state.answer3}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer3}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }
                  </Item>
                </View>
              </View>
            )
            break;
        case 4:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14,backgroundColor:'#ececec'}}
                      value = {this.state.answer4}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer4}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }
                  </Item>
                </View>
              </View>
            )
            break;
        case 5:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14,backgroundColor:'#ececec'}}
                      value = {this.state.answer5}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer5}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }
                  </Item>
                </View>
              </View>
            )
            break;
        case 6:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14,backgroundColor:'#ececec'}}
                      value = {this.state.answer6}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer6}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }
                  </Item>
                </View>
              </View>
            )
            break;
        case 7:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14,backgroundColor:'#ececec'}}
                      value = {this.state.answer7}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer7}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }
                  </Item>
                </View>
              </View>
            )
            break;
        case 8:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14,backgroundColor:'#ececec'}}
                      value = {this.state.answer8}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer8}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }
                  </Item>
                </View>
              </View>
            )
            break;
        case 9:
            answer.push(
              <View 
              key = {i}>
                <View>
                  <Item>
                    <Label
                    style={{fontSize:14, fontWeight:'bold'}}
                    >Answer {i + parseInt(1)} : </Label>
                    {iQuestion != 0 ? 
                      <Input
                      disabled
                      style={{fontSize:14, backgroundColor:'#ececec'}}
                      value = {this.state.answer9}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />
                      : 
                      
                      <Input
                      style={{fontSize:14}}
                      value = {this.state.answer9}
                      onChangeText = {(text) => this.onChangeAnswerList(iQuestion,i, text)}
                      />

                    }
                  </Item>
                </View>
              </View>
            )
      }

      

    }


    return (
      <View
      style={{}}
      >
            <View>

            {answer}

            </View>

      </View>
    )
  }

  LoopingBody() {

    var questions = [];
    var answer = [];

    for(let iQuestion = 0; iQuestion < this.state.countItem; iQuestion++){
  
      questions.push(
        <View 
        style={{marginBottom:20, padding:20, borderColor:'#999999', borderWidth:1}}
        key = {iQuestion}>
        
            
            <Label
            style={{marginBottom:3, marginTop:3, fontSize:14, fontWeight:'bold'}}
            >Question number {iQuestion + parseInt(1)} :</Label>
            <Textarea 
            rowSpan={5} 
            bordered 
            placeholder="Please insert text evaluation here ..."
            onChangeText ={(text) => this.onChangeQuestionList(text,iQuestion)} />
            {this.LoopingAnswer(iQuestion)}
          
        </View>
      )
    }


    return (
      <View
      style={{ }}
      >
            <View>

            {questions}

            </View>

      </View>
    )
  }


  render() {

    let { chooseOption } = this.state;

    let data = [
      { value: '1', label: '1' }, 
      { value: '2', label: '2'}, 
      { value: '3', label: '3'},
      { value: '4', label: '4'},
      { value: '5', label: '5'},
      { value: '6', label: '6'},
      { value: '7', label: '7'},
      { value: '8', label: '8'},
      { value: '9', label: '9'},
      { value: '10', label: '10'},
    ];




    // const { attendees } = this.props.attendeesData;
    // const {eventAddress, eventTitle, eventDescription, source,} = this.props.navigation.state.params;


    return (
        <Container style={styles.container}>


          <Header
          headerContainer={styles.headerContainerStyle}
          headerTextStyle={styles.headerTextStyle}
          headerLeft={{padding: 10}}
          onLeftIconClick={() => this.props.navigation.goBack(null)}
          onRightIconClick={() => this.onPressShare()}
          leftIcon="md-arrow-back"
          // rightIcon="share"
          title="Add Evaluation Form"
          />
          <Content contentContainerStyle={{ flex: 1 }}>



          <Grid>
              <Row
                size={20}
              >
              

              <Content
                style={{backgroundColor:'#ececec',padding:20}}
              >

              <View>

              <Label
              style={{marginBottom:5, fontSize:16, color:'#000000'}}
              >Title Evaluation Form</Label>
              <Item regular>
              <Input 
              style={{padding:5, paddingLeft:10, fontSize:16, height:40, backgroundColor:'#ffffff'}}
              placeholder="Please insert title evaluation here ..." />
              </Item>

              <View 
              style={{height:70}}
              >
              <Grid>
                  <Col
                  style={{marginRight:5}}
                  >
                    <Dropdown
                      value={chooseOption}
                      label='Item'
                      labelFontSize="16"
                      data={data}
                      ref={this.sampleRef}
                      onChangeText={this.onChangeItem}
                      baseColor={backgroundColor='#000000'}
                    />
                  </Col>
                  <Col
                  style={{marginLeft:5}}
                  >

                    <Dropdown
                      value={chooseOption}
                      label='Item Answer'
                      labelFontSize="16"
                      data={data}
                      ref={this.sampleRef}
                      onChangeText={this.onChangeItemAnswer}
                      baseColor={backgroundColor='#000000'}
                    />

                  </Col>
              </Grid>
              </View>

              </View>

              </Content>

              </Row>

              <Row
                size={5}
              >
                <Content>
                <View style={{backgroundColor:'#E5E5E5', padding:10, paddingLeft:20,  color:'#ffffff'}}>
                <Text>Please fill the answer</Text>
                </View>
                </Content>
              </Row>

              
              <Row
              size={55}
              // style={{backgroundColor:'#cccccc'}}
              >


              {this.state.countItem != '' ? 

              <Content 
              style={{padding:20}}
              >
                {this.LoopingBody()}
              </Content>
              

              : null}

              </Row>
          </Grid>

          </Content>

          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

            <Row style={{ marginTop: 0, marginBottom: 0 }}>

                    <Button 
                      danger
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.cancelButton()}
                      
                    >
                      <Text>Cancel</Text>
                    </Button>

                    <Button 
                      success
                        style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0
                      }}

                      onPress= {() => this.submitButton()}

                    >
                      <Text>Submit</Text>
                    </Button>


            </Row>

          </Grid>

          </Footer>
        </Container>
    );
  }
} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(AddEvaluation);