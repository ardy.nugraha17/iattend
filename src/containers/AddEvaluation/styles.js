import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({

    viewStyle: {
        backgroundColor: '#ececec',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
      },

    buttonTextStyle: {
    color: Common.darkColor
    },      

});
module.exports = styles;