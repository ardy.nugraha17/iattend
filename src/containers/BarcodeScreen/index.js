import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid,
  // Col,
  // Row
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, TouchableOpacity, CameraRoll, AsyncStorage, Vibration, BackHandler  } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../CameraScreen/styles';

import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';
import email from 'react-native-email';
import { RNCamera } from 'react-native-camera';
import { Col, Row, Grid } from "react-native-easy-grid";
import store from 'react-native-simple-store';
import ImageResizer from 'react-native-image-resizer';
import ApiCaller from '../../common/apiCaller';
import axios from 'axios';

class AboutUs extends Component {


  constructor(props) {

    super(props);
    this.state = {

    }
  }

  componentDidMount(){

    console.log("componentDidMount",this.props);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillMount(){

  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  onViewBarcode(){
    // ToastAndroid.show('onViewBarcode', ToastAndroid.SHORT);
    this.props.navigation.navigate('ScanScreen', { ...this.props });
  }

  onViewAttend(){
    // ToastAndroid.show('onViewAttend', ToastAndroid.SHORT);
    this.props.navigation.navigate('Attendeelist', { ...this.props });
  }

  goBack(){
    this.props.navigation.goBack(null);
  }

  render() {
  
    const {navigation} = this.props;
    
    return (
        <Container style={styles.container}>


            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Attend"
            />
  
            <Content>
              <List>
                <ListItem style={{paddingLeft:8, marginLeft:0}} button onPress={() => this.onViewBarcode()}>
                  <Body>
                    <Text>Attend Event</Text>
                    <Text note>Please scan event barcode when You attend </Text>
                  </Body>
                </ListItem>
                <ListItem style={{paddingLeft:8, marginLeft:0}} button onPress={() => this.onViewAttend()}>
                  <Body>
                    <Text>Attendee list</Text>
                    <Text note>This is for see who has been attend</Text>
                  </Body>
                </ListItem>
              </List>
            </Content>

        </Container>
      );


  }
}


const mapStateToProps = state => ({
  aboutData: state.aboutData,
});

export default connect(mapStateToProps)(AboutUs);