import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid,
  // Col,
  // Row
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, TouchableOpacity, CameraRoll, AsyncStorage, Vibration, BackHandler  } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../CameraScreen/styles';

import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';
import email from 'react-native-email';
import { RNCamera } from 'react-native-camera';
import { Col, Row, Grid } from "react-native-easy-grid";
import store from 'react-native-simple-store';
import ImageResizer from 'react-native-image-resizer';
import ApiCaller from '../../common/apiCaller';
import axios from 'axios';

class AboutUs extends Component {


  constructor(props) {

    super(props);
    this.state = {
      search: false,
      showDrawer: true,
      torchMode: 'on',
      cameraType: 'back',
      dialogVisible:false,
      camereBarcodeValue:false,
      accessToken:'',
      audienceid:'',
      email:'',
      name:''
    }
  }

  componentDidMount(){

    console.log("componentDidMount",this.props);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  componentWillMount(){


    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        console.log("trace anya componentWillMount",data);
        this.setState({
          accessToken: JSON.parse(data).token,
          audienceid: JSON.parse(data).audienceid,
          email: JSON.parse(data).mail,
          name: JSON.parse(data).name,

        })
      }
    }).done();


    this.setState({
      eventId : this.props.navigation.state.params.navigation.state.params.eventId,
      source : this.props.navigation.state.params.source,
      eventTitle : this.props.navigation.state.params.eventTitle,
      eventAddress : this.props.navigation.state.params.eventAddress,
      eventDate : this.props.navigation.state.params.eventDate,
      eventDescrip : this.props.navigation.state.params.eventDescrip,
    })

  }


  takePicture = async function() {
    if(this.camera) {
      const options = { quality: 0.5, base64: true, forceUpOrientation: true, fixOrientation: true };
      const data = await this.camera.takePictureAsync(options)
      console.warn(data.uri);
      console.log("trace anya", data.uri);
      const imgPath = data.uri;
      

      ImageResizer.createResizedImage(data.uri, 200, 150, 'JPEG', 80)
      .then(({ uri }) => {
        CameraRoll.saveToCameraRoll(uri);

        setTimeout(function(){ 
          AsyncStorage.setItem('cameraScreen',uri);
        }, 2000);

        this.props.navigation.navigate('Home');

      })
      .catch(err => {
        console.log(err);
        // return Alert.alert('Unable to resize the photo', 'Check the console for full the error message');
      });
      

      
    }
  }


  barcodeReceived(param) {

    if(!this.state.camereBarcodeValue){

       this.setState({
        camereBarcodeValue:true
       })

       console.log("barcodeReceived mulai",param);

       // console.log("submitButton", this.state.eventId + "data userdata" + data + ":" + this.state.textClass + ":" + this.state.textTrack);
 
       const displit = param.split(",");
       var eventIdPrint = displit[0];
       var namaEvent = displit[1];
       
 
       console.log("barcodeReceived split",displit.length,displit[0],displit[1]);
 
       if(displit.length == 2){


        if(eventIdPrint == this.state.eventId){

          ApiCaller('mobileIattend_insertattendees', 'post', this.state.accessToken, {
            audienceid : this.state.audienceid,
            eventId : this.state.eventId,
            email : this.state.email,
            name : this.state.name
        
          }).then(response =>{
    
            console.log("barcodeReceived response", response);
    
            this.setState({ 
            dialogVisible: true,
            pesan:response.pesan + namaEvent,
            eventListCurrent: response.user.eventid,
            camereBarcodeValue: true,
            textBarcode:namaEvent
            })

            Vibration.vibrate(500);

          })

        }else{

          ToastAndroid.show('You cannot do attendance at this event', ToastAndroid.SHORT);
          Vibration.vibrate(500);
          this.setState({
            camereBarcodeValue: false
          })
          

        }



       }
   

    }else{

    }


  }

  goToNext(){
    // ToastAndroid.show('Loading ....', ToastAndroid.SHORT);
    // setTimeout(this._tabs.goToPage.bind(this._tabs,1))
    this.setState({
      dialogVisible: false,
      camereBarcodeValue: false
    })
  }

  goBack(){
    this.props.navigation.goBack(null)
  }





  render() {
   
    const speaker= this.props.navigation.state.params;
    const {navigation} = this.props;
    
    return (
        <Container style={styles.container}>

          <Dialog 
              visible={this.state.dialogVisible} 
              title="Information"
              // onTouchOutside={() => this.setState({dialogVisible: false})}
              dialogStyle={{width:'80%',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              
              }}
              titleStyle={{
                textAlign: 'left', alignSelf: 'stretch'
              }}
              overlayStyle={{backgroundColor: 'transparent'}}
              >
              <View>
                  <Text style={{fontSize: 16}}>{this.state.pesan}</Text>
                  <Button 
                  full
                  style = {{
                    marginTop:20,
                    // height:30
                  }}
                  onPress={() => this.goToNext()}
                  
                  >
                  <Text style={{color: '#ffffff'}}>Close</Text>
                  </Button>
              </View>
          </Dialog>


            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Barcode Scanner"
            />
  
          <Grid>
              <Row>
                  <RNCamera 
                  ref={ref => {
                    this.camera = ref;
                  }}
                  style = {styles.preview}
                  type={RNCamera.Constants.Type.back}
                  onBarCodeRead={(e) => this.barcodeReceived(e.data)}
                  permissionDialogTitle={'Permission to use camera'}
                  permissionDialogMessage={'We need your permission to use your camera'}
                >
                  {/* <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
                    <TouchableOpacity
                      onPress={this.takePicture.bind(this)}
                      style={styles.capture}
                    >
                      <Text style={{fontSize: 14}}> CAPTURE </Text>
                    </TouchableOpacity>
                  </View> */}
                  
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                              <Image style={styles.borderImage} source={require("../../images/cameraScreenDua.png")} />
                      </View>

                </RNCamera>
              </Row>
          </Grid>
  
        </Container>
      );


  }
}


const mapStateToProps = state => ({
  aboutData: state.aboutData,
});

export default connect(mapStateToProps)(AboutUs);