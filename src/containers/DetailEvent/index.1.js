import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dimensions, View, Image  } from 'react-native';
import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  Thumbnail,
  Footer
} from 'native-base';
import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import EventDetailTab from '../EventDetail';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';

import { Col, Row, Grid } from "react-native-easy-grid";

class DetailEvent extends Component {

  componentDidMount(){
    console.log("trace anya onPress", this.props);
  }


  render() {


    const {
      eventAddress, eventTitle, eventDescription, source, eventDescrip
    } = this.props.navigation.state.params;


    return (
      <Container style={styles.container}>
      <Header
      headerContainer={styles.headerContainerStyle}
      headerTextStyle={styles.headerTextStyle}
      headerLeft={{padding: 10}}
      onLeftIconClick={() => this.props.navigation.goBack(null)}
      // onRightIconClick={() => this.onPressShare()}
      leftIcon="md-arrow-back"
      // rightIcon="share"
      title="Detail"
      />


      <Content contentContainerStyle={{ flex: 1 }}>
        
      <Grid>
      <Row size={30} style={{backgroundColor:'#ececec', paddingTop:30, paddingBottom:30}}>
      
      <Content>

          <View style={styles.imageSpeaker}>

          <Thumbnail 
          style={{width: 200, height: 200, borderRadius: 200/2}}
          source={{ uri: 'http://192.168.90.95:1337/get_file?fd='+this.props.navigation.state.params.eventPhoto }} />

          </View>


          <View>
          <Text style={{textAlign:'center', alignContent:'center', alignItems:'center'}}>{this.props.navigation.state.params.eventNama}</Text>
          </View>

      </Content>

      
      </Row>

      
      <Row size={50} style={{backgroundColor:'#ffffff'}}>


            <Grid>
                <Col>
                
                    <Content>

                    <View>
                    <Text style={{padding:10, backgroundColor:'#E5E5E5'}}>Nama Istansi</Text>
                    <Text style={{padding:10}}>{this.props.navigation.state.params.eventIstansi}</Text>

                    <Text style={{padding:10, backgroundColor:'#E5E5E5'}}>Jabatan</Text>
                    <Text style={{padding:10}}>{this.props.navigation.state.params.eventJabatan}</Text>

                    <Text style={{padding:10, backgroundColor:'#E5E5E5'}}>Email</Text>
                    <Text style={{padding:10}}>{this.props.navigation.state.params.eventEmail}</Text>

                    <Text style={{padding:10, backgroundColor:'#E5E5E5'}}>No. Handphone</Text>
                    <Text style={{padding:10}}>{this.props.navigation.state.params.eventHp}</Text>
                    </View>


                    </Content>

                </Col>
                <Col>
                
                    <View>
                    <Text style={{padding:10, backgroundColor:'#D8D8D8'}}>Signature</Text>
                    </View>
                    <View style={styles.imageTtd}>

                    <Thumbnail square large
                    style={{width: 200, height: 200}}
                    source={{ uri: 'http://192.168.90.95:1337/get_file?fd='+this.props.navigation.state.params.eventTtd }} />

                    </View>

                </Col>
            </Grid>



      </Row>
      </Grid>
      </Content>

      <Footer style={{ backgroundColor: "transparent" }}>

      <Grid>

        <Row style={{ marginTop: 0, marginBottom: 0 }}>

                <Button 
                  success
                  style = {{
                    flexDirection: "row",
                    flexWrap: "wrap",
                    flex: 1,
                    justifyContent: "center",
                    borderWidth:0,
                    height:55,
                    borderRadius:0,
                    marginRight:1
                  }}

                  onPress= {() => this.props.navigation.goBack(null)}
                  
                >
                  <Text>Back</Text>
                </Button>


        </Row>

      </Grid>

      </Footer>

    </Container>
    );
  }
}


const mapStateToProps = state => ({
  // aboutData: state.aboutData,
});

export default connect(mapStateToProps)(DetailEvent);
