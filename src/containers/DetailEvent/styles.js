import * as Common from '../../common/common';
export default {
    text: {
      alignSelf: "center",
      marginBottom: 7
    },
    mb: {
      marginBottom: 15
    },
    container: {
        flex: 1,
        width: null,
        height: null,
        backgroundColor: "#FFF"
      },
      iconContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-around",
        paddingLeft: 15
      },
      iconText: {
        fontSize: 14,
        // justifyContent:"center",
        textAlign: 'center',
        width:70,
        fontWeight: 'bold',
        marginTop: 10,
      },
      icon: {
        width: 70,
        height: 70,
        justifyContent: "center"
      },
      col: {
        alignItems: "center",
        paddingHorizontal: 3
      },
      row: {
        paddingBottom: 20
      },
      viewIcon:{
        width: 70, height: 110, padding:"5%",marginBottom:"5%", 
        // backgroundColor:"#eeeeee",
        justifyContent: 'center', alignItems: 'center' 
      },
      headerStyle:{
        // backgroundColor: Common.lightGreen,
        backgroundColor:"#666666",
        justifyContent: "center", 
        height:120,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 40,
        paddingRight: 40,
      },
      forTitle:{
        alignSelf: "center",
        color: Common.whiteColor,
        fontSize: 19,
        marginBottom: 10,
        textAlign:'center'
      },
      forSubTitle:{
        alignSelf: "center",
        fontSize:16,
        color: Common.whiteColor,
      },
      MainContainer :{
        // Setting up View inside component in Vertically center.
        justifyContent: 'center',
        // Setting up View inside component align horizontally center.
        alignItems: 'center',
        flex:1
         
        },


        imageSpeaker:{
          flex: 2,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          textAlign:'center',
          marginTop:20,
          marginBottom:10
        },

        imageTtd:{
            flex: 3,
            // flexDirection: 'column',
            // justifyContent: 'center',
            alignItems: 'center',
            // textAlign:'center',
            marginTop:30,
            // marginBottom:10
          },

        latarRowSatu:{
          backgroundColor:'#ffffff',
          textAlign:'center',
          alignContent: 'center',
          alignItems: 'center',
        },

        latarRowDua:{
          backgroundColor:'#ececec'

        },
        TitleList:{
          backgroundColor:'#666666',
          fontSize:14,
          padding:10
        },
        latarRowTiga:{
          width:'100%'
        }

        
  };
