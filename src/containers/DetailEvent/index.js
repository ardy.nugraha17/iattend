import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
} from 'native-base';
import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import EventDetailTab from '../EventDetail';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import { eventList, eventListJune , mainText} from '../../common/constant';

class DetailEvent extends Component {

  componentDidMount(){
    // console.log("trace anya onPress", this.prop);
  }

  closeDrawer = () => {
    this.drawer._root.close()
  }

  openDrawer = () => {
    this.drawer._root.open()
  }

  render() {


    const {
      eventAddress, eventTitle, eventDescription, source, eventDescrip
    } = this.props.navigation.state.params;
    return (
      <DrawerView
        onClose={() => this.closeDrawer()}
        referVar={(ref) => { this.drawer = ref; }}
        content={<SideMenu {...this.props} onClose={() => this.closeDrawer()}/>}
        
      >
        <Container style={styles.container}>
          <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.openDrawer()}
            leftIcon="ios-menu"
            title={mainText}
          />

          <Content>
            <EventDetailTab infoDetail={this.props.navigation.state.params} nav={this.props.navigation} {...this.props}/>
          </Content>

        </Container>
      </DrawerView>
    );
  }
}
 
export default DetailEvent;