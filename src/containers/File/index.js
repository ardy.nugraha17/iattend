import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  Grid,
  Col,
  Row,
  // CheckBox,
  // Radio
  Footer
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, AsyncStorage, ActivityIndicator, FlatList, RefreshControl, BackHandler } from 'react-native';
import styles from '../Polling/styles';

import Header from '../../component/Header';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';

import RadioButton from 'radio-button-react-native';
import ApiCaller from '../../common/apiCaller';

class Detail extends Component {


  constructor(props) {

    super(props);
    this.state = {
      listFile:'',
      accessToken:'',
      audienceid:'',
      eventId:'',
      agendaId:'',
      refreshing: true


    }
  }


  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    // console.log("trace detail", this.props.navigation.state.params.ListChooseData);
    // console.log("trace detail", this.props.navigation.state.params.eventId);
    console.log("dataLengthPolling atas", this.props.navigation.state.params.navigation.state.params);

    this.setState({
      eventId:this.props.navigation.state.params.navigation.state.params.navs.state.params.eventId,
      agendaId:this.props.navigation.state.params.navigation.state.params.eventId
    })

  }

  goBack(){
    this.props.navigation.goBack(null);
  }


  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  componentWillReceiveProps(next){
    console.log("dataLengthPolling",next.fileData.listfileData);

    if(next.fileData.listfileData.rows > 0){
      this.setState({
        listFile:next.fileData.listfileData.datanya,
        refreshing: false
      })
    }

  }
  

  componentWillMount() {
    AsyncStorage.getItem('userData').then((data) => {
      if (JSON.parse(data)) {
        // console.log("trace anya",data);
        this.setState({
          accessToken: JSON.parse(data).token,
          audienceid: JSON.parse(data).audienceid,
        })
      }
    }).done();


  }


  handleOnPress(value){
    console.log("trace detail back", value);
    this.setState({value:value})
  }

  onPressClose(){
    this.props.navigation.goBack(null);
  }


  ListViewItemSeparator = () => {
    return (
      //returning the listview item saparator view
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#ececec',
        }}
      />
    );
  };

  onRefresh() {
    //Clear old data of the list
    this.setState({ dataSource: [] });
    //Call the Service to get the latest data
    this.onGetData();
  }

  onGetData(){

    ApiCaller('fileaudienceakar', 'post', this.state.accessToken, {
      audienceid : this.state.audienceid,
      eventId : this.state.eventId,
      agendaId: this.state.agendaId
  
    }).then(response =>{

      console.log("onGetData response", response);

      this.setState({
        listFile:response.datanya
      })


    })

  }

  onPressPDF(files,materiName){

    this.props.navigation.navigate('DetailFile', { ...this.props, files, materiName });

  }


  render() {
   
    const speaker= this.props.navigation.state.params;
    var items = this.state.listPollingChoose;
    const {navigation} = this.props;

    return (
        <Container style={styles.container}>


            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Files"
            />
  
          <Content refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh.bind(this)} />}>


          {this.state.listFile == '' ?
          
            null

          :

          <View style={{paddingTop:0}}>

            <FlatList
            data={this.state.listFile}
            ItemSeparatorComponent={this.ListViewItemSeparator}
            enableEmptySections={true}
            renderItem={({item}) => (
              <List>
                <ListItem 
                noBorder
                thumbnail
                // style={{marginLeft:10}}
                button onPress={() => this.onPressPDF(item.files,item.file_title)}
                >
                  <Left style={{margin:0}}>
                    {/* <Thumbnail square source={{ uri: 'http://192.168.90.95:1337/get_file?fd='+props.eventPhoto }} /> */}
                    <Thumbnail square source={{uri:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9oVPMdyDBCseMR95sueX8UjIPkjQX3waoJAo5VCD6esmM-F_i"}} />
                  </Left>
                  <Body>
                    <Text>{item.file_title}</Text>
                    <Text note>File type : PDF</Text>
                  </Body>
                  {/* <Right>
                    <Button transparent onPress={() => this.onPressPDF(item.files,item.file_title)}>
                      <Text>View</Text>
                    </Button>
                  </Right> */}
                </ListItem>
              </List>
            )}
            refreshControl={
              <RefreshControl
                //refresh control used for the Pull to Refresh
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
            />

          </View>

          
          }


             
          </Content>
  

        </Container>
      );


  }
}


const mapStateToProps = state => ({
  fileData: state.fileData,
});

export default connect(mapStateToProps)(Detail);