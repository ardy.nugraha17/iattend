import { StyleSheet, Dimensions } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerStyle: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  image: {
    resizeMode: 'stretch',
  },
  headerTextStyle: {
    color: Common.whiteColor,
    fontWeight: 'bold',
  },
  headerContainerStyle: {
    flex: 1,
    margin: 5,
  },
  text: {
    color: '#fff',
    fontSize: 16,
  },
  viewWrapper: {
    flex: 9,
  },
  tabStyle: {
    backgroundColor: Common.lightGreen,
  },
  viewSubheaderTab:{
    padding: 15,
    paddingLeft: 25,
    // backgroundColor: Common.lightGray,
    backgroundColor: "#f2f2f2"
  },
  viewSubheaderTabText:{
    fontSize:16,
    fontWeight:'bold'
  },

  ActivIndicatorContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  ActivIndicatorHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  aboutText: {
    padding: 0,
    paddingTop: 0,
    // paddingRight: 20,
  },



  // Ini untuk detail

  containerMateri: {
    // flex: 1,
    // justifyContent: 'flex-start',
    // alignItems: 'center',
    // marginTop: 25,
    // height:30,
    backgroundColor:'#F2F2F2',
    paddingTop: 10,
    paddingLeft: 20,
    paddingRight: 10,
    paddingBottom: 10,
  },

  textMateri:{
    fontSize:14,
    fontWeight: 'bold',
  },
  containerPdf: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'center',
  },
  pdf: {
      flex:1,
      width:Dimensions.get('window').width,
  }


});

module.exports = styles;
