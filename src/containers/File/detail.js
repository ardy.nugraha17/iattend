import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  // Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage,
  TouchableHighlight, Platform,
  ActivityIndicator,
  NativeAppEventEmitter,
  DeviceEventEmitter,
  NativeModules,
  NativeEventEmitter,
  Dimensions
} from 'react-native';

import styles from './styles';
import Header from '../../component/Header';

import Pdf from 'react-native-pdf';


class ViewMateri extends Component {


  constructor(props) {
    super(props);

    this.state = {

      eventTitle: '',
      materiName: '',
      pathFile: ''

    };


  }


  componentWillMount(){
    console.log("componentWillMount Materi", this.props.navigation.state.params.navigation.state.params.navigation.state.params.eventTitle);

    this.setState({
      eventTitle: this.props.navigation.state.params.navigation.state.params.navigation.state.params.eventTitle,
      materiName: this.props.navigation.state.params.materiName,
      pathFile: this.props.navigation.state.params.files
    })


  }

  componentDidMount(){
  }

  componentWillUnmount (){

  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  render() {
    // const source = require('./document.pdf');

    // const source = {uri:'http://192.168.3.117:1337/get_file_moderator?fd='+ this.state.pathFile,cache:true};
    // const source = {uri:'http://www.africau.edu/images/default/sample.pdf',cache:true};

    const source = {uri:this.state.pathFile,cache:true};

    return (
        <Container style={styles.container}>


        <Header
        headerContainer={styles.headerContainerStyle}
        headerTextStyle={styles.headerTextStyle}
        headerLeft={{padding: 10}}
        onLeftIconClick={() => this.props.navigation.goBack(null)}
        onRightIconClick={() => this.onPressShare()}
        leftIcon="md-arrow-back"
        // rightIcon="share"
        title="Detail"
        />
        <Content contentContainerStyle={{ flex: 1 }}>
        
        <View style={styles.containerMateri}>

          <Text style={styles.baseText}>
            <Text style={styles.textMateri}>Materi name : </Text>
            <Text numberOfLines={5} style={{color:'#666666'}}>{this.state.materiName}</Text>
          </Text>

          <Text style={styles.baseText}>
            <Text style={styles.textMateri}>Agenda : </Text>
            <Text numberOfLines={5} style={{color:'#666666'}}>{this.state.eventTitle}</Text>
          </Text>

         
        </View>

        <View style={styles.containerPdf}>
            <Pdf
                source={source}
                onLoadComplete={(numberOfPages,filePath)=>{
                    console.log(`number of pages: ${numberOfPages}`);
                }}
                onPageChanged={(page,numberOfPages)=>{
                    console.log(`current page: ${page}`);
                }}
                onError={(error)=>{
                    console.log(error);
                }}
                style={styles.pdf}/>
        </View>


        {/* <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View> */}


        </Content>

        <Footer style={{ backgroundColor: "transparent" }}>
            <Button 
              light
              style = {{
                flexDirection: "row",
                flexWrap: "wrap",
                flex: 1,
                justifyContent: "center",
                borderWidth:0,
                height:55,
                borderRadius:0,
                marginRight:1
              }}

              onPress= {() => this.props.navigation.goBack(null)}
              
            >
              <Text>Close</Text>
            </Button>
        </Footer>
        </Container>
    )
  }


} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(ViewMateri);