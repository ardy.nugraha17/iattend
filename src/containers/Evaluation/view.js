import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  // Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid, 
  // Row, 
  // Col,
  Footer,
  Label,
  Item,
  Input,
  Textarea
} from 'native-base';

import { ToastAndroid, View, AsyncStorage,
  TouchableHighlight
} from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from './styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import { Dropdown } from 'react-native-material-dropdown';

import { Col, Row, Grid } from "react-native-easy-grid";
import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import ApiCaller from '../../common/apiCaller';
import Icon from 'react-native-vector-icons/MaterialIcons';


const eventListEvaluation = [
  {id:1, name:'Tom Cruise',image:'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Tom_Cruis.jpg/250px-Tom_Cruis.jpg'},
  {id:2, name:'Johnny Depp',image:'https://m.media-amazon.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1_UY317_CR4,0,214,317_AL_.jpg'},
  {id:3, name:'Kate Winslet', image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKRaFPucye6r1C1gJpCy7QX0XMJlQZzFW7JqZKajM686SZBl8h'},
  {id:4, name:'Natalie Portman', image:'https://www.biography.com/.image/t_share/MTE4MDAzNDEwNzU3MDYwMTEw/natalie-portman-9542326-1-402.jpg'},
  {id:5, name:'Cecilia Cheung', image:'https://i.mydramalist.com/kAqRwc.jpg'},
  {id:6, name:'Andy Lau', image:'https://s.kaskus.id/r480x480/images/fjb/2016/03/29/koleksi_film_andy_lau_725487_1459244874.jpg'},
  {id:7, name:'Stephen Chow', image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdMtWGVTQtAH5SoB_XKBRuvVCAR5OmurfuljnGIUupYAWNUwp1'}
];


const eventListEvaluationAudience = [
  {id:1, name:'Isu-isu yang didiskusikan menjawab permasalahan', answer: 'Sangat tidak setuju'},
  {id:2, name:'Saya Berpartisipasi aktif dalam pelatihan', answer: 'Maybe'},
  {id:3, name:'Fasilitas dan suasana tempat pelatihan mendukung saya belajar', answer:'Setuju'},
  {id:4, name:'Ruangan sangat nyaman untuk event ini', answer:'Setuju'},
  {id:5, name:'Tempatnya nyaman dan baik', answer:'Setuju'},
  {id:6, name:'Saya berpatisipasi',answer:'Setuju'},
  {id:7, name:'Pembicara event ini sangat menguasai materi',answer:'Setuju'},
];

class AddEvaluation extends Component {


  constructor(props) {
    super(props);

    this.state = {
      listEvaluation: eventListEvaluation,
      listEvaluationAudience: eventListEvaluationAudience,
      dialogVisible:false
    };

    this.onButtonViewPress = this.onButtonViewPress.bind(this);

  }

  componentDidMount(){
    console.log("register event",this.props.navigation.state.params);
  }

  componentWillReceiveProps(next){
    // console.log("register event",next.registerEventData);

  }

  
  onButtonViewPress(){
    console.log("onButtonViewPress");
    this.setState({
      dialogVisible:true
    })    

  }


  onButtonCloseDialog(){
    console.log("onButtonViewPress");
    this.setState({
      dialogVisible:false
    })    

  }


  LoopingBody() {


    return (
      <View>
        <Dialog 
        visible={this.state.dialogVisible} 
        title="Audience evaluation form"
        onTouchOutside={() => this.setState({dialogVisible: false})} 
        >
        <View 
        style={{height:300}}
        >

                  {
                    this.state.listEvaluationAudience != '' ? 

                    <List 
                    style={{paddingLeft:0, marginLeft:0,paddingRight:10}}
                    dataArray={this.state.listEvaluationAudience}
                    renderRow={(item) =>
                      //  console.log("trace PollingDetail bawah dua",item.id)
                      <ListItem
                      style={{paddingLeft:0, marginLeft:0, paddingRight:10}}>
                      <Body>
                        <Text>{item.name}</Text>
                        <Text note numberOfLines={1}>Answer : {item.answer}</Text>
                      </Body>
                    </ListItem>
                    }>
                </List>
                      : null
                  }

        </View>

        <View style={{height:40, marginTop:10}}>
        <Button 
        onPress={() => this.onButtonCloseDialog()}
        block light>
            <Text
            >Close</Text>
        </Button>
        </View>

        </Dialog>
      </View>
    )
  }




  render() {


    // const { attendees } = this.props.attendeesData;
    // const {eventAddress, eventTitle, eventDescription, source,} = this.props.navigation.state.params;

    return (
      
        <Container style={styles.container}>


          {this.LoopingBody()}

          <Header
          headerContainer={styles.headerContainerStyle}
          headerTextStyle={styles.headerTextStyle}
          headerLeft={{padding: 10}}
          onLeftIconClick={() => this.props.navigation.goBack(null)}
          // onRightIconClick={() => this.onPressShare()}
          leftIcon="md-arrow-back"
          // rightIcon="share"
          title="Evaluation Form"
          />
          <Content contentContainerStyle={{ flex: 1 }}>


          <Grid>
              <Row size={19}>
              <View style={styles.headertitleviewevaluation}>
                <Text>Event Name Here</Text>
                <Text>24 Desember 2018</Text>
                <Text>Jakarta</Text>
              </View>
              </Row>

              {/* baris kedua */}
              <Row size={6}>
              <View style={styles.titleListParticipant}>
                <Text>List participant</Text>
              </View>
              </Row>
              
              {/* baris ketiga */}
              <Row size={70}>
              
              <Content>

              <View>
                  {
                    this.state.listEvaluation != '' ? 

                    <List dataArray={this.state.listEvaluation} thumbnail
                    renderRow={(item) =>
                      //  console.log("trace PollingDetail bawah dua",item.id)
                      <ListItem thumbnail>
                      <Left>
                        <Thumbnail source={{ uri: item.image }} />
                      </Left>
                      <Body>
                        <Text>{item.name}</Text>
                        <Text note numberOfLines={1}>loremipsum@gmail.com</Text>
                      </Body>
                      <Right>
                        <Button transparent
                        onPress={() => this.onButtonViewPress()}
                        >
                            <Icon size={24} active name="visibility" />
                        </Button>
                      </Right>
                    </ListItem>
                    }>
                </List>
                      : null
                  }
                  </View>        

              </Content>
                    

              </Row>
          </Grid>

          

          </Content>

          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

            <Row style={{ marginTop: 0, marginBottom: 0 }}>

                    <Button 
                      light
                      style = {{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        flex: 1,
                        justifyContent: "center",
                        borderWidth:0,
                        height:55,
                        borderRadius:0,
                        marginRight:1
                      }}

                      onPress= {() => this.props.navigation.goBack(null)}
                      
                    >
                      <Text>Back</Text>
                    </Button>

            </Row>

          </Grid>

          </Footer>
        </Container>
    );
  }
} 

const mapStateToProps = state => ({
  // registerEventData: state.registerEventData,
});

export default connect(mapStateToProps)(AddEvaluation);