import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({

    viewStyle: {
        backgroundColor: '#ececec',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
      },

    buttonTextStyle: {
    color: Common.darkColor
    },
    
    headertitleviewevaluation:{
        backgroundColor:'#cccccc',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    titleListParticipant:{
        backgroundColor:'#ececec',
        width:'100%',
        paddingLeft:20,
        paddingTop:10,
        paddingBottom:10
    },
    listViewEvaluation : {
        width:'100%'
    }

});
module.exports = styles;