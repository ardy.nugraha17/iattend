import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerStyle: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  image: {
    resizeMode: 'stretch',
  },
  headerTextStyle: {
    color: Common.whiteColor,
    fontWeight: 'bold',
  },
  headerContainerStyle: {
    flex: 1,
    margin: 5,
  },
  text: {
    color: '#fff',
    // fontSize: 13,
    // fontSize:16,
    fontSize: 13
  },
  viewWrapper: {
    flex: 9,
  },
  tabStyle: {
    backgroundColor: Common.lightGreen,
  },
  MainContainerMoveOrder:{
    justifyContent: 'center',
    flex:1,
    margin: 7
  },
  latarInputan:{
    backgroundColor : "#ececec",
    padding: 10
},
TextInputStyleClass:{

  fontSize: 16,
  textAlign: 'left',
  paddingLeft:20,
  height: 50,
  borderWidth: 1,
  borderColor: '#cccccc',
  borderRadius: 7 ,
  backgroundColor : "#ffffff",
  marginTop: 10,
 
 },
 baseText: {
  // fontFamily: 'Cochin',
},
titleText: {
  // fontSize: 20,
  fontWeight: 'bold',
},
SectionHeaderStyle:{
  backgroundColor : '#e9ecf1',
  fontSize : 20,
  padding: 5,
  paddingLeft:20,
  color: '#666666'
},
containerOverlay: {
  flex: 1,
  justifyContent: 'center'
},
horizontalOverlay: {
  flexDirection: 'row',
  justifyContent: 'space-around',
  padding: 10
}
});

module.exports = styles;
