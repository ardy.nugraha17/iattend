import React, { Component } from 'react'
import {
  // View,
  Image,
  StatusBar,
  Text,
  AsyncStorage, 
  ScrollView,
  TextInput,
  Alert,
  TouchableHighlight,
  ToastAndroid,
  Picker,
  SectionList,
  TouchableOpacity,
  ActivityIndicator 
} from 'react-native'
import { Tabs, Tab, ScrollableTab, 
  List,
  ListItem,
  Button,
  View,
  Fab,
  TabHeading,
  Label,
  Left,
  Thumbnail,
  Body, Right,
} from "native-base"

import { connect } from 'react-redux';
// import Icon from "react-native-vector-icons/FontAwesome";
// import Icon from 'react-native-vector-icons/SimpleLineIcons';
import styles from './styles';
import DrawerView from '../../component/DrawerView';
import { eventList, eventListJune , mainText} from '../../common/constant';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import { Col, Row, Grid } from "react-native-easy-grid";
import { FloatingAction } from 'react-native-floating-action';

import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs';
import { Dropdown } from 'react-native-material-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Rating, AirbnbRating } from 'react-native-ratings';

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';


const dataQuestion = [{ value: 'pending', label :'Pending'}, { value: 'answered', label : 'Answered'},];

const eventListEvaluation = [
  {id:1, name:'evaluation satu'},
  {id:2, name:'evaluation dua'},
  {id:3, name:'evaluation tiga'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation lima'},
  {id:4, name:'evaluation enam'},
  {id:4, name:'evaluation tujuh'},
  {id:4, name:'evaluation delapan'},
  {id:4, name:'evaluation sembilan'},
  {id:4, name:'evaluation sepuluh'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
  {id:4, name:'evaluation empat'},
];

const eventListMateri = [{id:1, name:'polling satu'},{id:2, name:'polling dua'},{id:3, name:'polling tiga'},{id:4, name:'polling empat'}];
const eventListFeedBack = [{id:1, name:'feedback satu'},{id:2, name:'feedback dua'},{id:3, name:'feedback tiga'},{id:4, name:'feedback empat'}];
const eventListPolling = [{id:1, name:'polling satu'},{id:2, name:'polling dua'},{id:3, name:'polling tiga'},{id:4, name:'polling empat'}];
const eventListQuestion = [
          {id:1, name:'Paolo Maldini', image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNJ5-dN3FVJNjaGB1J3Jypdp7UF3yhC-J5k2nwvB8IFA8OxbRZ'},
          {id:2, name:'David Beckham', image:'https://st.depositphotos.com/1814084/1666/i/950/depositphotos_16667903-stock-photo-david-beckham.jpg'},
          {id:3, name:'Jim Strugers', image:'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/JimSturgess08TIFF.jpg/220px-JimSturgess08TIFF.jpg'},
          {id:4, name:'Cecilia Cheung', image:'http://www.spcnet.tv/thumbnail.php?img=http://s3.amazonaws.com/spcnet-images/images/actors/AsLpcP-3164.jpg&width=250&height=250'}];

const actions = [{
  text: 'Add Polling',
  textBackground:'#48a701',
  color:'#48a701',
  textColor:'#ffffff',
  icon: require('../../images/polling.png'),
  name: 'bt_polling',
  position: 1
}, {
  text: 'Add Note',
  textBackground:'#f98f2b',
  textColor:'#ffffff',
  color:'#f98f2b',
  icon: require('../../images/voteWhite.png'),
  name: 'bt_note',
  position: 2
}
];


class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: false,
      showDrawer: true,
      stateTitle:'EVALUATION FORM',
      active: 'true',
      listEvaluation: eventListEvaluation,
      listFeedBack: eventListFeedBack,
      listPolling: eventListPolling,
      listQuestion: eventListQuestion,
      listMateri: eventListMateri,
      dialogVisibleQuestion: false,
      dataSource:'',
      ArraylistSpeaker:'',
      sectionTitle: [],
      sectionTitleArray : [],
      ActivityIndicator: false
    }
    this.ArraylistEvaluation = eventListEvaluation;
    this.ArraylistFeedback = eventListFeedBack;
    this.ArraylistPolling = eventListPolling;
    // this.ArraylistQuestion = eventListQuestion;
    // this.ArraylistMateri = eventListMateri;
    
  }

  componentDidMount(){
    console.log("trace anya componentDidMount");
    console.log("eventListEvaluation trace",this.props)
    // this.setState({
    //   listEvaluation:eventListEvaluation
    // })
  }

  componentWillMount() {
    AsyncStorage.getItem('userData').then((data) => {
      // console.log("data@@componentWillMount",data)
      if(data) {
        this.setState({showDrawer: true})
      }
    }).done();
  }


  componentWillReceiveProps(nextProps) {

    console.log("trace anya componentWillReceiveProps");

    console.log("componentWillReceiveProps Home", nextProps);
    
    this.setState({
      dataSource : nextProps.eventData.event.eventListSpeaker,
      ArraylistSpeaker: nextProps.eventData.event.eventListSpeaker,

      dataSourceNote : nextProps.eventData.event.eventListNote,
      ArraylistNote: nextProps.eventData.event.eventListNote,

      dataSourceMateri : nextProps.eventData.event.eventListMateri,
      ArraylistMateri: nextProps.eventData.event.eventListMateri,

      dataSourceQuestion : nextProps.eventData.event.eventListQuestion,
      ArraylistQuestion: nextProps.eventData.event.eventListQuestion,

      dataSourcePolling : nextProps.eventData.event.eventListPolling,
      ArraylistPolling: nextProps.eventData.event.eventListPolling,

      ActivityIndicator:true
    })

    this.ArraylistSpeaker = nextProps.eventData.event.eventListSpeaker
    this.ArraylistNote = nextProps.eventData.event.eventListNote
    this.ArraylistMateri = nextProps.eventData.event.eventListMateri
    this.ArraylistQuestion = nextProps.eventData.event.eventListQuestion
    this.ArraylistPolling = nextProps.eventData.event.eventListPolling


    // // console.log("menu speaker kiri",nextProps.speakerData);
    // if(nextProps.speakerData) {
    //   // this.setState({dataSource : nextProps.speakerData.speakers})
    //   this.setState({dataSource : nextProps.speakerData.speakers})
      
    // }else{
    //   this.setState({dataSource : "empty"})
    // }

  }

  componentDidUpdate(){
    console.log("trace anya componentDidUpdate");

  }

  componentWillUnmount () {
    console.log('trace anya Component WILL UNMOUNT!')
  }

  increment () {
    // this.setState({number: this.state.number + 1, update: true})
    console.log('trace anya Component WILL UNMOUNT!')
  }

  dontUpdate () {
    // this.setState({number: this.state.number + 1, update: false})
    console.log('trace anya Component WILL UNMOUNT!')
  }


  closeDrawer = () => {
    this.drawer._root.close()
  }

  openDrawer = () => {
    this.drawer._root.open()
  }

  
  setTitleNameId(pram){
    if(pram == 0){
      this.setState({stateTitle: "EVALUATION FORM"});
    }else if(pram == 1){
      this.setState({stateTitle: "FEEDBACK FORM"});
    }else if(pram == 2){
      this.setState({stateTitle: "POLLING"});
    }else{
      this.setState({stateTitle: "QUESTION"});
    }

  }


  sortBy(key, reverse) {

    // Move smaller items towards the front
    // or back of the array depending on if
    // we want to sort the array in reverse
    // order or not.
    var moveSmaller = reverse ? 1 : -1;
  
    // Move larger items towards the front
    // or back of the array depending on if
    // we want to sort the array in reverse
    // order or not.
    var moveLarger = reverse ? -1 : 1;
  
    /**
     * @param  {*} a
     * @param  {*} b
     * @return {Number}
     */
    return function(a, b) {
      if (a[key] < b[key]) {
        return moveSmaller;
      }
      if (a[key] > b[key]) {
        return moveLarger;
      }
      return 0;
    };
  
  }  



  getMappedData(data){
    let arr={};
    monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];  
    data.forEach(function(each) {
      let d=each.eventDescription;
      let key=monthNames[parseInt(d.slice(5,7))-1]+'-'+d.slice(0,4);
      if(!arr[key]) arr[key]=[];
      arr[key].push(each);
      }); 
    return arr;
  }

  searchEvaluation(text){

    // console.log("searchEvaluation",this.state.dataSource);
    const newData = this.ArraylistSpeaker.filter(function(item){
      console.log("searchEvaluation dua",item.name);
      const itemData = item.name.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    })
    this.setState({
      dataSource: newData,
    })
  }


  searchFeedBack(text){
    const newData = this.ArraylistNote.filter(function(item){
      const itemData = item.text.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    })
    this.setState({
        dataSourceNote: newData,
    })
  }

  searchPolling(text){
    const newData = this.ArraylistPolling.filter(function(item){
      const itemData = item.pollingQuestion.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    })
    this.setState({
        dataSourcePolling: newData,
    })
  }

  searchQuestion(text){
    const newData = this.ArraylistQuestion.filter(function(item){
      const itemData = item.answer_value.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    })
    this.setState({
      dataSourceQuestion: newData,
    })
  }  


  searchMateri(text){

    // console.log("searchEvaluation",this.state.dataSource);
    const newData = this.ArraylistMateri.filter(function(item){
      console.log("searchEvaluation dua",item.fileName);
      const itemData = item.fileName.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    })
    this.setState({
      dataSourceMateri: newData,
    })
  } 

  onPressActionFabs(param){
    
    console.log("pilihan",param)

    switch (param) {
      case "bt_note":
          this.props.navigation.navigate('AddNote');
          break;
      case "bt_feedbackform":
          this.props.navigation.navigate('AddFeedback');
          break;
      case "bt_polling":
          this.props.navigation.navigate('AddPolling');
          break;
      case "bt_question":
          this.props.navigation.navigate('AddQuestion');
    }


  }


  onPressViewEvalParitici(id){

    // ToastAndroid.show('A pikachu appeared nearby !' + id, ToastAndroid.SHORT);
    this.props.navigation.navigate('ViewEvaluation');

  }

  onPressViewPolling(item){

    console.log("onPressViewPolling",item);

    const value = {
      'agendaId': item.agendaId,
      'pollingNumber': item.pollingNumber,
    }


    this.props.dispatch({
      type: 'FETCH_QUESTION_LIST',
      payload: { ...value }
    });

    this.props.navigation.navigate('ViewPolling', item);

  }


  onPressViewMateri(item){
    // ToastAndroid.show('A pikachu appeared nearby !' + id, ToastAndroid.SHORT);
    this.props.navigation.navigate('ViewMateri',{item});
  }

  onPressViewQuestion(item){

    this.props.navigation.navigate('ViewQuestion',{item});

  }


  onPressSpeaker(id){



  }

  ratingCompleted(rating) {
    console.log("Rating is: " + rating)
  }

  onPressSpeakers(data) {
    this.props.navigation.navigate('SpeakersDetail', { ...data });
  }

  onPressViewNote(data) {
    this.props.navigation.navigate('ViewNote', { ...data });
  }

  

  render () {

    let { showDrawer } = this.state;
    var itemsEvaluation = eventListEvaluation;
    var itemsFeedback = eventListFeedBack;
    var itemsPolling = eventListPolling;
    var itemsQuestion = eventListQuestion;


    // Ini untuk data tab speaker //


    var A = [];
    var B = [];
    var C = [];
    var D = [];
    var E = [];
    var F = [];
    var G = [];
    var H = [];
    var I = [];
    var J = [];
    var K = [];
    var L = [];
    var M = [];
    var N = [];
    var O = [];
    var P = [];
    var Q = [];
    var R = [];
    var S = [];
    var T = [];
    var U = [];
    var V = [];
    var W = [];
    var X = [];
    var Y = [];
    var Z = [];
    var sectionTitle = [];
    var sectionTitleArray = [];


    if(this.state.dataSource){
      // console.log("menu speaker kiri render",this.state.dataSource);

      // const {
      //   eventAddress, eventTitle, eventDescription, source,
      // } = this.props.navigation.state.params;
  
      // console.log("menu speaker kiri",speakersAll);
      this.state.dataSource.map((userData) => {
        // console.log("trace anya",userData.name , userData.name.charAt(0).toUpperCase());
  
        if(userData.name.charAt(0).toUpperCase() == 'A'){
          // console.log("trace anya",sectionTitle.indexOf("A"));
  
          if(sectionTitle.indexOf('A') == -1){
            sectionTitle.push('A');
            sectionTitleArray.push({"title": "A", "data": A});
          }
  
          // console.log("trace anya",userData.name , userData.name.charAt(0).toUpperCase());
          A.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'B'){
          if(sectionTitle.indexOf('B') == -1){
            sectionTitle.push('B');
            sectionTitleArray.push({"title": "B", "data": B})
          }
          // console.log("trace anya",userData.name , userData.name.charAt(0).toUpperCase());
          // B.push(userData.name)}
          B.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'C'){
          if(sectionTitle.indexOf('C') == -1){
            sectionTitle.push('C');
            sectionTitleArray.push({"title": "C", "data": C})
          }
  
          // C.push(userData.name)}
          C.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'D'){
          if(sectionTitle.indexOf('D') == -1){
            sectionTitle.push('D');
            sectionTitleArray.push({"title": "D", "data": D})
          }
  
          // D.push(userData.name)}
          D.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'E'){
          if(sectionTitle.indexOf('E') == -1){
            sectionTitle.push('E');
            sectionTitleArray.push({"title": "E", "data": E})
          }
  
          // E.push(userData.name)}
          E.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'F'){
          if(sectionTitle.indexOf('F') == -1){
            sectionTitle.push('F');
            sectionTitleArray.push({"title": "F", "data": F})

          }
          // F.push(userData.name)}
          F.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'G'){
          if(sectionTitle.indexOf('G') == -1){
            sectionTitle.push('G');
            sectionTitleArray.push({"title": "G", "data": G})

          }
          
          // G.push(userData.name)}
          G.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'H'){
          if(sectionTitle.indexOf('H') == -1){
            sectionTitle.push('H');
            sectionTitleArray.push({"title": "H", "data": H})

          }
          // H.push(userData.name)}
          H.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'I'){
          if(sectionTitle.indexOf('I') == -1){
            sectionTitle.push('I');
            sectionTitleArray.push({"title": "I", "data": I})

          }
          // I.push(userData.name)}
          I.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'J'){
          if(sectionTitle.indexOf('J') == -1){
            sectionTitle.push('J');
            sectionTitleArray.push({"title": "J", "data": J})

          }
          // J.push(userData.name)}
          J.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'K'){
          if(sectionTitle.indexOf('K') == -1){
            sectionTitle.push('K');
            sectionTitleArray.push({"title": "K", "data": K})
          }
          // K.push(userData.name)}
          K.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'L'){
          if(sectionTitle.indexOf('L') == -1){
            sectionTitle.push('L');
            sectionTitleArray.push({"title": "L", "data": L})

          }
          // L.push(userData.name)}
          L.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'M'){
          if(sectionTitle.indexOf('M') == -1){
            sectionTitle.push('M');
            sectionTitleArray.push({"title": "M", "data": M})
          }
          // M.push(userData.name)}
          M.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'N'){
          if(sectionTitle.indexOf('N') == -1){
            sectionTitle.push('N');
            sectionTitleArray.push({"title": "N", "data": N})
          }
          // N.push(userData.name)}
          N.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'O'){
          if(sectionTitle.indexOf('O') == -1){
            sectionTitle.push('O');
            sectionTitleArray.push({"title": "O", "data": O})
          }
          // O.push(userData.name)}
          O.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'P'){
          if(sectionTitle.indexOf('P') == -1){
            sectionTitle.push('P');
            sectionTitleArray.push({"title": "P", "data": P})

          }
          // P.push(userData.name)}
          P.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'Q'){
          if(sectionTitle.indexOf('Q') == -1){
            sectionTitle.push('Q');
            sectionTitleArray.push({"title": "Q", "data": Q})

          }
          // Q.push(userData.name)}
          Q.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'R'){
          if(sectionTitle.indexOf('R') == -1){
            sectionTitle.push('R');
            sectionTitleArray.push({"title": "R", "data": R})
          }
          // R.push(userData.name)}
          R.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'S'){
          if(sectionTitle.indexOf('S') == -1){
            sectionTitle.push('S');
            sectionTitleArray.push({"title": "S", "data": S})

          }
          // S.push(userData.name)}
          S.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'T'){
          if(sectionTitle.indexOf('T') == -1){
            sectionTitle.push('T');
            sectionTitleArray.push({"title": "T", "data": T})

          }
          // T.push(userData.name)}
          T.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'U'){
          if(sectionTitle.indexOf('U') == -1){
            sectionTitle.push('U');
            sectionTitleArray.push({"title": "U", "data": U})
          }
          // U.push(userData.name)}
          U.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'V'){
          if(sectionTitle.indexOf('V') == -1){
            sectionTitle.push('V');
            sectionTitleArray.push({"title": "V", "data": V})
          }
          // V.push(userData.name)}
          V.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'W'){
          if(sectionTitle.indexOf('W') == -1){
            sectionTitle.push('W');
            sectionTitleArray.push({"title": "W", "data": W})
          }
          // W.push(userData.name)}
          W.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'X'){
          if(sectionTitle.indexOf('X') == -1){
            sectionTitle.push('X');
            sectionTitleArray.push({"title": "X", "data": X})
          }
          // X.push(userData.name)}
          X.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'Y'){
          if(sectionTitle.indexOf('Y') == -1){
            sectionTitle.push('Y');
            sectionTitleArray.push({"title": "Y", "data": Y})
          }
          // Y.push(userData.name)}
          Y.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        if(userData.name.charAt(0).toUpperCase() == 'Z'){
          if(sectionTitle.indexOf('Z') == -1){
            sectionTitle.push('Z');
            sectionTitleArray.push({"title": "Z", "data": Z})
          }
          // Z.push(userData.name)}
          Z.push({
            "id": userData.id, 
            "name": userData.name, 
            "image": userData.image, 
            "skills": userData.skills, 
            "education": userData.education, 
            "experience": userData.experience,
            "description": userData.description,
          })}
  
        
      });
  
  
      // console.log("sectionTitleArray",sectionTitleArray);


    }else{

    }



    const SECTIONS = sectionTitleArray.sort(this.sortBy('title'));






    return (
      <DrawerView
        onClose={() => this.closeDrawer()}
        referVar={(ref) => { this.drawer = ref; }}
        content={<SideMenu {...this.props} onClose={() => this.closeDrawer()} />}
      >
        <View style={styles.container}>
          <StatusBar
             hidden={true}
           />


          <Header

            hasTabs
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={showDrawer ? () => this.openDrawer(): null}
            // onRightIconClick={() => this.setState({ search : !this.state.search})}
            leftIcon={showDrawer ? "ios-menu" : null}
            // rightIcon="ios-search"
            // search={this.state.search}
            // title={mainText}
            // title={this.state.stateTitle}
            title = "Moderator Event Management"
            {...this.props}
          />

          { this.state.ActivityIndicator == false ? 
        
            <View style={[styles.containerOverlay, styles.horizontalOverlay]}>
            <ActivityIndicator size="large" color="#666666" />
            </View>
          : 

          <View style={styles.viewWrapper}>
            <Tabs 
                // initialPage={0} 
                renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#DD0212" }} />} 
                // onChangeTab={({i, ref, from}) => 
                // // console.log("trace anya",i);
                // this.setTitleNameId(i)
                // }
            >
              <Tab 
              
                heading="SPEAKERS"
                // heading={ <TabHeading style={{backgroundColor:'#DD0212'}}><Icon name="book-open" style={{marginRight:10,color:'#ffffff',fontSize:16}} /><Text style={{fontSize:16,color:'#ffffff'}}>EVALUATION FORM</Text></TabHeading>}
                tabStyle={styles.tabStyle}
                activeTextStyle={styles.text}
                activeTabStyle={styles.tabStyle}
                textStyle={styles.text}> 

                <Grid>

                  <Row size={10}>
                  <View style={{width:'100%', padding:10, backgroundColor:'#F2F2F2'}}>

                  <TextInput
                  style={styles.TextInputStyleClass}
                  onChangeText={(text) => this.searchEvaluation(text)}
                  value={this.state.text_moveorder}
                  underlineColorAndroid='transparent'
                  placeholder="Search speaker here"
                    />

                  </View>
                  </Row>

                  <Row size={90} style={{backgroundColor:'#ffffff'}}>


          <SectionList

              sections={SECTIONS}
              renderSectionHeader={ ({section}) => <Text style={styles.SectionHeaderStyle}> { section.title } </Text> }
              renderItem={ ({item}) => 

              <View style={styles.viewStyle}>

                  <List thumbnail>
                  <TouchableOpacity>
                    <ListItem thumbnail noBorder
                    style={{
                      margin:0, 
                      paddingBottom:10,
                      marginLeft:0, 
                      paddingLeft:20,
                      borderBottomColor:'#ececec',
                      borderBottomWidth:1
                    }}
                    onPress = {() => this.onPressSpeakers(item) }
                    >
                      <Left>
                        <Thumbnail source={{ uri: 'http://eventapp.dexagroup.com/apiget_file?fd='+item.image }} />
                      </Left>
                      <Body>
                        <Text>{item.name}</Text>
                        <Text note>{item.skills}</Text>
                      </Body>
                      <Right>
                        {/* <Text note>3:43 pm</Text> */}
                        <Button
                        transparent
                        onPress = {() => this.onPressSpeakers(item) }
                        >
                        <IconFontAwesome size={24} active name="chevron-right" />
                        </Button>
                        
                      </Right>
                    </ListItem>
                    </TouchableOpacity>
                  </List>

              </View>
              }

              keyExtractor={ (item, index) => index }

              />
                  </Row>
                </Grid>

                  
              </Tab>

              <Tab 
              
                heading="NOTE"
                // heading={ <TabHeading style={{backgroundColor:'#DD0212'}}><Icon name="envelope-letter" style={{marginRight:10,color:'#ffffff',fontSize:16}} /><Text style={{fontSize:16,color:'#ffffff'}}>FEEDBACK FORM</Text></TabHeading>}
                tabStyle={styles.tabStyle}
                activeTextStyle={styles.text}
                activeTabStyle={styles.tabStyle}
                textStyle={styles.text}>

                <Grid>

                <Row size={10}>
                <View style={{width:'100%', padding:10, backgroundColor:'#F2F2F2'}}>

                <TextInput
                style={styles.TextInputStyleClass}
                onChangeText={(text) => this.searchFeedBack(text)}
                value={this.state.text_moveorder}
                underlineColorAndroid='transparent'
                placeholder="Search note title here ..."
                  />

                </View>
                </Row>

                <Row size={90} style={{backgroundColor:'#ffffff'}}>

                <View style={styles.MainContainerMoveOrder}>
                {
                  this.state.dataSourceNote != '' ? 

                    <List 
                    style={{marginLeft:0, paddingLeft:0,}}
                    dataArray={this.state.dataSourceNote}
                        renderRow={(item) =>
                          //  console.log("trace PollingDetail bawah dua",item.id)
                          // <ListItem
                          //   onPress={() => this.onPressFeedback(
                          //     item.id
                          //   )}
                          // >
                          // {/* <Text>{item.id}</Text> */}
                          // <Text style={{flexWrap: 'wrap', paddingRight:10}}>{item.name}</Text>
                          // </ListItem>

                          <ListItem thumbnail noBorder
                          style={{ paddingLeft:0, marginLeft:0}}
                          >
                      <Body style={{paddingRight:0, marginRight:20}}>
                      <View style={{flex: 1, flexDirection: 'row'}}>

                        {/* content */}
                        <TouchableOpacity 
                        style={{width:'100%'}}
                        onPress={() => this.onPressViewNote(item)}
                        >
                        <View 
                        style={{width: '100%', backgroundColor: '#e9ecf1',borderRadius:10, width:'100%'  }} >
                        
                        {/* batas awal */}

                            <View style={{borderRadius:10}}>


                                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'stretch', }}>
                                  <View style={{ flexDirection:'row', flex:0.8, backgroundColor:'#e9ecf1', padding:20, borderRadius:10, 
                                  borderBottomRightRadius:0, borderTopRightRadius:0, width:'98%'}}>

                                      <View style={{flex: 1,flexDirection: 'column',justifyContent: 'center',alignItems: 'stretch'}}>

                                                                                <View style={{ flexDirection:'row', flex:0.8, marginTop:5,}}>

                                        <View style={{flex: 1, flexDirection: 'row'}}>
                                          <View style={{width: 70, }} ><Text style={{fontSize:14, fontWeight:'bold'}}>Title note :</Text></View>
                                          <View style={{width: '100%', }} ><Text>{item.heading}</Text></View>
                                        </View>

                                        </View>

                                        <View style={{ minHeight: 25 }}>
                                          <Text style={{flexWrap: 'wrap', color:'#989898'}}>
                                                {item.text}
                                          </Text>
                                        </View>

                                      </View>

                                  </View>
                                </View>

                              </View>

                        {/* batas akhir */}
                        </View>
                        </TouchableOpacity>
                      </View>
                      </Body>
                    </ListItem>

                        }>
                    </List>
                    : null
                }
                </View>


                </Row>
                </Grid>

              </Tab>

              <Tab 
              
                heading="POLLING"
                // heading={ <TabHeading style={{backgroundColor:'#DD0212'}}><Icon name="hourglass" style={{marginRight:10,color:'#ffffff',fontSize:16}} /><Text style={{fontSize:16,color:'#ffffff'}}>POLLING</Text></TabHeading>}
                tabStyle={styles.tabStyle}
                activeTextStyle={styles.text}
                activeTabStyle={styles.tabStyle}
                textStyle={styles.text}> 

                <Grid>

                <Row size={10}>
                <View style={{width:'100%', padding:10, backgroundColor:'#F2F2F2'}}>

                <TextInput
                style={styles.TextInputStyleClass}
                onChangeText={(text) => this.searchPolling(text)}
                value={this.state.text_moveorder}
                underlineColorAndroid='transparent'
                placeholder="Search polling name here"
                  />

                </View>
                </Row>

                <Row size={90} style={{backgroundColor:'#ffffff'}}>

                <View style={styles.MainContainerMoveOrder}>
                {
                  this.state.dataSourcePolling != '' ? 

                    <List 
                    style={{margin:0, marginBottom:10, padding:0, marginLeft:0, paddingLeft:0}}
                    dataArray={this.state.dataSourcePolling} thumbnail
                        renderRow={(item) =>
                          //  console.log("trace PollingDetail bawah dua",item.id)
                          <TouchableOpacity>
                          <ListItem noBorder
                          style={{
                            // backgroundColor:'#e9ecf1', 
                            margin:0, 
                            paddingBottom:10,
                            marginLeft:0, 
                            paddingLeft:10,
                            borderBottomColor:'#ececec',
                            borderBottomWidth:1
                          }}
                          thumbnail
                          onPress={() => this.onPressViewPolling(item)}
                          >
                          <Left
                          style={{margin:0, padding:0}}
                          >
                            {/* <Thumbnail
                            source={{ uri: item.image }} /> */}
                            <IconFontAwesome size={36} active name="hourglass-half" />

                          </Left>
                          <Body>
                            <Text style={{fontWeight:'bold', fontSize:14}}>{item.pollingQuestion}</Text>
                            <Text style={{flex: 1, flexWrap: 'wrap', paddingRight:20}}>Total participant : {item.jumlah_audience} audience</Text> 
                            
                          </Body>
                          <Right>
                            <Button 
                              transparent
                              onPress={() => this.onPressViewPolling(item)}
                              >
                              <IconFontAwesome size={24} active name="chevron-right" />
                            </Button>
                          </Right>
                        </ListItem>
                        </TouchableOpacity>
                        }>
                    </List>
                    : null
                }
                </View>


                </Row>
                </Grid>

              </Tab>

              <Tab 
              
                heading="QUESTION"
                // heading={ <TabHeading style={{backgroundColor:'#DD0212'}}><Icon name="notebook" style={{marginRight:10,color:'#ffffff',fontSize:16}} /><Text style={{fontSize:16,color:'#ffffff'}}>QUESTION</Text></TabHeading>}
                tabStyle={styles.tabStyle}
                activeTextStyle={styles.text}
                activeTabStyle={styles.tabStyle}
                textStyle={styles.text}>

                <Grid>

                <Row size={10}>
                <View style={{width:'100%', padding:10, backgroundColor:'#F2F2F2'}}>

                <TextInput
                style={styles.TextInputStyleClass}
                onChangeText={(text) => this.searchQuestion(text)}
                value={this.state.text_moveorder}
                underlineColorAndroid='transparent'
                placeholder="Search question here"
                  />

                </View>
                </Row>

                <Row size={90} style={{backgroundColor:'#ffffff'}}>

                <View style={styles.MainContainerMoveOrder}>
                {
                  this.state.dataSourceQuestion != '' ? 

                    <List 
                    style={{margin:0, marginBottom:10, padding:0, marginLeft:0, paddingLeft:0}}
                    dataArray={this.state.dataSourceQuestion} thumbnail
                        renderRow={(item) =>
                          //  console.log("trace PollingDetail bawah dua",item.id)
                          <TouchableOpacity>
                          <ListItem noBorder
                          style={{
                            // backgroundColor:'#e9ecf1', 
                            margin:0, 
                            paddingBottom:10,
                            marginLeft:0, 
                            paddingLeft:10,
                            borderBottomColor:'#ececec',
                            borderBottomWidth:1
                          }}
                          thumbnail
                          onPress={() => this.onPressViewQuestion(item)}
                          >
                          <Left
                          style={{margin:0, padding:0}}
                          >
                          <Thumbnail source={{ uri: 'http://eventapp.dexagroup.com/apiget_file?fd='+item.image }} />
                          </Left>
                          <Body>
                            <Text style={{fontWeight:'bold', fontSize:14}}>{item.name}</Text>
                            <Text style={{flex: 1, flexWrap: 'wrap', paddingRight:20}}>
                            
                            {item.answer_value}
                            </Text> 
                            
                          </Body>
                          <Right>
                            <Button 
                              transparent
                              onPress={() => this.onPressViewQuestion(item)}
                              >
                              <IconFontAwesome size={24} active name="chevron-right" />
                            </Button>
                          </Right>
                        </ListItem>
                        </TouchableOpacity>
                        }>
                    </List>
                    : null
                }
                </View>


                </Row>
                </Grid>

              </Tab>


              <Tab 
              
              heading="MATERI"
              // heading={ <TabHeading style={{backgroundColor:'#DD0212'}}><Icon name="notebook" style={{marginRight:10,color:'#ffffff',fontSize:16}} /><Text style={{fontSize:16,color:'#ffffff'}}>QUESTION</Text></TabHeading>}
              tabStyle={styles.tabStyle}
              activeTextStyle={styles.text}
              activeTabStyle={styles.tabStyle}
              textStyle={styles.text}>

              <Grid>

              <Row size={10}>
              <View style={{width:'100%', padding:10, backgroundColor:'#F2F2F2'}}>

              <TextInput
              style={styles.TextInputStyleClass}
              onChangeText={(text) => this.searchMateri(text)}
              value={this.state.text_moveorder}
              underlineColorAndroid='transparent'
              placeholder="Search materi name here"
                />

              </View>
              </Row>

              <Row size={90} style={{backgroundColor:'#ffffff'}}>

                  <View style={styles.MainContainerMoveOrder}>
                  {
                    this.state.dataSourceMateri != '' ? 

                      <List dataArray={this.state.dataSourceMateri} thumbnail
                          renderRow={(item) =>
                            //  console.log("trace PollingDetail bawah dua",item.id)
                            <TouchableOpacity>
                            <ListItem thumbnail noBorder
                            style={{
                              paddingLeft:0,
                              borderBottomColor:'#ececec',
                              borderBottomWidth:1
                            }}
                            onPress={() => this.onPressViewMateri(item)}
                            >
                            <Left>
                              {/* <Thumbnail square source={require('../../images/materiDownload.png')} /> */}
                              <IconFontAwesome size={36} active name="file-pdf-o" />
                            </Left>
                            <Body>
                              <Text style={styles.baseText}>
                                <Text style={styles.titleText} onPress={this.onPressTitle}>Materi Name : </Text> 
                                <Text numberOfLines={5}>{item.fileName}</Text>
                              </Text>
                              <Text style={styles.baseText}>
                                <Text >Agenda : </Text> 
                                <Text numberOfLines={5}>{item.name}</Text>
                              </Text>
                            </Body>
                            <Right>
                              <Button 
                                transparent
                                onPress={() => this.onPressViewMateri(item)}
                                >
                                <IconFontAwesome size={24} active name="chevron-right" />
                              </Button>
                            </Right>
                          </ListItem>
                          </TouchableOpacity>
                          }>
                      </List>
                      : null
                  }
                  </View>


              </Row>
              </Grid>

            </Tab>

            </Tabs>

            {/* <Fab
              active={this.state.active}
              direction="up"
              containerStyle={{ }}
              style={{ backgroundColor: '#5067FF' }}
              position="bottomRight"
              onPress={() => this.setState({ active: !this.state.active })}>
              <Icon name="plus" />
              <Button style={{ backgroundColor: '#fb871a'}}>
                <Icon type="Entypo" active name="book-open" style={{ fontSize: 15, color:'#fff' }} />
              </Button>
              <Button style={{ backgroundColor: '#49a506'}}>
                <Icon type="Entypo" active name="envelope-letter" style={{ fontSize: 15, color:'#ffffff' }} />
              </Button>
              <Button disabled style={{ backgroundColor: '#df2727' }}>
                <Icon type="Entypo" active name="hourglass" style={{ fontSize: 15, color:'#ffffff' }} />
              </Button>
              <Button disabled style={{ backgroundColor: '#9b9ca0' }}>
                <Icon type="Entypo" active name="notebook" style={{ fontSize: 15, color:'#ffffff' }} />
              </Button>
            </Fab> */}

            <FloatingAction
            actions={actions}
            position="right"
            overlayColor="transparent"
            color='#666666'
            // onPressItem={
            //   (position) => {
            //     Alert.alert('Icon pressed', `the icon name ${position} was pressed`);
            //   }
            // }

            onPressItem={(name) => {this.onPressActionFabs(name)}}
          />

          </View>


          }

        </View>
      </DrawerView>
    )
  }
}


  const mapStateToProps = state => ({
    eventData: state.eventData,
  });

  export default connect(mapStateToProps)(Dashboard);
