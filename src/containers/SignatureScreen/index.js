import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  // Grid,
  // Col,
  // Row
  Footer
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share,   TouchableHighlight,
  TouchableOpacity, CameraRoll, AsyncStorage} from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../SignatureScreen/styles';

import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';
import email from 'react-native-email';
import { RNCamera } from 'react-native-camera';
import { Col, Row, Grid } from "react-native-easy-grid";
import SignatureCapture from 'react-native-signature-capture';
import ImageResizer from 'react-native-image-resizer';

arrayLo=[];
class AboutUs extends Component {


  constructor(props) {

    super(props);
    this.state = {
      showDialog: false,
      dialogVisible: false
    }
  }

  // ini untuk signature 

  saveSign() {
    this.refs["sign"].saveImage();
    setTimeout(function(){ 
      console.log("sight",arrayLo[0]);
      AsyncStorage.setItem('signatureScreen',arrayLo[0]);
    }, 2000);
    
    this.props.navigation.navigate('Home');
  }

  
  resetSign() {
      this.refs["sign"].resetImage();
  }

  _onSaveEvent(result) {
      //result.encoded - for the base64 encoded png
      //result.pathName - for the file path name
      // arrayLo.push(result.pathName);
      console.log("disini", result);
      // CameraRoll.saveToCameraRoll(result.pathName);


      ImageResizer.createResizedImage(result.pathName, 200, 150, 'JPEG', 80)
      .then(({ uri }) => {
        arrayLo.push(uri);
        console.log(uri);
        CameraRoll.saveToCameraRoll(uri);

      })
      .catch(err => {
        console.log(err);
        // return Alert.alert('Unable to resize the photo', 'Check the console for full the error message');
      });

      
  }
  _onDragEvent() {
      // This callback will be called when the user enters signature
      console.log("dragged");
  }


  render() {
   
    const speaker= this.props.navigation.state.params;
    return (
        <Container style={styles.container}>

            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="Signature"
            />
  
  <Grid>
              <Row>
                  <SignatureCapture
                      style={[{flex:1},styles.signature]}
                      ref="sign"
                      onSaveEvent={this._onSaveEvent}
                      onDragEvent={this._onDragEvent}
                      saveImageFileInExtStorage={true}
                      showNativeButtons={false}
                      showTitleLabel={false}
                      viewMode={"portrait"}/>

              </Row>
          </Grid>
  
          <Footer style={{ backgroundColor: "transparent" }}>

          <Grid>

          <Row style={{ marginTop: 0, marginBottom: 0 }}>


          <Button success
              style = {{
              flexDirection: "row",
              flexWrap: "wrap",
              flex: 1,
              justifyContent: "center",
              borderWidth:0,
              height:55,
              borderRadius:0,
              marginRight:1
              }}
              onPress={() => { this.saveSign() } }>
            <Text>Save</Text>
          </Button>

          <Button light
              disabled = {this.state.ButtonStateHolder}
              style = {{
              flexDirection: "row",
              flexWrap: "wrap",
              flex: 1,
              justifyContent: "center",
              borderWidth:0,
              height:55,
              borderRadius:0
              }}
              onPress={() => { this.resetSign() } } >
            <Text>Reset</Text>
          </Button>


          </Row>

          </Grid>

          </Footer>
        </Container>
      );


  }
}


const mapStateToProps = state => ({
  aboutData: state.aboutData,
});

export default connect(mapStateToProps)(AboutUs);