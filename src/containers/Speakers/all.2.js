import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
} from 'native-base';

import { ToastAndroid, SectionList, View, Platform, Alert } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../Agenda/styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';

class Speakers extends Component {



    componentDidMount(){

      console.log("ss anya speakers");

        this.props.dispatch({
          type: 'FETCH_SPEAKERS_LIST_ALL'
        });
    
      }


  closeDrawer = () => {
    this.drawer._root.close()
  }

  openDrawer = () => {
    this.drawer._root.open()
  }

  showpage = () => {
    console.log("testnya");

  }

  onPressSpeakers(data) {
    
    this.props.navigation.navigate('SpeakersDetail', { ...data });
    
  }

  GetSectionListItem=(item)=>{
 
    Alert.alert(item)
 
  }  

  render() {

    var A = [];
    var B = [];
    var C = [];
    var D = [];
    var E = [];
    var F = [];
    var G = [];
    var H = [];
    var I = [];
    var J = [];
    var K = [];
    var L = [];
    var M = [];
    var N = [];
    var O = [];
    var P = [];
    var Q = [];
    var R = [];
    var S = [];
    var T = [];
    var U = [];
    var V = [];
    var W = [];
    var X = [];
    var Y = [];
    var Z = [];
    var sectionTitle = [];
    var sectionTitleArray = [];


    // console.log("ss anya speakers",this.props.speakerAllData.speakersAll);
    const { speakersAll } = this.props.speakerAllData;
    const {
      eventAddress, eventTitle, eventDescription, source,
    } = this.props.navigation.state.params;

    // console.log("trace anya",speakersAll);
    speakersAll.map((userData) => {
      console.log("trace anya",userData.name , userData.name.charAt(0).toUpperCase());

      if(userData.name.charAt(0).toUpperCase() == 'A'){
        // console.log("trace anya",sectionTitle.indexOf("A"));

        if(sectionTitle.indexOf('A') == -1){
          sectionTitle.push('A');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": AA})
        }

        // console.log("trace anya",userData.name , userData.name.charAt(0).toUpperCase());
        A.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'B'){
        if(sectionTitle.indexOf('B') == -1){
          sectionTitle.push('B');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": BB})
        }
        // console.log("trace anya",userData.name , userData.name.charAt(0).toUpperCase());
        B.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'C'){
        if(sectionTitle.indexOf('C') == -1){
          sectionTitle.push('C');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": CC})
        }

        C.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'D'){
        if(sectionTitle.indexOf('D') == -1){
          sectionTitle.push('D');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": DD})
        }

        D.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'E'){
        if(sectionTitle.indexOf('E') == -1){
          sectionTitle.push('E');
          // sectionTitleArray.push({"title": "Fruits Name From E", "data": EE})
        }

        E.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'F'){
        if(sectionTitle.indexOf('F') == -1){
          sectionTitle.push('F');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": FF})
        }
        F.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'G'){
        if(sectionTitle.indexOf('G') == -1){
          sectionTitle.push('G');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": GG})
        }
        G.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'H'){
        if(sectionTitle.indexOf('H') == -1){
          sectionTitle.push('H');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": HH})
        }
        H.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'I'){
        if(sectionTitle.indexOf('I') == -1){
          sectionTitle.push('I');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": II})
        }
        I.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'J'){
        if(sectionTitle.indexOf('J') == -1){
          sectionTitle.push('J');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": JJ})
        }
        J.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'K'){
        if(sectionTitle.indexOf('K') == -1){
          sectionTitle.push('K');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": KK})
        }
        K.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'L'){
        if(sectionTitle.indexOf('L') == -1){
          sectionTitle.push('L');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": LL})
        }
        L.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'M'){
        if(sectionTitle.indexOf('M') == -1){
          sectionTitle.push('M');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": MM})
        }
        M.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'N'){
        if(sectionTitle.indexOf('N') == -1){
          sectionTitle.push('N');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": NN})
        }
        N.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'O'){
        if(sectionTitle.indexOf('O') == -1){
          sectionTitle.push('O');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": OO})
        }
        O.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'P'){
        if(sectionTitle.indexOf('P') == -1){
          sectionTitle.push('P');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": PP})
        }
        P.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'Q'){
        if(sectionTitle.indexOf('Q') == -1){
          sectionTitle.push('Q');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": QQ})
        }
        Q.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'R'){
        if(sectionTitle.indexOf('R') == -1){
          sectionTitle.push('R');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": RR})
        }
        R.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'S'){
        if(sectionTitle.indexOf('S') == -1){
          sectionTitle.push('S');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": SS})
        }
        S.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'T'){
        if(sectionTitle.indexOf('T') == -1){
          sectionTitle.push('T');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": TT})
        }
        T.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'U'){
        if(sectionTitle.indexOf('U') == -1){
          sectionTitle.push('U');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": UU})
        }
        U.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'V'){
        if(sectionTitle.indexOf('V') == -1){
          sectionTitle.push('V');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": VV})
        }
        V.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'W'){
        if(sectionTitle.indexOf('W') == -1){
          sectionTitle.push('W');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": WW})
        }
        W.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'X'){
        if(sectionTitle.indexOf('X') == -1){
          sectionTitle.push('X');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": XX})
        }
        X.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'Y'){
        if(sectionTitle.indexOf('Y') == -1){
          sectionTitle.push('Y');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": YY})
        }
        Y.push(userData.name)}
      if(userData.name.charAt(0).toUpperCase() == 'Z'){
        if(sectionTitle.indexOf('Z') == -1){
          sectionTitle.push('Z');
          // sectionTitleArray.push({"title": "Fruits Name From D", "data": ZZ})
        }
        Z.push(userData.name)}
      
    });


    console.log("sectionTitleArray",sectionTitle);
    


    return (
      <DrawerView
        onClose={() => this.closeDrawer()}
        referVar={(ref) => { this.drawer = ref; }}
        content={<SideMenu {...this.props} />}
      >
        <Container style={styles.container}>
          <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.openDrawer()}
            leftIcon="ios-menu"
            title="Speakers"
          />
          <Content padder>
          <View style={{ marginTop : (Platform.OS) == 'ios' ? 20 : 0 }}>

<SectionList
              dataArray={speakersAll}
              renderItem={data =>
                        (
                          console.log("ss anya trace",data),
                          <ListItem 
                          
                          onPress = {() => this.onPressSpeakers(data) }
                        avatar>
                            <Left>
                            <Thumbnail small source={{ uri: 'http://192.168.90.95:1337/get_file?fd='+data.image }} />
                            </Left>
                            <Body>
                            <Text>
                                {data.name}
                            </Text>
                            <Text numberOfLines={1} note>
                              {data.description}
                            </Text>
                            </Body>
                            <Right>
                            <Button transparent
                              style={{
                                color: '#666666', 
                                border: 0,
                                margin:0, 
                                padding:0,
                                height:30
                              }}
                            >
                              <Icon type="Ionicons" name="ios-bookmark" 
                              style={{
                                color: '#666666', 
                                border: 0,
                                margin:0, 
                                padding:0
                                }} />
                            </Button>

                            </Right>
                        </ListItem>  
                        )}
    sections={[

      { title: 'A', data: A }, { title: 'B', data: B }, { title: 'C', data: C }, { title: 'D', data: D }, { title: 'E', data: E },
      { title: 'F', data: F }, { title: 'G', data: G }, { title: 'H', data: H }, { title: 'I', data: I }, { title: 'J', data: J },
      { title: 'K', data: K }, { title: 'L', data: L }, { title: 'M', data: M }, { title: 'N', data: N }, { title: 'O', data: O },
      { title: 'P', data: P }, { title: 'Q', data: Q }, { title: 'R', data: R }, { title: 'S', data: S }, { title: 'T', data: T },
      { title: 'U', data: U }, { title: 'V', data: V }, { title: 'W', data: W }, { title: 'X', data: X }, { title: 'Y', data: Y },
      { title: 'Z', data: Z },

    ]}

    renderSectionHeader={ ({section}) => 

      sectionTitle.indexOf(section.title) != -1 ? <Text style={styles.SectionHeaderStyle}> { section.title } </Text> : null

    }

    // renderItem={ ({item}) => <Text style={styles.SectionListItemStyle} onPress={this.GetSectionListItem.bind(this, item)}> { item } </Text> }

    keyExtractor={ (item, index) => index }
   
  />

</View>
          </Content>
        </Container>
      </DrawerView>
    );
  }
} 

const mapStateToProps = state => ({
    speakerAllData: state.speakerAllData,
});

export default connect(mapStateToProps)(Speakers);
