import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Common.whiteColor,
  },
  aboutText: {
    padding: 20,
    paddingTop: 0,
  },
  textStyle: {
    fontSize: 16,
    marginTop: 20,
  },
  imageSpeaker:{
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:20,
    marginBottom:10
  },
  iconSpeaker:{
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:0,
    marginBottom:0,
    
    borderBottomWidth:1,
    borderColor:'#e5e5e5'
  },
  body: {
    flex: 1,
    paddingLeft: 0,
    paddingRight: 0,
    backgroundColor: '#ffffff'
  },
  titleSpeaker:{
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    
    marginBottom:0
  },
  titleSpeakerText:{
    fontSize:23,
    color:"#666666"
  },
  descriptionSpeakerText:{
    fontSize:12,
    color:"#666666",
    textAlign: 'justify'
  },
  descriptionSpeaker:{
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:30,
    // borderBottomWidth:1,
    // borderColor:'#e5e5e5',
    paddingBottom:40
  },  
  deskripsiContent:{
    padding: 20,
    backgroundColor: '#f8f8f8',
    borderTopWidth:1,
    borderBottomWidth:1,
    borderColor:'#e5e5e5',
    minHeight:20,
    marginTop:30
  },
  deskripsiContentTwo:{
    padding: 20,
    backgroundColor: '#f8f8f8',
    minHeight:20,
    borderBottomWidth:1,
    borderColor:'#e5e5e5'
  },  
  bioContent:{
    marginTop:20,
    color:"#666666"
  },
  container: {
    flex: 1,
    width: null,
    height: null,
    backgroundColor: "#FFF"
  },
  iconContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-around",
    paddingLeft: 15
  },
  iconText: {
    fontSize: 12
  },
  icon: {
    width: 45,
    height: 45,
    justifyContent: "center"
  },
  col: {
    alignItems: "center",
    // backgroundColor:"#cccccc",
    marginLeft:70,
    paddingBottom:30
  },
  colTwo: {
    alignItems: "center",
    // backgroundColor:"#cccccc",
    marginRight:70,
    paddingBottom:30
  },
  row: {
    paddingBottom: 20
  },
  iconStyle:{
    color: "#666666", 
    fontSize:35 
  },
  iconText:{
    fontSize:15,
    marginTop:10,
    color: "#666666"
  },
  textAreaContainer: {
    // borderColor: "#cccccc",
    // borderWidth: 1,
    flex:1,
    padding: 5
  },
  textArea: {
    height: 150,
    justifyContent: "flex-start"
  },
  iconBookmarkAll:{
    color: "#666666",
    backgroundColor: "#666666", 
    // fontSize: 100 ,
    // height:100
  },
  SectionHeaderStyle:{
    backgroundColor : '#CDDC39',
    fontSize : 20,
    padding: 5,
    paddingLeft:20,
    color: '#fff'
  },

  viewStyle: {
    paddingTop: 15,
    paddingLeft: 0,
    paddingRight: 0,
    paddingBottom: 0,
    flex: 1,
  },
  gridStyle: {
    borderBottomWidth: 1,
    borderBottomColor: Common.lightGray,
    paddingBottom: 15,
  },
  titleItemName:{
    fontSize:15,
    fontWeight: 'bold',
    color: "#666666",
  },
  subtitleItemName:{
    fontSize:13,
    color: "#666666",
  }
  
});

module.exports = styles;
