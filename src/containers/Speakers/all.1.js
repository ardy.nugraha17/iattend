import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
} from 'native-base';

import { ToastAndroid } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../Agenda/styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';

class Speakers extends Component {


    componentDidMount(){

      console.log("ss anya speakers");

        this.props.dispatch({
          type: 'FETCH_SPEAKERS_LIST_ALL'
        });
    
      }


  closeDrawer = () => {
    this.drawer._root.close()
  }

  openDrawer = () => {
    this.drawer._root.open()
  }

  showpage = () => {
    console.log("testnya");

  }

  onPressSpeakers(data) {
    
    this.props.navigation.navigate('SpeakersDetail', { ...data });
    
  }

  render() {
    // console.log("ss anya speakers",this.props.speakerAllData.speakersAll);
    const { speakersAll } = this.props.speakerAllData;
    const {
      eventAddress, eventTitle, eventDescription, source,
    } = this.props.navigation.state.params;
    return (
      <DrawerView
        onClose={() => this.closeDrawer()}
        referVar={(ref) => { this.drawer = ref; }}
        content={<SideMenu {...this.props} />}
      >
        <Container style={styles.container}>
          <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.openDrawer()}
            leftIcon="ios-menu"
            title="Speakers"
          />
          <Content padder>
            <List
              dataArray={speakersAll}
              renderRow={data =>
                        (
                          <ListItem 
                          
                          onPress = {() => this.onPressSpeakers(data) }
                        avatar>
                            <Left>
                            <Thumbnail small source={{ uri: 'http://192.168.43.66:1337/get_file?fd='+data.image }} />
                            </Left>
                            <Body>
                            <Text>
                                {data.name}
                            </Text>
                            <Text numberOfLines={1} note>
                              {data.description}
                            </Text>
                            </Body>
                            <Right>
                            <Button transparent
                              style={{
                                color: '#666666', 
                                border: 0,
                                margin:0, 
                                padding:0,
                                height:30
                              }}
                            >
                              <Icon type="Ionicons" name="ios-bookmark" 
                              style={{
                                color: '#666666', 
                                border: 0,
                                margin:0, 
                                padding:0
                                }} />
                            </Button>

                            </Right>
                        </ListItem>  
                        )}
            />
          </Content>
        </Container>
      </DrawerView>
    );
  }
} 

const mapStateToProps = state => ({
    speakerAllData: state.speakerAllData,
});

export default connect(mapStateToProps)(Speakers);
