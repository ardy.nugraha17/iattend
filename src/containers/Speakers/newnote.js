import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  Grid,
  Col,
  Row,
  Footer
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, TextInput, ImageBackground, AsyncStorage } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../Speakers/styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import ApiCaller from '../../common/apiCaller';


const launchscreenBg = require("../../../src/images/paperback.png");

class NoteSpeakers extends Component {

constructor(props) {
    super(props);
    this.state = {
        text: '',
        heading:'',
        ButtonStateHolder: ''
    }
}

componentDidMount(){

  AsyncStorage.getItem("userData").then((data) => {
   
    console.log("ss anya",JSON.parse(data).audienceid);
  
  }).done();

}

onPressSaveNote() {
  AsyncStorage.getItem("userData").then((data) => {

      const titleNote = this.state.heading;
    
      ApiCaller('addnote', 'post', JSON.parse(data).token, {
        text: this.state.text, heading:titleNote, id:JSON.parse(data).audienceid }).then(response =>{
          console.log("ss anya balikan addnote",response);
        if(response.affectedRows == 1){
        
         Helper.navigateToPage(this, 'NotesList')
        }
      })

    
  }).done();
 
}

onPressChangeText(value){

  this.setState({
    text: value,
    heading: value
  });


}







  render() {


    // alert( JSON.stringify(this.props.navigation.state.params.data));

    return (
        <Container style={styles.container}>


<ImageBackground 
        //    source={{uri: 'https://static.vecteezy.com/system/resources/previews/000/136/063/non_2x/dirty-vector-paper-texture.jpg'}}
           source={launchscreenBg}
           style={{ flex: 1,
             width: null,
             height: '100%',
             }}
>



            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="New Note"
            />
  
          <Content>
          <View>
  
            <Content padder style={{padding:10}}>     


       <View style={styles.textAreaContainer}>

            <TextInput
            autoFocus={true}
            style={{textAlignVertical: 'top',fontSize:17,lineHeight:27}}
            multiline={true}
            numberOfLines={10}
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.onPressChangeText({text})}
            value={this.state.text}/>

            </View>







            

  
             </Content>
          </View>
          </Content>
  
          <Footer style={{ backgroundColor: "transparent" }}>

        <Grid>

          <Row style={{ marginTop: 0, marginBottom: 0 }}>


                  <Button light 
                      style = {{
                      flexDirection: "row",
                      flexWrap: "wrap",
                      flex: 1,
                      justifyContent: "center",
                      borderWidth:0,
                      height:55,
                      borderRadius:0,
                      marginRight:1
                     }}
                     
                     onPress={() => this.props.navigation.goBack(null)}
                     >
                    <Text>Cancel</Text>
                  </Button>

                  <Button success
                      disabled = {this.state.ButtonStateHolder}
                      style = {{
                      flexDirection: "row",
                      flexWrap: "wrap",
                      flex: 1,
                      justifyContent: "center",
                      borderWidth:0,
                      height:55,
                      borderRadius:0
                     }}

                     onPress={() => this.onPressSaveNote()}
                  >
                    <Text>Save</Text>
                  </Button>


          </Row>

        </Grid>

        </Footer>
        </ImageBackground >
        </Container>
      );


  }
}


export default NoteSpeakers;