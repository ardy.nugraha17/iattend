import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  Grid,
  Col,
  Row
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../Speakers/styles';

import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';
import email from 'react-native-email'

class DetailSpeakers extends Component {


  constructor(props) {

    super(props);
    this.state = {
      showDialog: false,
      dialogVisible: false
    }
  }

  onPressNote(id) {
    // kalau ada kiriman variable, page selanjutnya dikasih initial : alert( JSON.stringify(this.props.navigation.state.params.data));
    // this.props.navigation.navigate('SpeakersNewNote', { id: id });
    this.props.navigation.navigate('SpeakersNewNote');
  }

  onPressShare() {
    this.setState({ showDialog: true })
  }

  onPressShareApi(){
    this.setState({showDialog: false});
    Share.share(
      {
          
        message: "test share"
      
      }).then(result => console.log(result)).catch(errorMsg => console.log(errorMsg));
  }

  onPressEmail(){
    this.setState({showDialog: false});
    const to = ['tiaan@gmail.com', 'foo@gmail.com'] // string or array of email addresses
    email(to, {
        // Optional additional arguments
        cc: ['bazzy@gmail.com', 'doooo@gmail.com'], // string or array of email addresses
        bcc: 'mee@gmail.com', // string or array of email addresses
        subject: 'Show how to use',
        body: 'Some body right here'
    }).catch(console.error)
    
  }

  onPressBookmark() {
    this.setState({ dialogVisible: true })
  }


  onPressDevelopment(){
    ToastAndroid.show('Still Development', ToastAndroid.SHORT);
  }

  render() {
   
    const speaker= this.props.navigation.state.params;
    return (
        <Container style={styles.container}>

                <Dialog
                    visible={this.state.showDialog}
                    title="Share"
                    onTouchOutside={() => this.setState({showDialog: false})}
                    // contentStyle={{ justifyContent: 'center', alignItems: 'center', }}
                    animationType="fade">
                    <View>
                      <Text
                        style={{textAlign :'left'}}
                      >Post to Event Feed</Text>
                      <Text
                        onPress={() => this.onPressEmail()}
                      >Email</Text>
                      <Text 
                        onPress={() => this.onPressShareApi()}
                      >Share to social</Text>

                    </View>
                </Dialog>

                <ConfirmDialog
                    title="Confirm Dialog"
                    visible={this.state.dialogVisible}
                    onTouchOutside={() => this.setState({dialogVisible: false})}
                    positiveButton={{
                        title: "LOGIN",
                        onPress: () => alert("Ok touched!")
                    }}
                    
                    negativeButton={{
                      title: "CANCEL",
                      onPress: () => this.setState({dialogVisible: false}) 
                    }}
                    >
                    <View>
                        <Text>
                          You must login first
                        </Text>
                    </View>
                </ConfirmDialog>





            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            // onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title={speaker.name}
            />
  
          <Content>
          <View>
             <Content>
             {/* this is for icon avatar */} 


             <View style={styles.imageSpeaker}>

              {/* <Image
                style={{width: 200, height: 200}}
                source={{uri: 'http://192.168.90.95:1337/get_file?fd='+speaker.image}} 
              /> */}

              <Thumbnail 
              style={{width: 200, height: 200, borderRadius: 200/2}}
              source={{ uri: 'http://192.168.84.229:1337/get_file?fd='+speaker.image }} />

             </View>

            {/* this is for name */}
            <View style={styles.titleSpeaker}>
            <Text style={styles.titleSpeakerText} >{speaker.name}</Text>
            </View>

            <Content padder style={{paddingLeft:30,paddingRight:30}}>

            {/* this is for description */}
            <View style={styles.descriptionSpeaker}>
            <Text style={styles.descriptionSpeakerText}>{speaker.description}</Text>
            </View>

            </Content>


            <View style={styles.iconSpeaker}>
            <Row style={styles.row}>

              <Col style={styles.col}>
              {/* <Icon type="IonIcons" name="ios-create-outline" style={styles.iconStyle} onPress={() => this.onPressNote(this.props.navigation.state.params.id) }  /> */}
              <Icon type="IonIcons" name="ios-create-outline" style={styles.iconStyle} onPress={() => this.onPressDevelopment() }  />
              <Text numberOfLines={1} style={styles.iconText}>
              NOTE
              </Text>
              </Col>

              <Col style={styles.colTwo}>
              <Icon type="Ionicons" active name="ios-bookmark-outline" style={styles.iconStyle} onPress={() => this.onPressDevelopment() }/>
              <Text numberOfLines={1} style={styles.iconText}>
              BOOKMARK
              </Text>
              </Col>

            </Row>
            </View>


            <View style={styles.deskripsiContentTwo}>
            <Text style={{fontWeight:"bold",fontSize:18,color:"#666666"}}>Skills</Text>
            <Text style={styles.bioContent}>{speaker.skills}</Text>
            </View>

            <View style={styles.deskripsiContentTwo}>
            <Text style={{fontWeight:"bold",fontSize:18,color:"#666666"}}>Experience</Text>
            <Text style={styles.bioContent}>{speaker.experience}</Text>
            </View>        

            <View style={styles.deskripsiContentTwo}>
            <Text style={{fontWeight:"bold",fontSize:18,color:"#666666"}}>Education</Text>
            <Text style={styles.bioContent}>{speaker.education}</Text>
            </View>

             </Content>
          </View>
          </Content>
  
        </Container>
      );


  }
}


export default DetailSpeakers;