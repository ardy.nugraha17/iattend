import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Grid, Row, Col,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
} from 'native-base';

import { ToastAndroid, SectionList, View, Platform, Alert, TouchableOpacity, BackHandler } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../Speakers/styles';
import { eventImage } from '../../common/constant';
import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';


class Speaker extends Component {


  constructor(props) {

    super(props);
    this.state = {
      dialogVisible: false,
      idSpeakerBookmark: 0
    }
  }



  componentDidMount(){

    // console.log("ss anya speakers");
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  goBack(){
    this.props.navigation.goBack(null)
  }


  componentWillReceiveProps(nextProps){
    console.log("ss componentWillReceiveProps", nextProps);
  }


  closeDrawer = () => {
    this.drawer._root.close()
  }

  openDrawer = () => {
    this.drawer._root.open()
  }

  showpage = () => {
    console.log("testnya");

  }

  onPressSpeakers(data) {
    this.props.navigation.navigate('SpeakersDetail', { ...data });
  }

  GetSectionListItem=(item)=>{
    Alert.alert(item)
  }  

  onPressBookmark(idSpeaker) {
    this.setState({ dialogVisible: true, idSpeakerBookmark:idSpeaker })
  }

  sortBy(key, reverse) {

    // Move smaller items towards the front
    // or back of the array depending on if
    // we want to sort the array in reverse
    // order or not.
    var moveSmaller = reverse ? 1 : -1;
  
    // Move larger items towards the front
    // or back of the array depending on if
    // we want to sort the array in reverse
    // order or not.
    var moveLarger = reverse ? -1 : 1;
  
    /**
     * @param  {*} a
     * @param  {*} b
     * @return {Number}
     */
    return function(a, b) {
      if (a[key] < b[key]) {
        return moveSmaller;
      }
      if (a[key] > b[key]) {
        return moveLarger;
      }
      return 0;
    };
  
  }  

  onPressDevelopment(){
    ToastAndroid.show('Still Development', ToastAndroid.SHORT);
  }

  render() {

    const {navigation} = this.props;

    var A = [];
    var B = [];
    var C = [];
    var D = [];
    var E = [];
    var F = [];
    var G = [];
    var H = [];
    var I = [];
    var J = [];
    var K = [];
    var L = [];
    var M = [];
    var N = [];
    var O = [];
    var P = [];
    var Q = [];
    var R = [];
    var S = [];
    var T = [];
    var U = [];
    var V = [];
    var W = [];
    var X = [];
    var Y = [];
    var Z = [];
    var sectionTitle = [];
    var sectionTitleArray = [];


    console.log("ss anya speakers",this.props);
    // const { speakersAll } = this.props.speakerAllData;
    const { speakers } = this.props.speakerData;
    const {
      eventAddress, eventTitle, eventDescription, source,
    } = this.props.navigation.state.params;

    // console.log("trace anya",speakersAll);
    speakers.map((userData) => {
      console.log("trace anya",userData.name , userData.name.charAt(0).toUpperCase());

      if(userData.name.charAt(0).toUpperCase() == 'A'){
        // console.log("trace anya",sectionTitle.indexOf("A"));

        if(sectionTitle.indexOf('A') == -1){
          sectionTitle.push('A');
          sectionTitleArray.push({"title": "A", "data": A})
        }

        // console.log("trace anya",userData.name , userData.name.charAt(0).toUpperCase());
        A.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'B'){
        if(sectionTitle.indexOf('B') == -1){
          sectionTitle.push('B');
          sectionTitleArray.push({"title": "B", "data": B})
        }
        // console.log("trace anya",userData.name , userData.name.charAt(0).toUpperCase());
        // B.push(userData.name)}
        B.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'C'){
        if(sectionTitle.indexOf('C') == -1){
          sectionTitle.push('C');
          sectionTitleArray.push({"title": "C", "data": C})
        }

        // C.push(userData.name)}
        C.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'D'){
        if(sectionTitle.indexOf('D') == -1){
          sectionTitle.push('D');
          sectionTitleArray.push({"title": "D", "data": D})
        }

        // D.push(userData.name)}
        D.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'E'){
        if(sectionTitle.indexOf('E') == -1){
          sectionTitle.push('E');
          sectionTitleArray.push({"title": "E", "data": E})
        }

        // E.push(userData.name)}
        E.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'F'){
        if(sectionTitle.indexOf('F') == -1){
          sectionTitle.push('F');
          sectionTitleArray.push({"title": "F", "data": F})
        }
        // F.push(userData.name)}
        F.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'G'){
        if(sectionTitle.indexOf('G') == -1){
          sectionTitle.push('G');
          sectionTitleArray.push({"title": "G", "data": G})
        }
        // G.push(userData.name)}
        G.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'H'){
        if(sectionTitle.indexOf('H') == -1){
          sectionTitle.push('H');
          sectionTitleArray.push({"title": "H", "data": H})
        }
        // H.push(userData.name)}
        H.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'I'){
        if(sectionTitle.indexOf('I') == -1){
          sectionTitle.push('I');
          sectionTitleArray.push({"title": "I", "data": I})
        }
        // I.push(userData.name)}
        I.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'J'){
        if(sectionTitle.indexOf('J') == -1){
          sectionTitle.push('J');
          sectionTitleArray.push({"title": "J", "data": J})
        }
        // J.push(userData.name)}
        J.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'K'){
        if(sectionTitle.indexOf('K') == -1){
          sectionTitle.push('K');
          sectionTitleArray.push({"title": "K", "data": K})
        }
        // K.push(userData.name)}
        K.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'L'){
        if(sectionTitle.indexOf('L') == -1){
          sectionTitle.push('L');
          sectionTitleArray.push({"title": "L", "data": L})
        }
        // L.push(userData.name)}
        L.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'M'){
        if(sectionTitle.indexOf('M') == -1){
          sectionTitle.push('M');
          sectionTitleArray.push({"title": "M", "data": M})
        }
        // M.push(userData.name)}
        M.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'N'){
        if(sectionTitle.indexOf('N') == -1){
          sectionTitle.push('N');
          sectionTitleArray.push({"title": "N", "data": N})
        }
        // N.push(userData.name)}
        N.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'O'){
        if(sectionTitle.indexOf('O') == -1){
          sectionTitle.push('O');
          sectionTitleArray.push({"title": "O", "data": O})
        }
        // O.push(userData.name)}
        O.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'P'){
        if(sectionTitle.indexOf('P') == -1){
          sectionTitle.push('P');
          sectionTitleArray.push({"title": "P", "data": P})
        }
        // P.push(userData.name)}
        P.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'Q'){
        if(sectionTitle.indexOf('Q') == -1){
          sectionTitle.push('Q');
          sectionTitleArray.push({"title": "Q", "data": Q})
        }
        // Q.push(userData.name)}
        Q.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'R'){
        if(sectionTitle.indexOf('R') == -1){
          sectionTitle.push('R');
          sectionTitleArray.push({"title": "R", "data": R})
        }
        // R.push(userData.name)}
        R.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'S'){
        if(sectionTitle.indexOf('S') == -1){
          sectionTitle.push('S');
          sectionTitleArray.push({"title": "S", "data": S})
        }
        // S.push(userData.name)}
        S.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'T'){
        if(sectionTitle.indexOf('T') == -1){
          sectionTitle.push('T');
          sectionTitleArray.push({"title": "T", "data": T})
        }
        // T.push(userData.name)}
        T.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'U'){
        if(sectionTitle.indexOf('U') == -1){
          sectionTitle.push('U');
          sectionTitleArray.push({"title": "U", "data": U})
        }
        // U.push(userData.name)}
        U.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'V'){
        if(sectionTitle.indexOf('V') == -1){
          sectionTitle.push('V');
          sectionTitleArray.push({"title": "V", "data": V})
        }
        // V.push(userData.name)}
        V.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'W'){
        if(sectionTitle.indexOf('W') == -1){
          sectionTitle.push('W');
          sectionTitleArray.push({"title": "W", "data": W})
        }
        // W.push(userData.name)}
        W.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'X'){
        if(sectionTitle.indexOf('X') == -1){
          sectionTitle.push('X');
          sectionTitleArray.push({"title": "X", "data": X})
        }
        // X.push(userData.name)}
        X.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'Y'){
        if(sectionTitle.indexOf('Y') == -1){
          sectionTitle.push('Y');
          sectionTitleArray.push({"title": "Y", "data": Y})
        }
        // Y.push(userData.name)}
        Y.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      if(userData.name.charAt(0).toUpperCase() == 'Z'){
        if(sectionTitle.indexOf('Z') == -1){
          sectionTitle.push('Z');
          sectionTitleArray.push({"title": "Z", "data": Z})
        }
        // Z.push(userData.name)}
        Z.push({
          "id": userData.id, 
          "name": userData.name, 
          "image": userData.image, 
          "skills": userData.skills, 
          "education": userData.education, 
          "experience": userData.experience,
          "description": userData.description,
        })}

      
    });


    console.log("sectionTitleArray",sectionTitle);

    const SECTIONS = sectionTitleArray.sort(this.sortBy('title'));
    


    return (
      <DrawerView
        onClose={() => this.closeDrawer()}
        referVar={(ref) => { this.drawer = ref; }}
        content={<SideMenu {...this.props} onClose={() => this.closeDrawer()} />}
      >
        <Container style={styles.container}>


          <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            leftIcon="md-arrow-back"
            // rightIcon="ios-search"
            title="Speakers"
          />
          <Content>
          <View style={{ marginTop : (Platform.OS) == 'ios' ? 20 : 0 }}>


            <ConfirmDialog
            // title="You must be logged"
            visible={this.state.dialogVisible}
            onTouchOutside={() => this.setState({dialogVisible: false})}
            positiveButton={{
                title: "LOG IN",
                onPress: () => alert("Ok touched!")
            }}
            
            negativeButton={{
              title: "CLOSE",
              onPress: () => this.setState({dialogVisible: false}) 
            }}
            >
            <View>
                <Text style={{fontWeight: 'bold'}}>
                  You must be logged in to perform this action {this.state.idSpeakerBookmark}
                </Text>

                <Text>
                  Log in to continue
                </Text>
            </View>
            </ConfirmDialog>


          <SectionList
    sections={SECTIONS}
    renderSectionHeader={ ({section}) => 
      <Text style={styles.SectionHeaderStyle}> { section.title } </Text>
    }

    renderItem={ ({item}) => 
    
    <View style={styles.viewStyle}>

      <ListItem 
      // noBorder
      thumbnail
      button onPress={() => this.onPressSpeakers(item)}
      >
        <Left style={{margin:0}}>

        <Thumbnail small
        style={{marginLeft: 0}}
        source={{ uri: 'https://eventapp.dexagroup.com/apiget_file?fd='+item.image }} />  

        </Left>
        <Body>
        <Text style={styles.titleItemName}>{ item.name }</Text>
          <Text note style={styles.subtitleItemName}>{ item.skills }</Text>
        </Body>
      </ListItem>


  </View>
    }

    keyExtractor={ (item, index) => index }
   
  />

</View>
          </Content>
        </Container>
      </DrawerView>
    );
  }
} 

const mapStateToProps = state => ({
    speakerData: state.speakerData,
});

export default connect(mapStateToProps)(Speaker);