import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  List,
  Thumbnail,
  ListItem,
  Grid,
  Col,
  Row
} from 'native-base';

import { ToastAndroid, View, StatusBar, Image, Share, Dimensions, ScrollView, BackHandler } from 'react-native';

import HeaderStyles from '../../component/Header/styles';
import styles from '../Speakers/styles';

import * as Helper from '../../common/helper';
import Header from '../../component/Header';
import SideMenu from '../SideMenu'
import DrawerView from '../../component/DrawerView';
import {
  Dialog,
  ProgressDialog,
  ConfirmDialog,
} from 'react-native-simple-dialogs';
import email from 'react-native-email';
import HTML from 'react-native-render-html';

class AboutUs extends Component {


  constructor(props) {

    super(props);
    this.state = {
      showDialog: false,
      dialogVisible: false
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  onPressNote(id) {
    // kalau ada kiriman variable, page selanjutnya dikasih initial : alert( JSON.stringify(this.props.navigation.state.params.data));
    // this.props.navigation.navigate('SpeakersNewNote', { id: id });
    this.props.navigation.navigate('SpeakersNewNote');
  }

  onPressShare() {
    this.setState({ showDialog: true })
  }

  onPressShareApi(){
    this.setState({showDialog: false});
    Share.share(
      {
          
        message: "test share"
      
      }).then(result => console.log(result)).catch(errorMsg => console.log(errorMsg));
  }

  onPressEmail(){
    this.setState({showDialog: false});
    const to = ['tiaan@gmail.com', 'foo@gmail.com'] // string or array of email addresses
    email(to, {
        // Optional additional arguments
        cc: ['bazzy@gmail.com', 'doooo@gmail.com'], // string or array of email addresses
        bcc: 'mee@gmail.com', // string or array of email addresses
        subject: 'Show how to use',
        body: 'Some body right here'
    }).catch(console.error)
    
  }

  onPressBookmark() {
    this.setState({ dialogVisible: true })
  }

  goBack(){
    this.props.navigation.goBack(null);
  }


  render() {
   
    const speaker= this.props.navigation.state.params;
    const {navigation} = this.props;
    
    return (
        <Container style={styles.container}>

            <Header
            headerContainer={styles.headerContainerStyle}
            headerTextStyle={styles.headerTextStyle}
            headerLeft={{padding: 10}}
            onLeftIconClick={() => this.props.navigation.goBack(null)}
            onRightIconClick={() => this.onPressShare()}
            leftIcon="md-arrow-back"
            // rightIcon="share"
            title="About"
            />
  
          <Content>
          
          <View style={styles.aboutText}>
             {/* <Text style={styles.textStyle}>{}</Text> */}
             <ScrollView style={{ flex: 1 }}>
                <HTML html={this.props.navigation.state.params.eventDescrip} imagesMaxWidth={Dimensions.get('window').width} />
            </ScrollView>
           </View>
             
          </Content>
  
        </Container>
      );


  }
}


const mapStateToProps = state => ({
  aboutData: state.aboutData,
});

export default connect(mapStateToProps)(AboutUs);