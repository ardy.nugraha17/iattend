import { StyleSheet } from 'react-native';
import * as Common from '../../common/common';
import {Dimensions} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const { height, width } = Dimensions.get('window');

export const w = percent => (width * percent) / 150;
export const h = percent => (height * percent) / 150;
export const totalSize = num => (Math.sqrt((height * height) + (width * width)) * num) / 150;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  bodyOtp: {
    // flex: 1,
    // backgroundColor:Common.lightGreen,
    // backgroundColor:'#f7f7f7',
    // flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf:'center',
    width:null,
    marginTop:wp('5%')
  },
  container: {
    flex: 1,
    // backgroundColor:Common.lightGreen,
    backgroundColor:'#3ad3f2',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerLogin: {
    flex: 1,
    backgroundColor: Common.whiteColor,
  },
  splashStyle: {
    height: Common.deviceHeight,
    width: Common.deviceWidth,
    resizeMode: 'stretch',
  },
  image: {
    flexGrow:1,
    height:null,
    width:null,
    alignItems: 'center',
    justifyContent:'center',
  },
  // paragraph: {
  //   textAlign: 'center',
  //   width: '80%',
  //   height: w(50),
  // },
  // ini untuk intro
  imageIntro: {
    width: 200,
    height: 200,
  },
  textIntro: {
    color: '#FFFFFF',
    fontSize: 16,
    lineHeight: 24
  },
  titleIntro: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 16,
  },
  backgroundImage: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // width: null,
    // height: null,
    flex: 1,
    width: null,
    height: null
  },
  body: {
    flex: 1,
    // paddingLeft: 10,
    // paddingRight: 10,
    // backgroundColor: '#F2F2F2',
    width: '80%',
    // height:200,
    // alignContent:'center',
    // alignItems:'center',
    alignSelf:'center',
    justifyContent:'center',
  },
  
  inputStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    // paddingLeft:10,
    // padding:10,
    padding:wp('0.5%'),
    paddingTop: 0,
    paddingLeft: 10,
    backgroundColor: Common.whiteColor,
    borderBottomWidth:1,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    // marginBottom:w(4),
    // backgroundColor:"#ffffff",
    // borderRadius: 50,
    // borderRadius: w(10),
    borderColor: '#af1e1e'
  },
  
  textInputAlt: {
    borderColor: '#f4efcb',
    paddingLeft: 0,
    // borderTopWidth: 0.8,
    // borderRightWidth:0.8,
    // borderBottomWidth:0.8,
    // borderLeftWidth: 0.8,
    // paddingLeft:10,
    // justifyContent: 'center',
    // alignItems: 'center',
    // flexDirection: 'row',
    // backgroundColor:"#ffffff",
    // marginBottom:0
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding:wp('2%'),
    paddingTop: 0,
    borderBottomWidth:1,
    marginBottom:w(4),
  },
  buttonTextStyleDua: {
    color: Common.darkColor
  },
  buttonStyleDua: {
    flex: 1, justifyContent: "center", 
    alignItems: "center", 
    // height: 50,
    // marginTop: 25,
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#ececec',
    // borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 10,
  },
  buttonTextStyle: {
    color: '#ffffff',
    // alignContent:'center',
    // alignItems:'center',
    // alignSelf:'center',
    // justifyContent:'center',
    // fontSize:w(3)
    // fontSize:14
    // fontSize:wp('4%'),
    // fontSize:windowWidth * 0.5 - windowWidth,
    
  },
  buttonStyle:{
    // backgroundColor:'#01c853',
    // backgroundColor:'#106b98',
    backgroundColor:'#af1e1e',
    width:'100%',
    padding: windowWidth * 0.75 - windowWidth * 0.73 ,
    // padding:20,
    // padding:w(4),
    // padding:wp('1.5%'),
    alignContent:'center',
    alignItems:'center',
    alignSelf:'center',
    justifyContent:'center',
    marginTop:wp('2%'),
    // marginTop: 20,
    // borderRadius: 50,
    // borderRadius: w(10),
    // borderRadius: 10,
  },
  buttonStyleRegister:{
    // backgroundColor:'#01c853',
    // backgroundColor:'#106b98',
    backgroundColor:'#af1e1e',
    width:'100%',
    padding: windowWidth * 0.75 - windowWidth * 0.73 ,
    // padding:20,
    // padding:w(4),
    // padding:wp('1.5%'),
    alignContent:'center',
    alignItems:'center',
    alignSelf:'center',
    justifyContent:'center',
    marginTop:wp('2%'),
    // marginTop: 20,
    // borderRadius: 50,
    // borderRadius: w(10),
    // borderRadius: 10,
  },
  paragraph: {
    textAlign: 'center',
    width:wp('60%'),
    height:hp('20%')
    // width: windowWidth * 0.75 - 100,
    // height: windowHeight * 0.33 - 100
  },
  label:{
    fontWeight:'bold', 
    // color:'#f7d581',
    color:'#af1e1e',
    fontSize:hp('1.8%'),
    // fontSize:windowWidth * 0.75 - windowWidth,
    marginBottom:wp('1%')
  },
  labelPassword:{
    fontWeight:'bold', 
    // color:'#f7d581',
    color:'#af1e1e',
    fontSize:hp('1.8%'),
    // fontSize:windowWidth * 0.75 - windowWidth,
    // marginBottom:2,
    marginBottom:wp('1%'),
    marginTop:20
  },
  InputTextStyle:{
    // fontSize:wp('4%'),
    fontSize:windowWidth * 0.75 - windowWidth,
    // color:'#ffffff',
    color:'#666666'
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  modalBackground:{
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper:{
    backgroundColor: '#ffffff',
    // height: 100,
    // width: 100,
    width:wp('15%'),
    height:hp('10%'),
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#666666',
    borderRadius: 4,
    color: '#000',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: '#666666',
    borderRadius: 8,
    color: '#000',
    paddingRight: 30, // to ensure the text is never behind the icon
  },

});

module.exports = styles;
