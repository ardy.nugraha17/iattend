import React, { Component } from 'react'
import {
  View,
  AsyncStorage,
  Dimensions,
  Linking,
  ImageBackground,
  Image,
  Alert,
  Text,
  StatusBar,
  Modal,
  ActivityIndicator,
  NetInfo,
  Vibration,
  ToastAndroid,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Button,
  BackHandler
} from 'react-native';
// import {Item, Icon, Input } from "native-base";
import { Input, Icon, Item, Label, ListItem, Body, Container, Content, Footer} from 'native-base';
import { connect } from 'react-redux';

import  styles from './styles';
import * as Helper from '../../common/helper';
import validate from '../../common/validate.js';
import Spinner from 'react-native-loading-spinner-overlay';

const { height, width } = Dimensions.get('window');
export const w = percent => (width * percent) / 150;
export const h = percent => (height * percent) / 150;
export const totalSize = num => (Math.sqrt((height * height) + (width * width)) * num) / 150;

var DeviceInfo = require('react-native-device-info');
import RNExitApp from 'react-native-exit-app';
import AutoHeightImage from 'react-native-auto-height-image';
import imageLogo from '../../images/logoIAttendLogin.png';
import ButtonInternal from '../../component/Button';
import DialogUpdate from "react-native-dialog";
import OfflineNotice from '../../component/Another/OfflineNotice';
import axios from 'axios';
import ApiCaller from '../../common/apiCaller';
import base64 from 'react-native-base64';
import { Dropdown } from 'react-native-material-dropdown';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Dialog, { DialogFooter, DialogButton, DialogContent, DialogTitle, SlideAnimation } from 'react-native-popup-dialog';
import RadioGroup from 'react-native-radio-buttons-group';
import OtpInputs from 'react-native-otp-inputs';

const successImageUri = 'https://cdn.pixabay.com/photo/2015/06/09/16/12/icon-803718_1280.png';

const constraints = {
  email: {
    presence: {
      allowEmpty:false,
      message: '^Please enter an email address'
    },
    email: {
      message: '^Please enter a valid email address'
    }
},
  password: {
    presence: {
      allowEmpty:false,
      message: '^Please enter a password'
    },
    length: {
      minimum: 1,
      message: '^Your password must be at least 1 characters'
    }
  }
}

const validator = (field, value) => {
  // Creates an object based on the field name and field value
  // e.g. let object = {email: 'email@example.com'}
  let object = {}
  object[field] = value

  let constraint = constraints[field]
  

  // Validate against the constraint and hold the error messages
  const result = validate(object, { [field]: constraint })
  

  // If there is an error message, return it!
  if (result) {
    // Return only the field error message if there are multiple
    return result[field][0]
  }

  return null
}

const ActivityStream = () => (
  <View >
    </View>
);


const DURATION = 500;
const PATTERN = [1000, 2000, 3000];


class Login extends Component {
  

  constructor(props) {
    super(props);

    this.onChangeTextConnection = this.onChangeTextConnection.bind(this);

    this.state = {
        data: [
          {
              label: 'Private Login',
              value: "Private Login",
          },
          {
              label: 'LDAP Login',
              value: "LDAP Login",
          }
      ],
      dataSourceDokter: [],  
      dataBackupDokter:[],
      nameConnection:'Private Login',
      visibleDialog: false,
      loader: false,
      // email: 'audience@gmail.com',
      email: '', 
      placeholderUsername:'Email address here',
      emailError: '',
      passwordError:'',
      emailListitem:'none',
      toggle:false,
      togglePassword:false,
      // password: '123456',
      password: '',
      placeholderPassword:'Password here',
      passwordError: '',
      userInfo:'',
      text: 'Sign out',
      cekInputUsername:false,
      cekInputPassword:false,  
      versionmobile:true,
      isConnected: true,
      statusDialog:0,
      otpConfirmation:false,
      phoneNumber:null,
      user: null,
      message: '',
      codeInput: '',
      confirmResult: null,
      otpSukses:false

    }


  }


  onPress = data => this.setState({ nameConnection: data });

  onChangeTextConnection(text) {

    console.log("onChangeTextConnection", text.find(e => e.selected == true).value);

    if(text.find(e => e.selected == true).value == 'Private Login'){
      
      if(this.state.email.length >= 2){
        this.setState({
          toggle:false,
          emailError: '',
          cekInputUsername:false,
          dataBackupDokter: '',  
          dataSourceDokter: '', 
          loader:true
        });


        ApiCaller('mobileIattend_autocomplete', 'post', 123456, {
          email : this.state.email
          
      
        }).then(response =>{

          this.setState({
            loader:false,
            dataBackupDokter: response.datanya,  
            dataSourceDokter: response.datanya
          });
    
          console.log("onBlurUsername", response);
          this.pressAutoComplete();
        
        })

        
      }
    }

    this.setState({ nameConnection : text.find(e => e.selected == true).value });
  }

  onTouchStartUsername(){
    this.setState({placeholderUsername: ''});
  }

  onTouchStartPassword(){
    this.setState({placeholderPassword: ''});
  }


  pressLDAP(){

    let { cekInputUsername, cekInputPassword, email, password, fcmToken } = this.state;

    if( cekInputUsername == true & cekInputPassword == true){
      this.setState({loader: true})

      
      // const username = "d2FoeXVuaUBkZXhhLW1lZGljYS5jb20";
      // const password = "MTIzNDU2";
      
  
      axios.post("http://portal.dexagroup.com//portal/app/login.php?email=" + base64.encode(email) + "&password=" + base64.encode(password))
        .then(res => {
          console.log("testing apildap consolelog sukses dua" + this.state.email + " : " + this.state.password + " : " + JSON.stringify(res.data));

          if(res.data.uid != null){

            Vibration.vibrate(DURATION);


            const value = {
              cn:res.data.cn,
              mail:email,
              employeeid: res.data.employeeid,
              title: res.data.title,
              departement: res.data.department,
              uid: res.data.uid,
              empno: res.data.empno,
              fcmToken,
              getuniqueid:DeviceInfo.getUniqueID(),
              gettimezone:DeviceInfo.getTimezone(),
              getdeviceid:DeviceInfo.getDeviceId(), 
              getmodel:DeviceInfo.getModel(),
              istablet:DeviceInfo.isTablet(),
              getsystemname:DeviceInfo.getSystemName(),
              getsystemversion:DeviceInfo.getSystemVersion(),
              
            }


            this.props.dispatch({
              type: 'USER_LOGIN',
              payload: value
            }); 
          }else{
            // this.setState({loader: false});
            this.setState({
              toggle:true,
              emailError: 'User does not exist',
              loader: false
        
            });
            setTimeout(() => {
              ToastAndroid.show("Anda belum terdaftar di LDAP", ToastAndroid.SHORT);
            }, 1000);
          }

        })
        .catch(function (error) {
          ToastAndroid.show("Koneksi error"+ error, ToastAndroid.SHORT);
          // console.log("testing apildap consolelog error dua" + JSON.stringify(error));

          });


    }else{
      Vibration.vibrate(DURATION);
      this.onBlurUsername(email);
      this.onBlurPassword(password);    
    }


  }

  pressPrivate(){

    let { cekInputUsername, cekInputPassword, email, password, fcmToken } = this.state;

    if( cekInputUsername == true & cekInputPassword == true){
      this.setState({loader: true})

      Vibration.vibrate(DURATION);


      const value = {
        email,
        password,
        fcmToken,
        getuniqueid:DeviceInfo.getUniqueID(),
        gettimezone:DeviceInfo.getTimezone(),
        getdeviceid:DeviceInfo.getDeviceId(), 
        getmodel:DeviceInfo.getModel(),
        istablet:DeviceInfo.isTablet(),
        getsystemname:DeviceInfo.getSystemName(),
        getsystemversion:DeviceInfo.getSystemVersion(),
        
      }


      this.props.dispatch({
        type: 'USER_LOGIN_OTHER',
        payload: value
      }); 


    }else{
      Vibration.vibrate(DURATION);
      this.onBlurUsername(email);
      this.onBlurPassword(password);    
    }



  }


  pressAutoComplete(){
    this.setState({
      visibleDialog:true,
      statusDialog:1
    })
    // ToastAndroid.show('icon Plus', ToastAndroid.SHORT);
  }

  pressAutoCompleteHide(){
    this.setState({
      visibleDialog:false,
      statusDialog:0
    })
    // ToastAndroid.show('icon Plus', ToastAndroid.SHORT);
  }


  // confirmCode = () => {
  //   const { codeInput, confirmResult } = this.state;

  //   if (confirmResult && codeInput.length) {
  //     confirmResult.confirm(codeInput)
  //       .then((user) => {
  //         this.setState({ message: 'Code Confirmed!' });
  //       })
  //       .catch(error => this.setState({ message: `Code Confirm Error: ${error.message}` }));
  //   }
  // };


  pressDokterAfterOtp(){

    let { cekInputUsername, cekInputPassword, email, password, fcmToken } = this.state;
    this.setState({loader: true});
    Vibration.vibrate(DURATION);


    const value = {
      email,
      password,
      fcmToken,
      getuniqueid:DeviceInfo.getUniqueID(),
      gettimezone:DeviceInfo.getTimezone(),
      getdeviceid:DeviceInfo.getDeviceId(), 
      getmodel:DeviceInfo.getModel(),
      istablet:DeviceInfo.isTablet(),
      getsystemname:DeviceInfo.getSystemName(),
      getsystemversion:DeviceInfo.getSystemVersion(),
      
    }


    this.props.dispatch({
      type: 'USER_LOGIN_DOKTER',
      payload: value
    }); 

  }


  pressDokter(){
      

    let { cekInputUsername, cekInputPassword, email, password, fcmToken } = this.state;

    if( cekInputUsername == true & cekInputPassword == true){

      this.setState({loader: true});
      Vibration.vibrate(DURATION);
  
  
      const value = {
        email,
        password,
        fcmToken,
        getuniqueid:DeviceInfo.getUniqueID(),
        gettimezone:DeviceInfo.getTimezone(),
        getdeviceid:DeviceInfo.getDeviceId(), 
        getmodel:DeviceInfo.getModel(),
        istablet:DeviceInfo.isTablet(),
        getsystemname:DeviceInfo.getSystemName(),
        getsystemversion:DeviceInfo.getSystemVersion(),
        
      }
  
  
      this.props.dispatch({
        type: 'USER_LOGIN_DOKTER',
        payload: value
      }); 


    }else{
      this.onBlurUsername(email);
      this.onBlurPassword(password);    
    }




  }


  onButtonPress() {


    let { cekInputUsername, cekInputPassword, email, password, fcmToken } = this.state;


    console.log("onButtonPress", cekInputUsername, cekInputPassword );

    if( cekInputUsername == true & cekInputPassword == true){

      this.setState({loader: true});
      Vibration.vibrate(DURATION);
  
  
      const value = {
        email,
        password,
        fcmToken,
        getuniqueid:DeviceInfo.getUniqueID(),
        gettimezone:DeviceInfo.getTimezone(),
        getdeviceid:DeviceInfo.getDeviceId(), 
        getmodel:DeviceInfo.getModel(),
        istablet:DeviceInfo.isTablet(),
        getsystemname:DeviceInfo.getSystemName(),
        getsystemversion:DeviceInfo.getSystemVersion(),
        
      }
  
  
      this.props.dispatch({
        type: 'USER_LOGIN_DOKTER',
        payload: value
      }); 


    }else{
      this.onBlurUsername(email);
      this.onBlurPassword(password);    
    }


  }



  onBlurUsername(value) {

    if(this.state.nameConnection == 'Private Login'){

      this.setState({email: value.toLowerCase()});

      console.log("jumlah email",this.state.email.length)
      if(this.state.email.length >= 2){

        this.setState({
          dataBackupDokter: '',  
          dataSourceDokter: '', 
          loader:true
        });

        ApiCaller('mobileIattend_autocomplete', 'post', 123456, {
          email : value
          
      
        }).then(response =>{

          this.setState({
            loader:false,
            dataBackupDokter: response.datanya,  
            dataSourceDokter: response.datanya
          });
    
          console.log("onBlurUsername", response);
          this.pressAutoComplete();
        
        })




      }

    }else{

      this.setState({
        visibleDialog:false,
      })

      this.setState({email: value.toLowerCase().trim()});

      let emailError = validator('email', value);
      if(emailError){
        this.setState({
          toggle:true,
          emailError: emailError,
        })

        if(this.state.email == ''){
          this.setState({
            placeholderUsername:'Email address here'
          })
        }

      }else{
        this.setState({
          toggle:false,
          emailError: emailError,
          cekInputUsername:true
        })
      }
    }
    


 }

 onBlurUsernameRelease(){

    if(this.state.placeholderUsername == ''){
      this.setState({
        placeholderUsername:'Email address here'
      })
    }else{

    }
 }

 onBlurPasswordRelease(){

  if(this.state.placeholderPassword == ''){
    this.setState({
      placeholderPassword:'Password here'
    })
  }else{

  }

}


 onBlurPassword(value) {
  // this.setState({password: value.toLowerCase().trim()});
  this.setState({password: value.trim()});
    
     let passwordError = validator('password', value);
     
     if(passwordError){
     
      this.setState({
        togglePassword:true,
        passwordError: passwordError,
      })

      if(this.state.password == ''){
        this.setState({
          placeholderPassword:'Password here'
        })
      }

     }else{
      this.setState({
        togglePassword:false,
        passwordError: passwordError,
        cekInputPassword:true
      })
     }
  }

  async componentDidMount(){

      // BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        this.goBack(); // works best when the goBack is async
        return true;
      });

      NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
      if(this.state.isConnected){
        this.checkPermission();

      }


  }

  componentWillMount(){
  
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);

    if(this.state.isConnected){

      console.log("componentWillMount trace login",this.props)
      console.log("trace login didmount",this.props.navigation.state.params);

      this.setState({
        loader: true
      }); 
  
      console.log("trace login",this.props.checkversionData.checkversion, DeviceInfo.getUniqueID(), DeviceInfo.getManufacturer())
  
      this.setState({
        versionmobile: this.props.checkversionData.checkversion.statusDevices,
        dxg : this.props.checkversionData.checkversion.versiDevices,
        linkDevices: this.props.checkversionData.checkversion.linkDevices,
        DeviceInfo: this.props.checkversionData.checkversion.DeviceInfo,
        user: this.props.checkversionData.checkversion.user,
        
      })
  
      if(this.props.checkversionData.checkversion.statusDevices == false && this.props.checkversionData.checkversion.DeviceInfo !=0 && this.props.navigation.state.params == undefined){
  

        const value = {
          email:this.props.checkversionData.checkversion.user.mail
        }

        AsyncStorage.setItem("userData", JSON.stringify(this.props.checkversionData.checkversion.user));
        this.props.dispatch({
          type: 'FETCH_EVENT_LIST',
          payload: value
        });
        //  Helper.navigateToPage(this, 'Home');
  
         setTimeout(() => {
          this.setState({
            loginError: false,
            loader: false
          }); 
  
          Helper.navigateToPage(this, 'Home', this.props.checkversionData.checkversion.user);
  
          
        }, 2000);
  
      }else{
        this.setState({
          loader: false
        }); 
      }


    }


 }

 componentWillUnmount() {
  BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
  if (this.unsubscribe) this.unsubscribe();
 }

 componentWillReceiveProps(nextProps) {



    if(this.state.nameConnection == 'LDAP Login'){

        this.setState({ loader: true})
        const { loginData } = nextProps;

        console.log("componentWillReceiveProps login LDAP", loginData);
          
        if(!loginData.login.exist){
        
          const value = {
            email:loginData.login.user.mail
          }

          AsyncStorage.setItem("userData", JSON.stringify(loginData.login.user));
          this.props.dispatch({
            type: 'FETCH_EVENT_LIST',
            payload:value
          });
          //  Helper.navigateToPage(this, 'Home');

          setTimeout(() => {
            this.setState({
              loginError: false,
              loader: false
            }); 
    
            Helper.navigateToPage(this, 'Home', loginData.login.user);
    
            
          }, 2000);

        }else{
          this.setState({
            loader: false
          }); 
        }

    }else if(this.state.nameConnection == 'Private Login'){

      this.setState({loader: true});

      const { loginDokterData } = nextProps;

      console.log("componentWillReceiveProps login Dokter", loginDokterData.loginDokter);

      if(loginDokterData.loginDokter.exist == true && loginDokterData.loginDokter.credential == true){
        
        console.log("componentWillReceiveProps login Dokter tiga", loginDokterData.loginDokter.user.mail);

        const value = {
          email:loginDokterData.loginDokter.user.mail
        }

        AsyncStorage.setItem("userData", JSON.stringify(loginDokterData.loginDokter.user));
        this.props.dispatch({
          type: 'FETCH_EVENT_LIST',
          payload:value
        });

        setTimeout(() => {
          this.setState({
            loginError: false,
            loader: false
          }); 
  
          Helper.navigateToPage(this, 'Home', loginDokterData.loginDokter.user);
  
          
        }, 2000);

      }else{
        this.setState({
          loader: false
        }); 
      }


      // this.setState({loader: true});

      // const { loginOtherData } = nextProps;

      // console.log("componentWillReceiveProps login Private", loginOtherData);

      // if(loginOtherData.loginOtherData.exist == true && loginOtherData.loginOtherData.credential == true){
        
      //   const value = {
      //     email:loginOtherData.loginOtherData.user.mail
      //   }

      //   AsyncStorage.setItem("userData", JSON.stringify(loginOtherData.loginOtherData.user));
      //   this.props.dispatch({
      //     type: 'FETCH_EVENT_LIST',
      //     payload:value
      //   });
      //   //  Helper.navigateToPage(this, 'Home');

      //   setTimeout(() => {
      //     this.setState({
      //       loginError: false,
      //       loader: false
      //     }); 
  
      //     Helper.navigateToPage(this, 'Home');
  
          
      //   }, 2000);

      // }else{
      //   this.setState({
      //     loader: false
      //   }); 
      // }


    }else{

      this.setState({loader: true});

      const { loginDokterData } = nextProps;

      console.log("componentWillReceiveProps login Dokter", loginDokterData.loginDokter);

      if(loginDokterData.loginDokter.exist == true && loginDokterData.loginDokter.credential == true){
        
        const value = {
          email:loginDokterData.loginDokter.user.mail
        }

        AsyncStorage.setItem("userData", JSON.stringify(loginDokterData.loginDokter.user));
        this.props.dispatch({
          type: 'FETCH_EVENT_LIST',
          payload:value
        });
        //  Helper.navigateToPage(this, 'Home');

        setTimeout(() => {
          this.setState({
            loginError: false,
            loader: false
          }); 
  
          Helper.navigateToPage(this, 'Home', loginDokterData.loginDokter.user);
  
          
        }, 2000);

      }else{
        this.setState({
          loader: false
        }); 
      }
    }


    
  }


  onClickPilihDokter(data){

    this.setState({ 
      email:data.LOGIN,
      password:'123456',
      phoneNumber:'',
      visibleDialog:false,
      statusDialog:0,
      cekInputUsername: true,
      cekInputPassword: true
     });


  }

  handleCancel = () => {
    this.setState({ versionmobile: false });
    RNExitApp.exitApp();
  };
 
  handleDelete = () => {
    Linking.openURL(this.state.linkDevices)
  };

  testAlert(){
    Alert.alert(
      'Exit Application',
      'Exiting the application?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => RNExitApp.exitApp()},
      ],
      { cancelable: false }
    )
    return true;
  }  

  handleConnectivityChange = isConnected => {
    if (isConnected) {
      this.setState({ isConnected });
    } else {
      this.setState({ isConnected });
    }
  };
  

  pressCekOTP = () => {
    
    const { codeInput, confirmResult } = this.state;

    if(codeInput.length == 6){
      this.setState({ otpSukses:true });
      if (confirmResult && codeInput.length) {
        confirmResult.confirm(codeInput)
          .then((user) => {
            this.setState({ message: 'Code Confirmed!'});
            ToastAndroid.show("Please wait ...", ToastAndroid.SHORT);
            this.pressDokterAfterOtp();
          })
          .catch(error => {
            this.setState({ message: `Code Confirm Error: ${error.message}`, otpSukses:false })
            ToastAndroid.show(this.state.message, ToastAndroid.SHORT);
          });
          console.log("pressCekOTP", this.state.message, );
      }
    }else{
      ToastAndroid.show("Please fill the code box", ToastAndroid.SHORT);
    }



  };


  setOtp(value){

    this.setState({
      codeInput:value
    })

  }

  renderSeparatorRelasi = () => {  
    return (  
     <View  
      style={{  
       height: 1,  
       width: '100%',  
       backgroundColor: '#CED0CE',  
      //  marginLeft: '14%',  
      }}  
     />  
    );  
   };


   onButtonPressClearEmail(){
     this.setState({
       email:'',
       placeholderUsername:'Email address here', 
       password:'',
       placeholderPassword:'Password here'
     })
   }

   renderMessage() {
    const { message } = this.state;
  
    if (!message.length) return null;
  
    return (
      <Text style={{ padding: 5, backgroundColor: '#000', color: '#fff' }}>{message}</Text>
    );
  }

  body() {

    const {email, password, emailError, passwordError } = this.state;
  
    return (
      <View style={styles.body}>
      
          <StatusBar
            //  hidden={false}
            //  translucent={true}
            backgroundColor="#920a08"
            //  barStyle="light-content"
            animated
            // barStyle={barStyle}
            // backgroundColor={backgroundColor}
            translucent
            showHideTransition={'fade'}
          />


          <DialogUpdate.Container visible={this.state.versionmobile}>
            <DialogUpdate.Title>Update information</DialogUpdate.Title>
            <DialogUpdate.Description>
            {this.state.dxg} version is available. Please update your application to the latest version.
            </DialogUpdate.Description>
            <DialogUpdate.Button label="Cancel" onPress={this.handleCancel} />
            <DialogUpdate.Button label="Update" onPress={this.handleDelete} />
          </DialogUpdate.Container>

            <Dialog
              width= {0.8}
              visible={this.state.visibleDialog}
              dialogTitle={<DialogTitle title="List user" align="left" />}
              footer={
                <DialogFooter
                >
                  <DialogButton
                    textStyle={{color:'#666666', fontSize:wp('2.2%')}}
                    text="CLOSE"
                    onPress={() => {this.pressAutoCompleteHide()}}
                  />
                </DialogFooter>
              }
              dialogAnimation={new SlideAnimation({
                slideFrom: 'bottom',
              })}
            >
            <DialogContent>
            <View style={{height:hp('40%'), marginTop:10}}>
            {/* <ScrollView style={{ flex: 1 }}>

            </ScrollView> */}

                  <FlatList  
                  data={this.state.dataSourceDokter}  
                  renderItem={({ item }) => (   

                  <ListItem 
                  style={{paddingLeft:8, marginLeft:0}} 
                  button onPress={() => this.onClickPilihDokter(item)}
                  noBorder
                  thumbnail
                  >
                    <Body>
                      {/* <Text>{props.eventId} - {props.barisId} - {props.jumBaris}</Text> */}
                      <Text>{item.LOGIN}</Text>
                      <Text note numberOfLines={1}>{item.keterangan}</Text>
                    </Body>
                  </ListItem>

                  )}  
                  ItemSeparatorComponent={this.renderSeparatorRelasi}  
                  // ListHeaderComponent={this.renderHeader}  
                  
                /> 
            </View>  
            </DialogContent>
          </Dialog>




          <View style={{flexDirection: 'row',flex: 0.1,justifyContent: 'center',marginTop:10}}> 
          <Modal
            transparent={true}
            animationType={'none'}
            visible={this.state.loader}
            onRequestClose={() => {console.log('close modal')}}>
            <View style={styles.modalBackground}>
              <View style={styles.activityIndicatorWrapper}>
                <ActivityIndicator
                  animating={this.state.loader} />
              </View>
            </View>
          </Modal> 
          </View>

          <View style={{alignContent:'center', alignItems:'center', marginBottom:w(10)}}>
            {/* <AutoHeightImage
              width={w(100)}
              source={imageLogo}
              // style={styles.paragraph}
            /> */}

          <Image
            source={imageLogo}
            resizeMode={"contain"}
            style={styles.paragraph}
          >
          </Image>

          </View>

          <Label style={styles.label}>USERNAME</Label>
          <Item style={styles.inputStyle}>
            <Icon active name='ios-person' style={{color:'#af1e1e'}} />
            <Input
              autoCorrect={false} 
              onChangeText={(value) => this.onBlurUsername(value)}
              value={this.state.email}
              onTouchStart={()=> this.onTouchStartUsername()}
              placeholder={this.state.placeholderUsername}
              onBlur={() => this.onBlurUsernameRelease()}
              style={styles.InputTextStyle}
              placeholderTextColor= '#666666'

            />

            { email != '' ? 
            <TouchableOpacity onPress={() => this.onButtonPressClearEmail()} >
            <Icon active type="FontAwesome" name='remove' style={{color:'#af1e1e'}} />
            </TouchableOpacity>
            :
            null
            }


            
          </Item>
          {emailError ? 
              <Text style={{color:"#af1e1e", paddingTop:3, paddingBottom:0}}>{emailError}</Text>
              : null }

          <Label style={styles.labelPassword}>PASSWORD</Label>  
          <Item style={styles.inputStyle}>
          <Icon active name='ios-unlock' style={{color:'#af1e1e'}} />
          <Input 
            onChangeText={(value) => this.onBlurPassword(value)}
            value={this.state.password}
            secureTextEntry={true}
            onTouchStart={()=> this.onTouchStartPassword()}
            placeholder={this.state.placeholderPassword}
            placeholderTextColor= '#666666'
            style={styles.InputTextStyle}
            onBlur={() => this.onBlurPasswordRelease()}
          />
          </Item>
          <Text style={{color:"#af1e1e", paddingTop:3, paddingBottom:10}}> {passwordError ? passwordError : null }</Text>

          {/* <Dropdown
            label='Login Connection'
            data={data}
            onChangeText={this.onChangeTextConnection}
          /> */}


          <ButtonInternal
            buttonTextStyle={styles.buttonTextStyle}
            text="Login"
            style={styles.buttonStyle}
            onPress={() => this.onButtonPress()}
          />

          <ButtonInternal
            buttonTextStyle={styles.buttonTextStyle}
           text="New User ? SignUp"
           style={styles.buttonStyleRegister}
           onPress={() => Helper.navigateToPage(this, 'Register')}
          />

      </View>
    )


  }

  closeApp(){
    RNExitApp.exitApp();
  };

  goBack(){
    if(this.state.statusDialog == 0){
      this.testAlert();
    }
    else{
      this.pressAutoCompleteHide();
    }
  }

  // buttonBack(){
  //   BackHandler.addEventListener('hardwareBackPress', function() {
  //     // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
  //     // Typically you would use the navigator here to go to the last state.
    
  //     if (!this.onMainScreen()) {
  //       this.goBack();
  //       return true;
  //     }
  //     return false;
  //   });
  // }

  // handleBackPress = () => {
  //   this.goBack(); // works best when the goBack is async
  //   return true;
  // }


  render() {

    return (

      
        <View style={styles.containerLogin}>

        <OfflineNotice />
        
        <ImageBackground 
            resizeMode='cover'
            // blurType="light"
            // blurAmount={5}
            // blurRadius={5}
            source={require("../../images/launchscreen.png")} style={styles.backgroundImage} >  

          {this.body()}

        </ImageBackground>
        </View>
    )
  }

}
const mapStateToProps = state => ({
  loginData: state.loginData,
  loginOtherData: state.loginOtherData,
  loginDokterData: state.loginDokterData,
  checkversionData: state.checkversionData
});



export default connect(mapStateToProps)(Login);