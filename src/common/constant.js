import { Subtitle } from 'native-base';

export const loginFeature = 'Log in more features';
export const crowdLogo = 'CrowdCompass';
export const crowdMoto = 'by Cvent';
export const aboutUsHeaderTitle = 'AboutUs';
export const versionDevice = '1.1.2';

export const options = [{
      value: 'event1',
    }, {
      value: 'event2',
    }, {
      value: 'event3',
    }]

//LogIN Page
export const logIn = 'Log In';
export const logInTitle = 'Let\'s get started.';
export const logInCaption = 'Enter your credentials';
export const terms = 'By logging in, you agree to IAttend\'s';
export const privacyPolicy = 'Privacy Policy';
export const and = 'and';
export const termsUse = 'Terms of Use';

export const RegisterConst ={
  pageTitle : 'Create your account',
  subTitle: 'Enter your detaills',
  btnText:'Register',
  footerText:'By signing up, you agree to our'
}


export const logInEntry = [
  { text: 'Email', icon: 'user', value: 'email'},
  { text: 'Password', icon: 'user', value: 'password' },
];

export const aboutUsFirst = 'EventApp is the definitive voice for technology and digital innovation across the financial and government services sectors, spanning Australia, New Zealand and South East Asia.';
export const drawerMain = [
  { text: 'Switch Event', icon: 'ios-swap' },
  { text: 'FST Banking Summit', icon: 'ios-home-outline' },
];

export const myItems = [
  { icon: 'ios-stopwatch-outline', menuOption: 'My Schedule', screen: 'Agenda' },
  // { icon: 'md-text', menuOption: 'My Messages', screen: 'Agenda'},
  // { icon: 'md-contact', menuOption: 'My Contacts', screen: 'NotesList' },
  { icon: 'ios-paper-outline', menuOption: 'My Notes', screen: 'NotesList' },
  { icon: 'ios-log-out', menuOption: 'Log Out'},
];

export const eventGuide = [
  { icon: 'ios-albums-outline', menuOption: 'Agenda', screen: 'Agenda' },
  { icon: 'ios-headset-outline', menuOption: 'Speakers', screen: 'SpeakersAll' },
  { icon: 'ios-happy-outline', menuOption: 'Sponsors', screen: 'SponsorsAll' },
  { icon: 'ios-book-outline', menuOption: 'Attendees', screen: 'Attendees'},
  { icon: 'ios-pricetag-outline', menuOption: 'About', screen: 'AboutUs' },
  { icon: 'logo-facebook', menuOption: 'Social Media', screen: 'SocialMedia' },
  { icon: 'ios-search', menuOption: 'Search', screen: 'SearchForm' },
];

export const eventBeforeLoginUser = [
  { icon: 'ios-home', menuOption: 'Home', screen: 'Home' },
  // { icon: 'ios-headset-outline', menuOption: 'Attend', screen: 'Attend' },
  // { icon: 'ios-book-outline', menuOption: 'Attendee list', screen: 'Attendeelist'},
  { icon: 'ios-exit-outline', menuOption: 'Logout', screen: 'Logout' },
  { icon: 'ios-close', menuOption: 'Exit Application', screen: 'Exit' },
];

export const eventImage = require('../images/Event.jpg');

// export const mainText = 'DEXA Events';
export const mainText = 'Events';
export const mainNotes = 'My Notes';
export const eventList = [
  {
    source: eventImage, eventTitle: 'DEXA Banking sunil', eventAddress: 'Singapore', eventDescription: '12 Apr 2018 - 12 Apr 2018', id: 1,
  },
  {
    source: eventImage, eventTitle: 'FST Government Western Australia', eventAddress: 'East Perth, Western Australia', eventDescription: '12 Apr 2018 - 12 Apr 2018', id: 2,
  },
];

export const eventListJune = [
  {
    source: eventImage, eventTitle: 'FST suni Summit', eventAddress: 'Singapore', eventDescription: '12 Apr 2018 - 12 Apr 2018', id: 3,
  },
  {
    source: eventImage, eventTitle: 'FST Banking Summit', eventAddress: 'Singapore', eventDescription: '12 Apr 2018 - 12 Apr 2018', id: 4,
  },
  {
    source: eventImage, eventTitle: 'FST Government Western Australia', eventAddress: 'East Perth, Western Australia', eventDescription: '12 Apr 2018 - 12 Apr 2018', id: 5,
  },
];

export const datas = [
    {
      img: eventImage,
      text: "Registration Start",
      note: "preparing your identity",
      time: "3:43 pm"
    },
    {
      img: eventImage,
      text: "Chairpersons Opening Remarks",
      note: "Ceremony and Praying",
      time: "1:12 pm"
    },
    {
      img: eventImage,
      text: "The Future Of Fintech : Creating A Culture",
      note: "Lee Hatton, U Bank",
      time: "10:03 am"
    },
    {
      img: eventImage,
      text: "Technology Presentation",
      note: "Tips and Trick",
      time: "5:47 am"
    },
    {
      img: eventImage,
      text: "The Need For Speed",
      note: "Christian Venter, ANZ",
      time: "11:11 pm"
    },
    {
      img: eventImage,
      text: "Dagelan Express",
      note: "Raditya Dika, Kang Sapu",
      time: "8:54 pm"
    }
  ];
export const dataSpeaker = [
      {
        img: eventImage,
        text: "Jess No Limit",
        note: "Founder EVOS",
        time: "3:43 pm"
      },
      {
        img: eventImage,
        text: "Tuturu",
        note: "CTO RRQ O2",
        time: "1:12 pm"
      },
      {
        img: eventImage,
        text: "Lemon",
        note: "Lead Programs RRQ O2",
        time: "10:03 am"
      },
      {
        img: eventImage,
        text: "Warpath",
        note: "Curtin University",
        time: "5:47 am"
      },
      {
        img: eventImage,
        text: "Oura",
        note: "Co Founder GOJEK",
        time: "11:11 pm"
      },
      {
        img: eventImage,
        text: "Vanesha Prescilla",
        note: "Kang Tambal Ban",
        time: "8:54 pm"
      }
    ];

    export const profileData = {
      name: 'Craig Rowlands',
      designation: 'General Manager, Information Management, Technolgy & Opeartions - Corporate Medibank',
      biography: 'Craig Rowlands is the general Manager of Information of Medibank. His role is to provide thought leadership, aligned with expertise in delivering management solutions and management solutions and imporvements in the data landscape.'
    }
