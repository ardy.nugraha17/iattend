import { eventImage } from '../common/constant';
import moment from 'moment';

module.exports = {

  getQuestionListData(value) {
    const questionListAudience = [];
    const questionListPolling = [];

    value.detailaudience.map((data) =>{
      questionListAudience.push({
        id: data.id,
        audince_id: data.audince_id,
        agendaId: data.agendaId,
        answer_value: data.answer_value,
        pollingNumber: data.pollingNumber,
        audinceId: data.audinceId,
        name: data.name,
        email: data.email,
        phone: data.phone,
        image: data.image,
        jawaban: data.jawaban,
      })
    } )

    value.detailpollingchoose.map((data) =>{ 
      questionListPolling.push({
        id: data.id,
        agendaId: data.agendaId,
        pollingNumber: data.pollingNumber,
        answer_text: data.answer_text,
        status: data.status
      })
    })


    const questionList = {
      questionListAudience,
      questionListPolling
    }
    return questionList;
  },

  parseList(value) {
    const parseList = [];
    value.map((data) => {
      parseList.push({
        value: data.name,
        id: data.id,
      })
    } )
    return parseList;
  },

};
