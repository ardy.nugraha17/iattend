import { eventImage } from '../common/constant';
import moment from 'moment';

module.exports = {

  getRegisterEventListData(value) {
    
    const eventListClass = [];
    const eventListTrack = [];
    value.classList.map((data) =>{
      eventListClass.push({
        name: data.name,
        id: data.id,
      })
    } )
    value.trackList.map((data) =>{
      eventListTrack.push({
        name: data.name,
        eventId: data.eventId,
        id: data.id,
      })
    } )
    const eventList = {
      eventListClass,
      eventListTrack
    }
    return eventList;
  },

  parseList(value) {
    const parseList = [];
    value.map((data) => {
      parseList.push({
        value: data.name,
        id: data.id,
      })
    } )
    return parseList;
  },

};
