import { eventImage } from '../common/constant';
import moment from 'moment';

module.exports = {

  getEventListData(value) {
    const eventListCurrent = [];
    const eventListPast = [];
    value.current.map((data) =>{
      eventListCurrent.push({
        source: data.image,
        eventTitle: data.name,
        eventAddress: data.location,
        eventDate: data.date,
        eventDescrip: data.description,
        lat_long: data.lat_long,
        id: data.id,
      })
    } )
    value.past.map((data) =>{
      eventListPast.push({
        source: data.image,
        eventTitle: data.name,
        eventAddress: data.location,
        eventDate: data.date,
        eventDescrip: data.description,
        lat_long: data.lat_long,
        id: data.idEvent,
      })
    } )
    const eventList = {
      eventListCurrent,
      eventListPast
    }
    return eventList;
  },

  parseList(value) {
    const parseList = [];
    value.map((data) => {
      parseList.push({
        value: data.name,
        id: data.id,
      })
    } )
    return parseList;
  },

};
