import { eventImage } from '../common/constant';
import moment from 'moment';

module.exports = {

  getEventListData(value) {
    const eventListCurrent = [];
    const eventListPast = [];
    value.current.map((data) =>{
      eventListCurrent.push({
        description: data.description,
        logo: data.logo,
        name: data.name,
        id: data.id,
      })
    } )
    value.past.map((data) =>{
      eventListPast.push({
        description: data.description,
        logo: data.logo,
        name: data.name,
        id: data.id,
      })
    } )
    const eventList = {
      eventListCurrent,
      eventListPast
    }
    return eventList;
  },

  parseList(value) {
    const parseList = [];
    value.map((data) => {
      parseList.push({
        value: data.name,
        id: data.id,
      })
    } )
    return parseList;
  },

};
