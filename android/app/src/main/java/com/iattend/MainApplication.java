package com.iattend;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.rssignaturecapture.RSSignatureCapturePackage;
import cl.json.RNSharePackage;
import com.safaeean.barcodescanner.BarcodeScannerPackage;
import org.wonday.pdf.RCTPdfView;
import com.airbnb.android.react.maps.MapsPackage;
import com.chirag.RNMail.RNMail;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import com.rnfs.RNFSPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.github.wumke.RNExitApp.RNExitAppPackage;
import com.reactlibrary.RNReactNativeDocViewerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import org.reactnative.camera.RNCameraPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNFetchBlobPackage(),
            new RSSignatureCapturePackage(),
            new RNSharePackage(),
            new BarcodeScannerPackage(),
            new RCTPdfView(),
            new MapsPackage(),
            new RNMail(),
            new ImageResizerPackage(),
            new RNFSPackage(),
            new RNFetchBlobPackage(),
            new RNExitAppPackage(),
            new RNReactNativeDocViewerPackage(),
            new RNDeviceInfo(),
            new RNCameraPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
